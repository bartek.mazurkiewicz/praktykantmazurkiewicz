﻿using System.Data.SQLite;
using System.Data;

namespace Monitor
{
    class SqliteDataAcess
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private void SetConnection(string dataBase)
        {
            sql_con = new SQLiteConnection("Data Source='"+dataBase+"';Version=3;New=False;Compress=True;");
        }
        public void ExecuteQuery(string txtQuery, string dataBase)
        {
            SetConnection(dataBase);
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
        }
        public void LoadData(string tableName, System.Windows.Forms.DataGridView dataGridView, string dataBase)
        {
            SetConnection(dataBase);
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string commandText = "select * from " + tableName;
            DB = new SQLiteDataAdapter(commandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dataGridView.DataSource = DT;
            sql_con.Close();
        }
    }
}
