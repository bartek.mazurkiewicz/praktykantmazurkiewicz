﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonitorService
{ 
    class UsageManager
    {
        PerformanceCounter pCPU = new PerformanceCounter();
        PerformanceCounter pRAM = new PerformanceCounter();
        string database = @"C:\Users\barte\source\repos\praktyka\Projekty - praktyka\Monitor\bin\Debug\Monitor.db";
        SqliteDataAcess sqliteDataAcess;
        public float LoadCPUData(PerformanceCounter p)
        {
            float fcpu = p.NextValue();
            return fcpu;
        }
        public float LoadRAMData(PerformanceCounter p)
        {
            float fram = p.NextValue();
            return fram;
        }
        public void LoadCPUUsageToDatabase(int ID)
        {
            sqliteDataAcess = new SqliteDataAcess();
            float fcpu = LoadCPUData(pCPU);
            
            string txtQueryCPU = "insert into CpuUsage (Raport_ID, Usage)values('"+ID+"','" + (int)fcpu + "')";
            sqliteDataAcess.ExecuteQuery(txtQueryCPU, database);
        }
        public void LoadRAMUsageToDatabase(int ID)
        {
            sqliteDataAcess = new SqliteDataAcess();
            float fram = LoadRAMData(pRAM);

            string txtQueryRAM = "insert into MemoryUsage (Raport_ID, Usage)values('"+ID+"','" + (int)fram + "')";
            sqliteDataAcess.ExecuteQuery(txtQueryRAM, database);
        }
        public void DefinePC()
        {
            pCPU.CategoryName = "Processor";
            pCPU.CounterName = "% Processor Time";
            pCPU.InstanceName = "_Total";
            pRAM.CategoryName = "Memory";
            pRAM.CounterName = "% Committed Bytes In Use";
        }
    }
}
