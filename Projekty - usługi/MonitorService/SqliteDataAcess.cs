﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data;

namespace MonitorService
{
    class SqliteDataAcess
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataReader reader;
        private string database = @"C:\Users\barte\source\repos\praktyka\Projekty - praktyka\Monitor\bin\Debug\Monitor.db";
        private void SetConnection(string dataBase)
        {
            sql_con = new SQLiteConnection("Data Source='"+dataBase+"';Version=3;New=False;Compress=True;");
        }
        public void ExecuteQuery(string txtQuery, string dataBase)
        {
            SetConnection(dataBase);
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
        }
        public int GenerateRaport(Types type)
        {
            int ID = 0;
            string date = DateTime.Now.ToString("dd/MM/yyyy");
            string time = DateTime.Now.ToString("HH:mm:ss");
            string txtquery = $"insert into Raport (Date, Time, Type) values('{date}', '{time}' ,{(int)type})";
            ExecuteQuery(txtquery, database);

            SetConnection(database);
            string txtQuery = $"select Raport_ID from Raport where Date='{date}'";
            sql_cmd = new SQLiteCommand(txtQuery, sql_con);
            sql_con.Open();
            reader = sql_cmd.ExecuteReader();
            while (reader.Read())
            {
                ID = Convert.ToInt32(reader["Raport_ID"]);
            }
            sql_con.Close();

            return ID;
        }
    }
}
