﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MonitorService
{
    public partial class Service1 : ServiceBase
    {
        TaskManager taskManager = new TaskManager();
        UsageManager usageManager = new UsageManager();
        SqliteDataAcess sqliteDataAcess = new SqliteDataAcess();
        Timer timer;
        public Service1()
        {
            InitializeComponent();
            string path = @"C:\Users\barte\source\repos\praktyka\time.txt";
            double time = Convert.ToDouble(System.IO.File.ReadAllText(path));
            timer = new Timer(time);
            //timer = new Timer(25000);
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int id = sqliteDataAcess.GenerateRaport(Types.All);
            taskManager.ListAllProcesses(id);
            usageManager.LoadCPUUsageToDatabase(id);
            usageManager.LoadRAMUsageToDatabase(id);
        }

        protected override void OnStart(string[] args)
        {
            usageManager.DefinePC();
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
        }
    }
}
