﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JoeAndBobSerialization
{
    public partial class Form1 : Form
    {
        Guy joe;
        Guy bob;
        int bank = 1000;


        public void UpdateForm()
        {
            joesCash.Text = joe.Name + " ma " + joe.Cash + " zł.";
            bobsCash.Text = bob.Name + " ma " + bob.Cash + " zł.";
            bankCash.Text = "Bank ma " + bank + " zł.";
        }
        public Form1()
        {
            InitializeComponent();
            joe = new Guy();
            joe.Cash = 3;
            joe.Name = "Joe";

            bob = new Guy();
            bob.Cash = 9000;
            bob.Name = "Bob";
            UpdateForm();
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
                if (bank >= 10)
                {
                    bank -= joe.ReceiveCash(10);
                    UpdateForm();
                }
                else
                {
                    MessageBox.Show("Bank nie posiada takiej ilosci pieniedzy");
                }
            
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            joe.Cash -= bob.ReceiveCash(10);
            UpdateForm();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            bob.Cash -= joe.ReceiveCash(5);
            UpdateForm();
        }

        private void SaveJoe_Click(object sender, EventArgs e)
        {
            using (Stream output = File.Create("Plik_faceta.dat"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(output, joe);
            }
        }

        private void LoadJoe_Click(object sender, EventArgs e)
        {
            using(Stream input = File.OpenRead("Plik_faceta.dat"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                joe = (Guy)formatter.Deserialize(input);
            }
            UpdateForm();
        }
    }
}
