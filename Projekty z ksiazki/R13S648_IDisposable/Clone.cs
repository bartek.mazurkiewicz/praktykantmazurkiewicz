﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace R13S648_IDisposable
{
    [Serializable]
    class Clone : IDisposable
    {
        public void Dispose()
        {
            string filename = @"C:\Users\barte\source\repos\praktykantmazurkiewicz\R13S648_IDisposable\Tymczasowe\Klon.dat";
            string dirname = @"C:\Users\barte\source\repos\praktykantmazurkiewicz\R13S648_IDisposable\Tymczasowe";
            if (File.Exists(filename) == false)
            {
                Directory.CreateDirectory(dirname);
            }
            BinaryFormatter bf = new BinaryFormatter();
            using (Stream output = File.OpenWrite(filename))
            {
                bf.Serialize(output, this);
            }
            MessageBox.Show("Tu " + this.Id + ", muszę... zserializować... obiekt...");
        }
        public int Id { get; private set; }
        public Clone(int Id)
        {
            this.Id = Id;
        }
        ~Clone()
        {
            MessageBox.Show("Aaaaaa! Dopadłeś mnie!", "Klon " + Id + ", mówi...");
        }
    }
}
