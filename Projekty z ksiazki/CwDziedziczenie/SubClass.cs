﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CwDziedziczenie
{
    public class SubClass : BaseClass
    {
        public SubClass(string baseClassNeedThis, int anotherValue) : base(baseClassNeedThis)
        {
            MessageBox.Show("To jest klasa pochodna: " + baseClassNeedThis + " i " + anotherValue);
        }
    }
}
