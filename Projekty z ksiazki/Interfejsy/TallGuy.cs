﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfejsy
{
    class TallGuy : IClown
    {
        public string FunnyThingIHave { get { return "duże buty"; } }
        public string Name;
        public int Height;
        public void TalkAboutYourself()
        {
            Console.WriteLine("Nazywam się " + Name + " i mam " + Height + " cm wzrostu. Pozdro");
        }
        public void Honk(int length)
        {
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Tut tuut!");
            }
            
        }
        public void Sing(string lalala)
        {
            Console.WriteLine(lalala);
        }
           
    }
}
