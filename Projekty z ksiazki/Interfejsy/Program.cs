﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfejsy
{
    class Program
    {
        static void Main(string[] args)
        {
            TallGuy tallGuy = new TallGuy() { Height = 74, Name = "Adam Brotnik" };
            tallGuy.TalkAboutYourself();
            tallGuy.Honk(5);
            ScaryScary fingersTheClown = new ScaryScary("kij do golfa", 35);
            FunnyFunny someFunnyClown = fingersTheClown;
            IScaryClown someOtherScaryClown = someFunnyClown as ScaryScary;
            someOtherScaryClown.Honk(4);
            someOtherScaryClown.ScareLittleChildren();
            someOtherScaryClown.ShowScaryThing();
            tallGuy.Sing("lalalalallalal");
            Console.ReadKey();
            
        }
    }
}
