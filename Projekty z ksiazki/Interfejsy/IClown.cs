﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfejsy
{
    interface IClown
    {
        void Honk(int length);
        string FunnyThingIHave { get; }
    }
}
