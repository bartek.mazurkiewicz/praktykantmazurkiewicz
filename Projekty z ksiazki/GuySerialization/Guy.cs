﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GuySerialization
{
    [DataContract(Namespace = "http://www.headfirstlabs.com/Chapter11")]
    class Guy
    {
        public Guy(string name, int age, decimal cash)
        {
            Name = name;
            Age = age;
            Cash = cash;
        }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public int Age { get; private set; }
        [DataMember]
        public decimal Cash { get; private set; }
        [DataMember(Name = "MyCard")]
        public Card TrumpCard { get; set; }
        public override string ToString()
        {
            return String.Format("Mam na imię {0}, {1} lat i {2} złotych" + " w kieszeni, a moją atutową kartą jest {3}, pozdrawiam serdecznie, " + Name + ".", Name, Age, Cash, TrumpCard);
        }
    }
}
