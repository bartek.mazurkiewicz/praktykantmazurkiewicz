﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypyWyliczeniowe
{
    class Card
    {
        public override string ToString()
        {
            return Name;
        }
        public Suits Suit { get; set; }
        public Values Value { get; set; }
        public Card(Suits suits, Values value)
        {
            this.Suit = suits;
            this.Value = value;
        }
        public string Name
        {
            get
            {
                return Value.ToString() + " of " + Suit.ToString();
            }
        }
       
   
    }
}
