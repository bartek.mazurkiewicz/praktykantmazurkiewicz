﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace BarNiechlujnyJanekWPF
{
    class MenuMaker : INotifyPropertyChanged
    {
        private Random Randomizer = new Random();
        private List<string> Meats = new List<string>() { "Skwarki", "Salami", "Konina", "Bycze jądra", "Kurczak marynowany w burakach" };
        private List<string> Condiments = new List<string>() { "Smalec", "Musztarda", "Czosnkowe masło", "Majonez", "Sos amerykanski", "Sos czosnkowy" };
        private List<string> Breads = new List<string>() { "Chleb żytni", "Chleb razowy", "Bułka kajzerka", "Bułka graham", "Pumpernikiel", "Ryz chlebowy" };

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<MenuItem> Menu { get; private set; }
        public DateTime GeneratedDate { get; set; }
        public int NumberOfItems { get; set; }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChangedEvent = PropertyChanged;
            if (propertyChangedEvent!=null)
            {
                propertyChangedEvent(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public MenuMaker()
            {
                Menu = new ObservableCollection<MenuItem>();
                NumberOfItems = 10;
                UpdateMenu();
            }
        public MenuItem CreateMenuItem()
        {
            string randomMeat = Meats[Randomizer.Next(Meats.Count)];
            string randomCondiment = Condiments[Randomizer.Next(Condiments.Count)];
            string randomBread = Breads[Randomizer.Next(Breads.Count)];
            return new MenuItem(randomMeat, randomCondiment, randomBread);
        }
        public void UpdateMenu()
        {
            Menu.Clear();
            for (int i = 0; i < NumberOfItems; i++)
            {
                Menu.Add(CreateMenuItem());
            }
            GeneratedDate = DateTime.Now;

            OnPropertyChanged("GeneratedDate");
        }
    }
}
