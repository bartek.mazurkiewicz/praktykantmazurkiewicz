﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Events
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kliknąłeś form!");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Click += new EventHandler(SaySomething);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Click += new EventHandler(SaySomethingElse);
        }
        private void SaySomething(object sender, EventArgs e)
        {
            MessageBox.Show("Coś", "Coś coś coś");
        }
        private void SaySomethingElse(object sender, EventArgs e)
        {
            MessageBox.Show("Coś jeszcze", "Jeszcze coś");
        }
    }
}
