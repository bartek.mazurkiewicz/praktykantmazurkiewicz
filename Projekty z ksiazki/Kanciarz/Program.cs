﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kanciarz
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter sw = new StreamWriter(@"C:\Users\cp24\source\repos\Wszystko\tajny_plan.txt");
            sw.WriteLine("W jaki sposób pokonać Kapitana Wspaniałego?");
            sw.WriteLine("Kolejny genialny, tajny plan Kanciarza.");
            sw.Write("Stworzę armię klonów, ");
            sw.WriteLine("uwolnię je i wystawię przeciwko mieszkańcom Obiektowa.");

            string[] location = new string[3] { "centrum handlowe.", "centrum miasta.", "kościół obiektowa" };

           
            for (int y = 0; y < location.Length; y++)
            {
                sw.WriteLine("Klon numer {0} atakuje {1}", y + 1, location[y]);
            }
            sw.Close();

            string folder = @"C:\Users\cp24\source\repos\Wszystko\";
            StreamReader reader = new StreamReader(folder + "tajny_plan.txt");
            StreamWriter writer = new StreamWriter(folder + "e-maildoKapitanaWspaniałego.txt");

            writer.WriteLine("To: KapitanWspanialy@obiektowo.pl");
            writer.WriteLine("From: Komisarz@obiektowo.pl");
            writer.WriteLine("Czy możesz ocalić znowu świat?");
            writer.WriteLine("");
            writer.WriteLine("Odkryliśmy plan Kanciarza:");
            while (!reader.EndOfStream)
            {
                string lineFromThePLan = reader.ReadLine();
                writer.WriteLine("Plan ->" + lineFromThePLan);
            }
            writer.WriteLine();
            writer.WriteLine("Czy możesz nam pomóc, Kapitanie?");
            writer.Close();
            reader.Close();

            sw.Close();
        }
    }
}
