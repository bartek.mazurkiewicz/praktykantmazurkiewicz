﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Listy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shoe> shoeCloset = new List<Shoe>();

            shoeCloset.Add(new Shoe()
            { Style = Style.Sneakers, Color = "Czarny" });
            shoeCloset.Add(new Shoe()
            { Style = Style.Clogs, Color = "Biały" });
            shoeCloset.Add(new Shoe()
            { Style = Style.Flipflops, Color = "Czarny" });
            shoeCloset.Add(new Shoe()
            { Style = Style.Wingtips, Color = "Brązowy" });
            shoeCloset.Add(new Shoe()
            { Style = Style.Loafers, Color = "Zółty" });
            shoeCloset.Add(new Shoe()
            { Style = Style.Sneakers, Color = "Różowy" });

            int numberOfShoes = shoeCloset.Count;
            foreach (Shoe shoe in shoeCloset)
            {
                shoe.Style = Style.Flipflops;
                shoe.Color = "Pomarańczowy";
            }

            shoeCloset.RemoveAt(4);

            Shoe thirdShoe = shoeCloset[2];
            Shoe secondShoe = shoeCloset[1];
            shoeCloset.Clear();

            shoeCloset.Add(secondShoe);

            if (shoeCloset.Contains(secondShoe))
            {
                Console.WriteLine("A to ci niespodzianka.");
                Console.ReadKey();
            }
        }
    }
}
