﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaczki
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Duck> ducks = new List<Duck>()
            {
                new Duck { Kind = KindOfDuck.Mallard, Size = 16},
                new Duck { Kind = KindOfDuck.Mallard, Size = 18},
                new Duck { Kind = KindOfDuck.Muscovy, Size = 14},
                new Duck { Kind = KindOfDuck.Mallard, Size = 12},
                new Duck { Kind = KindOfDuck.Decoy, Size = 27},
                new Duck { Kind = KindOfDuck.Muscovy, Size = 19},
                new Duck { Kind = KindOfDuck.Decoy, Size = 17},
                new Duck { Kind = KindOfDuck.Decoy, Size = 24},
                new Duck { Kind = KindOfDuck.Mallard, Size = 23},
                

            };
            DuckCompareBySize sizeComparer = new DuckCompareBySize();

            Console.WriteLine("Nieposortowane: ");
            Console.WriteLine("");

            PrintDucks(ducks);

            ducks.Sort(sizeComparer);

            Console.WriteLine("");
            Console.WriteLine("Posortowane względem wielkości: ");
            Console.WriteLine("");

            PrintDucks(ducks);

            DuckComparerByKind kindComparer = new DuckComparerByKind();

            ducks.Sort(kindComparer);

            Console.WriteLine("");
            Console.WriteLine("Posortowane względem typu:");
            Console.WriteLine("");

            PrintDucks(ducks);

            TheBestDuckComparer allComparer = new TheBestDuckComparer();

            allComparer.SortBy = SortCriteria.KindThenSize;
            ducks.Sort(allComparer);
            Console.WriteLine("");
            Console.WriteLine("Posortowane względem typu, a następnie wielkości:");
            Console.WriteLine("");
            PrintDucks(ducks);

            allComparer.SortBy = SortCriteria.SizeThenKind;
            ducks.Sort(allComparer);
            Console.WriteLine("");
            Console.WriteLine("Posortowane względem wielkości, a następnie typu:");
            Console.WriteLine("");
            PrintDucks(ducks);

            Console.ReadKey();
           

        }
        public static void PrintDucks(List <Duck> ducks)
        {
            foreach(Duck duck in ducks)
            {
                Console.WriteLine(duck.Size.ToString() + "-cm kaczka typu " + duck.Kind.ToString()+ "\n");
                
            }
        }
    }
}
