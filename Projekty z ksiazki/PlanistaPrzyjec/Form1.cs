﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlanistaPrzyjec
{
    public partial class Form1 : Form
    {
        DinnerParty dinnerParty;
        BirthdayParty birthdayParty;
        public Form1()
        {
            InitializeComponent();
            dinnerParty = new DinnerParty((int)people.Value, health.Checked, decoration.Checked);
            DisplayDinnerPartyCost();
            birthdayParty = new BirthdayParty((int)peopleBirthday.Value, decorationBirthday.Checked, cakeWriting.Text);
            DisplayBirthdayPartyCost();
           
            
        }
        void DisplayDinnerPartyCost()
        {
            decimal Cost = dinnerParty.Cost;
            cost.Text = Cost.ToString("c");
        }
        void DisplayBirthdayPartyCost()
        {
            tooLongLabel.Visible = birthdayParty.CakeWritingTooLong;
            decimal Cost = birthdayParty.Cost;
            costBirthday.Text = Cost.ToString("c");
        }


        private void People_ValueChanged(object sender, EventArgs e)
        {
            dinnerParty.NumberOfPeople = (int)people.Value;
            DisplayDinnerPartyCost();
        }

        private void Decoration_CheckedChanged(object sender, EventArgs e)
        {
            dinnerParty.FancyDecorations = decoration.Checked;
            DisplayDinnerPartyCost();
        }

        private void Health_CheckedChanged(object sender, EventArgs e)
        {
            dinnerParty.HealthyOption = health.Checked;
            DisplayDinnerPartyCost();
        }

        private void PeopleBirthday_ValueChanged(object sender, EventArgs e)
        {
            birthdayParty.NumberOfPeople = (int)peopleBirthday.Value;
            DisplayBirthdayPartyCost();
        }

        private void DecorationBirthday_CheckedChanged(object sender, EventArgs e)

        {
            birthdayParty.FancyDecorations = decorationBirthday.Checked;
            DisplayBirthdayPartyCost();
        }

        private void CakeWriting_TextChanged(object sender, EventArgs e)
        {
            birthdayParty.CakeWriting = cakeWriting.Text;
            DisplayBirthdayPartyCost();
        }
    }
}
