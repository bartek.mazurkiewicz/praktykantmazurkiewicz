﻿namespace PlanistaPrzyjec
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.people = new System.Windows.Forms.NumericUpDown();
            this.decoration = new System.Windows.Forms.CheckBox();
            this.health = new System.Windows.Forms.CheckBox();
            this.Koszt = new System.Windows.Forms.Label();
            this.cost = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cakeWriting = new System.Windows.Forms.TextBox();
            this.tooLongLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.costBirthday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.decorationBirthday = new System.Windows.Forms.CheckBox();
            this.peopleBirthday = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.people)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBirthday)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ilość osób";
            // 
            // people
            // 
            this.people.Location = new System.Drawing.Point(6, 26);
            this.people.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.people.Name = "people";
            this.people.Size = new System.Drawing.Size(64, 20);
            this.people.TabIndex = 1;
            this.people.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.people.ValueChanged += new System.EventHandler(this.People_ValueChanged);
            // 
            // decoration
            // 
            this.decoration.AutoSize = true;
            this.decoration.Checked = true;
            this.decoration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.decoration.Location = new System.Drawing.Point(9, 52);
            this.decoration.Name = "decoration";
            this.decoration.Size = new System.Drawing.Size(126, 17);
            this.decoration.TabIndex = 2;
            this.decoration.Text = "Dekoracje fantazyjne";
            this.decoration.UseVisualStyleBackColor = true;
            this.decoration.CheckedChanged += new System.EventHandler(this.Decoration_CheckedChanged);
            // 
            // health
            // 
            this.health.AutoSize = true;
            this.health.Location = new System.Drawing.Point(9, 75);
            this.health.Name = "health";
            this.health.Size = new System.Drawing.Size(91, 17);
            this.health.TabIndex = 3;
            this.health.Text = "Opcja zdrowa";
            this.health.UseVisualStyleBackColor = true;
            this.health.CheckedChanged += new System.EventHandler(this.Health_CheckedChanged);
            // 
            // Koszt
            // 
            this.Koszt.AutoSize = true;
            this.Koszt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Koszt.Location = new System.Drawing.Point(6, 96);
            this.Koszt.Name = "Koszt";
            this.Koszt.Size = new System.Drawing.Size(38, 13);
            this.Koszt.TabIndex = 4;
            this.Koszt.Text = "Koszt";
            // 
            // cost
            // 
            this.cost.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cost.Location = new System.Drawing.Point(55, 95);
            this.cost.Name = "cost";
            this.cost.Size = new System.Drawing.Size(65, 13);
            this.cost.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(194, 201);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.Koszt);
            this.tabPage1.Controls.Add(this.cost);
            this.tabPage1.Controls.Add(this.people);
            this.tabPage1.Controls.Add(this.decoration);
            this.tabPage1.Controls.Add(this.health);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(186, 175);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Okolicznosc";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cakeWriting);
            this.tabPage2.Controls.Add(this.tooLongLabel);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.costBirthday);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.decorationBirthday);
            this.tabPage2.Controls.Add(this.peopleBirthday);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(186, 175);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Urodziny";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cakeWriting
            // 
            this.cakeWriting.Location = new System.Drawing.Point(10, 94);
            this.cakeWriting.Name = "cakeWriting";
            this.cakeWriting.Size = new System.Drawing.Size(100, 20);
            this.cakeWriting.TabIndex = 8;
            this.cakeWriting.Text = "Sto lat!";
            this.cakeWriting.TextChanged += new System.EventHandler(this.CakeWriting_TextChanged);
            // 
            // tooLongLabel
            // 
            this.tooLongLabel.AutoSize = true;
            this.tooLongLabel.BackColor = System.Drawing.Color.OrangeRed;
            this.tooLongLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tooLongLabel.Location = new System.Drawing.Point(87, 77);
            this.tooLongLabel.Name = "tooLongLabel";
            this.tooLongLabel.Size = new System.Drawing.Size(72, 13);
            this.tooLongLabel.TabIndex = 7;
            this.tooLongLabel.Text = "ZBYT DŁUGI";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Napis na torcie";
            // 
            // costBirthday
            // 
            this.costBirthday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.costBirthday.Location = new System.Drawing.Point(47, 118);
            this.costBirthday.Name = "costBirthday";
            this.costBirthday.Size = new System.Drawing.Size(57, 13);
            this.costBirthday.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Koszt";
            // 
            // decorationBirthday
            // 
            this.decorationBirthday.AutoSize = true;
            this.decorationBirthday.Location = new System.Drawing.Point(7, 57);
            this.decorationBirthday.Name = "decorationBirthday";
            this.decorationBirthday.Size = new System.Drawing.Size(126, 17);
            this.decorationBirthday.TabIndex = 2;
            this.decorationBirthday.Text = "Dekoracje fantazyjne";
            this.decorationBirthday.UseVisualStyleBackColor = true;
            this.decorationBirthday.CheckedChanged += new System.EventHandler(this.DecorationBirthday_CheckedChanged);
            // 
            // peopleBirthday
            // 
            this.peopleBirthday.Location = new System.Drawing.Point(7, 30);
            this.peopleBirthday.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.peopleBirthday.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.peopleBirthday.Name = "peopleBirthday";
            this.peopleBirthday.Size = new System.Drawing.Size(57, 20);
            this.peopleBirthday.TabIndex = 1;
            this.peopleBirthday.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.peopleBirthday.ValueChanged += new System.EventHandler(this.PeopleBirthday_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ilość osób:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 198);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Planista przyjęć";
            ((System.ComponentModel.ISupportInitialize)(this.people)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBirthday)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown people;
        private System.Windows.Forms.CheckBox decoration;
        private System.Windows.Forms.CheckBox health;
        private System.Windows.Forms.Label Koszt;
        private System.Windows.Forms.Label cost;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label costBirthday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox decorationBirthday;
        private System.Windows.Forms.NumericUpDown peopleBirthday;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cakeWriting;
        private System.Windows.Forms.Label tooLongLabel;
        private System.Windows.Forms.Label label4;
    }
}

