﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Invaders.Model
{
    class InvadersModel
    {
        public readonly static Size PlayAreSize = new Size(400, 300);
        public const int MaximumPlayerShots = 3;
        public const int InitialStarCount = 50;

        private readonly Random _random = new Random();

        public int Score { get; private set; }
        public int Wave { get; private set; }
        public int Lives { get; private set; }

        public bool GameOver { get; private set; }

        private DateTime? _playerDied = null;
        public bool PlayerDying { get { return _playerDied.HasValue; } }
        private Player _player;

        private readonly List<Invader> _invaders = new List<Invader>();
        private readonly List<Shot> _playerShots = new List<Shot>();
        private readonly List<Shot> _invaderShots = new List<Shot>();
        private readonly List<Point> _stars = new List<Point>();

        private readonly Dictionary<InvaderType, int> _invaderScores = new Dictionary<InvaderType, int>()
        {
            { InvaderType.Star, 10 },
            { InvaderType.Satellite, 20 },
            { InvaderType.Saucer, 30 },
            { InvaderType.Bug, 40 },
            { InvaderType.Spaceship, 50 },
            
        };



        private Direction _invaderDirection = Direction.Left;
        private bool _justMovedDown = false;

        private DateTime _lastUpdated = DateTime.MinValue;

        public InvadersModel()
        {
            EndGame();
        }
        public void EndGame()
        {
            GameOver = true;
        }
        public void StartGame()
        {
            GameOver = false;

            foreach (Invader invader in _invaders)
            {
                OnShipChanged(invader as Ship, true);
                
            }
            _invaders.Clear();

            foreach (Shot shot in _invaderShots)
            {
                OnShotMoved(shot, true);
            }
            _invaderShots.Clear();
            foreach (Shot shot in _playerShots)
            {
                OnShotMoved(shot, true);
            }
            _playerShots.Clear();
            foreach (Point star in _stars)
            {
                OnStarChanged(star, true);
            }
            _stars.Clear();
            for (int i = 0; i < InitialStarCount; i++)
            {
                AddStar();
            }
            _player = new Player();
            OnShipChanged(_player, false);

            Lives = 2;
            Wave = 0;
            Score = 0;
            NextWave();    


        }
        public event EventHandler<ShipChangedEventArgs> ShipChanged;
        public void OnShipChanged(Ship shipUpdated, bool killed)
        {
            EventHandler<ShipChangedEventArgs> shipChanged = ShipChanged;
            if (shipChanged != null)
            {
                shipChanged(this, new ShipChangedEventArgs(shipUpdated, killed));
            }
        }
        public event EventHandler<ShotMovedEventArgs> ShotMoved;

        public void OnShotMoved(Shot shot, bool disappeared)
        {
            EventHandler<ShotMovedEventArgs> shotMoved = ShotMoved;
            if (shotMoved != null)
            {
                shotMoved(this, new ShotMovedEventArgs(shot, disappeared));
            }
        }
        public event EventHandler<StarChangedEventArgs> StarChanged;
        public void OnStarChanged(Point point, bool dissapeared)
        {
            EventHandler<StarChangedEventArgs> starChanged = StarChanged;
            if (starChanged != null)
            {
                starChanged(this, new StarChangedEventArgs(point, dissapeared));
            }
        }
        private void NextWave()
        {

        }
        public void AddStar()
        {

        }
    }
}
