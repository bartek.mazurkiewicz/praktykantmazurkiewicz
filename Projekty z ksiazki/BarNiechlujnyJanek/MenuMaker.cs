﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarNiechlujnyJanek
{
    class MenuMaker
    {
        public Random Randomizer;
        string[] Meats = { "Skwarki", "Salami", "Konina", "Bycze jądra", "Kurczak marynowany w burakach" };
        string[] Condiments = { "Smalec", "Musztarda", "Czosnkowe masło", "Majonez", "Sos amerykanski", "Sos czosnkowy" };
        string[] Breads = { "Chleb żytni", "Chleb razowy", "Bułka kajzerka", "Bułka graham", "Pumpernikiel", "Ryz chlebowy" };

        public string GetMenuItem()
        {
            string randomMeat = Meats[Randomizer.Next(Meats.Length)];
            string randomCondiment = Condiments[Randomizer.Next(Condiments.Length)];
            string randomBread = Breads[Randomizer.Next(Breads.Length)];
            return randomMeat + ", " + randomCondiment + ", " + randomBread + ".";
        }
    }
}
