﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramDoZarzadzaniaWymowkami
{
    public partial class Form1 : Form
    {
        Excuse currentExcuse = new Excuse();
        private string selectedFolder = @"C:\Users\cp24\source\repos\Wszystko\ProgramDoZarzadzaniaWymowkami\Wymowki";
        private bool formChanged = false;
        Random random = new Random();
        public Form1()
        {
            InitializeComponent();
            currentExcuse.LastUsed = lastusedBox.Value;
        }

        private void Directory_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = selectedFolder;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                selectedFolder = folderBrowserDialog1.SelectedPath;
                save.Enabled = true;
                open.Enabled = true;
                randomExcuse.Enabled = true;
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(excuseBox.Text) || String.IsNullOrEmpty(resultBox.Text))
            {
                MessageBox.Show("Określ wymówke i rezultat", "Nie można zapisać pliku", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            saveFileDialog1.InitialDirectory = selectedFolder;
            saveFileDialog1.Filter = "Pliki wymówek (*.excuse)| *.excuse |Wszystkie pliki (*.*)|*.*";
            saveFileDialog1.FileName = excuseBox.Text;
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                currentExcuse.Save(saveFileDialog1.FileName);
                UpdateForm(false);
            }
        }

        private void Open_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                openFileDialog1.InitialDirectory = selectedFolder;
                openFileDialog1.Filter = "Pliki wymówek (*.excuse)| *.excuse |Wszystkie pliki (*.*)|*.*";
                openFileDialog1.FileName = excuseBox.Text;
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    currentExcuse = new Excuse(openFileDialog1.FileName);
                    UpdateForm(false);
                }
            }
        }

        private void RandomExcuse_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                currentExcuse = new Excuse(random, selectedFolder);
                UpdateForm(false);
            }
        }
        private void UpdateForm(bool changed)
        {
            if (!changed)
            {
                this.excuseBox.Text = currentExcuse.Description;
                this.resultBox.Text = currentExcuse.Results;
                this.lastusedBox.Value = currentExcuse.LastUsed;
                if (!String.IsNullOrEmpty(currentExcuse.ExcusePath))
                {
                    date.Text = File.GetLastWriteTime(currentExcuse.ExcusePath).ToString();
                }
                this.Text = "Program do zarządzania wymówkami";

            }
            else
            {
                this.Text = "Program do zarządzania wymówkami*";
            }
            this.formChanged = changed;
        }
        private bool CheckChanged()
        {
            if (formChanged)
            {
                DialogResult result = MessageBox.Show(
                    "Bieżąca wymóka nie została zapisana. Czy kontynuować?",
                    "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                {
                    return false;
                }
            }
            return true;
        }

        private void FolderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void SaveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void ExcuseBox_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Description = excuseBox.Text;
            UpdateForm(true);
        }

        private void ResultBox_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Results = resultBox.Text;
            UpdateForm(true);
        }

        private void LastusedBox_ValueChanged(object sender, EventArgs e)
        {
            currentExcuse.LastUsed = lastusedBox.Value;
            UpdateForm(true);
        }
    }
}
