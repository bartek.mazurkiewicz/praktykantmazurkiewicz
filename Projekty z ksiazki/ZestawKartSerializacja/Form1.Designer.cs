﻿namespace ZestawKartSerializacja
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.saveRandomButton = new System.Windows.Forms.Button();
            this.loadRandomButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.save1CardButton = new System.Windows.Forms.Button();
            this.save2Card = new System.Windows.Forms.Button();
            this.compareTheFiles = new System.Windows.Forms.Button();
            this.changeButton = new System.Windows.Forms.Button();
            this.loadCardButton = new System.Windows.Forms.Button();
            this.hexButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.saveButton.Location = new System.Drawing.Point(54, 54);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 105);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loadButton.Location = new System.Drawing.Point(177, 54);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 105);
            this.loadButton.TabIndex = 1;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // saveRandomButton
            // 
            this.saveRandomButton.Location = new System.Drawing.Point(54, 187);
            this.saveRandomButton.Name = "saveRandomButton";
            this.saveRandomButton.Size = new System.Drawing.Size(75, 102);
            this.saveRandomButton.TabIndex = 2;
            this.saveRandomButton.Text = "Save random number of objects";
            this.saveRandomButton.UseVisualStyleBackColor = true;
            this.saveRandomButton.Click += new System.EventHandler(this.SaveRandomButton_Click);
            // 
            // loadRandomButton
            // 
            this.loadRandomButton.Location = new System.Drawing.Point(177, 187);
            this.loadRandomButton.Name = "loadRandomButton";
            this.loadRandomButton.Size = new System.Drawing.Size(75, 102);
            this.loadRandomButton.TabIndex = 3;
            this.loadRandomButton.Text = "Load random number of objects";
            this.loadRandomButton.UseVisualStyleBackColor = true;
            this.loadRandomButton.Click += new System.EventHandler(this.LoadRandomButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(50, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "You can save and load";
            // 
            // save1CardButton
            // 
            this.save1CardButton.Location = new System.Drawing.Point(32, 295);
            this.save1CardButton.Name = "save1CardButton";
            this.save1CardButton.Size = new System.Drawing.Size(120, 23);
            this.save1CardButton.TabIndex = 5;
            this.save1CardButton.Text = "Save one card";
            this.save1CardButton.UseVisualStyleBackColor = true;
            this.save1CardButton.Click += new System.EventHandler(this.Save1CardButton_Click);
            // 
            // save2Card
            // 
            this.save2Card.Location = new System.Drawing.Point(158, 295);
            this.save2Card.Name = "save2Card";
            this.save2Card.Size = new System.Drawing.Size(113, 23);
            this.save2Card.TabIndex = 6;
            this.save2Card.Text = "Save second card";
            this.save2Card.UseVisualStyleBackColor = true;
            this.save2Card.Click += new System.EventHandler(this.Save2Card_Click);
            // 
            // compareTheFiles
            // 
            this.compareTheFiles.Location = new System.Drawing.Point(32, 323);
            this.compareTheFiles.Name = "compareTheFiles";
            this.compareTheFiles.Size = new System.Drawing.Size(75, 25);
            this.compareTheFiles.TabIndex = 7;
            this.compareTheFiles.Text = "Compare";
            this.compareTheFiles.UseVisualStyleBackColor = true;
            this.compareTheFiles.Click += new System.EventHandler(this.CompareTheFiles_Click);
            // 
            // changeButton
            // 
            this.changeButton.Location = new System.Drawing.Point(196, 325);
            this.changeButton.Name = "changeButton";
            this.changeButton.Size = new System.Drawing.Size(75, 23);
            this.changeButton.TabIndex = 8;
            this.changeButton.Text = "Change";
            this.changeButton.UseVisualStyleBackColor = true;
            this.changeButton.Click += new System.EventHandler(this.ChangeButton_Click);
            // 
            // loadCardButton
            // 
            this.loadCardButton.Location = new System.Drawing.Point(115, 324);
            this.loadCardButton.Name = "loadCardButton";
            this.loadCardButton.Size = new System.Drawing.Size(75, 23);
            this.loadCardButton.TabIndex = 9;
            this.loadCardButton.Text = "Load card ";
            this.loadCardButton.UseVisualStyleBackColor = true;
            this.loadCardButton.Click += new System.EventHandler(this.LoadCardButton_Click);
            // 
            // hexButton
            // 
            this.hexButton.Location = new System.Drawing.Point(115, 353);
            this.hexButton.Name = "hexButton";
            this.hexButton.Size = new System.Drawing.Size(75, 23);
            this.hexButton.TabIndex = 10;
            this.hexButton.Text = "Hex";
            this.hexButton.UseVisualStyleBackColor = true;
            this.hexButton.Click += new System.EventHandler(this.HexButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 375);
            this.Controls.Add(this.hexButton);
            this.Controls.Add(this.loadCardButton);
            this.Controls.Add(this.changeButton);
            this.Controls.Add(this.compareTheFiles);
            this.Controls.Add(this.save2Card);
            this.Controls.Add(this.save1CardButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loadRandomButton);
            this.Controls.Add(this.saveRandomButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.saveButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button saveRandomButton;
        private System.Windows.Forms.Button loadRandomButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button save1CardButton;
        private System.Windows.Forms.Button save2Card;
        private System.Windows.Forms.Button compareTheFiles;
        private System.Windows.Forms.Button changeButton;
        private System.Windows.Forms.Button loadCardButton;
        private System.Windows.Forms.Button hexButton;
    }
}

