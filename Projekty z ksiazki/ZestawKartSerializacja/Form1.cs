﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZestawKartSerializacja
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        byte[] firstFile = File.ReadAllBytes("karta1.dat");
        byte[] secondFile = File.ReadAllBytes("karta2.dat");
        public Form1()
        {
            InitializeComponent();
            
           
        }
        private Deck RandomDeck(int number)
        {
            Deck myDeck = new Deck(new Card[] { });
            for (int i = 0; i < number; i++)
            {
                myDeck.Add(new Card(
                    (Suits)random.Next(4),
                    (Values)random.Next(1, 14)));
            }
            return myDeck;
        }
        private void DealCards(Deck deckToDeal, string Title)
        {
            string line = "-------------------------";
            Console.WriteLine(Title);
            while (deckToDeal.Count>0)
            {
                Card nextCard = deckToDeal.Deal(0);
                Console.WriteLine(nextCard.Name);
            }
            Console.WriteLine(line);
        }
        private void CompareBytes()
        {
            
            for (int i = 0; i < firstFile.Length; i++)
            {
                if (firstFile[i] != secondFile[i])
                {
                    Console.WriteLine("Bajt numer {0}: {1} i {2}", i, firstFile[1], secondFile[i]);
                }
            }
        }
        private void SaveButton_Click(object sender, EventArgs e)
        {
            Deck deckToWrite = RandomDeck(5);
            using (Stream output = File.Create("Zestaw1.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, deckToWrite);
            }
            DealCards(deckToWrite, "To, co zapisałem do pliku");
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            Deck deckFromFile;
            using (Stream input = File.OpenRead("Zestaw1.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                deckFromFile = (Deck)bf.Deserialize(input);
                DealCards(deckFromFile, "To, co z pliku odczytałem");
            }
        }

        private void SaveRandomButton_Click(object sender, EventArgs e)
        {
            using (Stream output = File.Create("Zestaw2.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                for (int i = 1; i <= 5; i++)
                {
                    Deck deckToWrite = RandomDeck(random.Next(1, 10));
                    bf.Serialize(output, deckToWrite);
                    DealCards(deckToWrite, "Zestaw numer" + i + " zapisany");
                }
            }
        }

        private void LoadRandomButton_Click(object sender, EventArgs e)
        {
            using (Stream input = File.OpenRead("Zestaw2.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                for (int i = 1; i <= 5; i++)
                {
                    Deck deckToRead = (Deck)bf.Deserialize(input);
                   
                    DealCards(deckToRead, "Zestaw numer" + i + " odczytany");
                }
            }
        }

        private void Save1CardButton_Click(object sender, EventArgs e)
        {
            using(Stream output = File.Create("karta1.dat"))
            {
                Card cardToSave = new Card(Suits.Clubs, Values.Three);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, cardToSave);
            }
        }

        private void Save2Card_Click(object sender, EventArgs e)
        {
            using (Stream output = File.Create("karta2.dat"))
            {
                Card cardToSave = new Card(Suits.Hearts, Values.Six);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, cardToSave);
            }
        }

        private void CompareTheFiles_Click(object sender, EventArgs e)
        {
            CompareBytes();
        }

        private void ChangeButton_Click(object sender, EventArgs e)
        {
            firstFile[307] = (byte)Suits.Spades;
            firstFile[364] = (byte)Values.King;
            File.Delete("karta3.dat");
            File.WriteAllBytes("karta3.dat", firstFile);
        }

        private void LoadCardButton_Click(object sender, EventArgs e)
        {
            Card cardToLoad;
            using (Stream input = File.OpenRead("karta3.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                cardToLoad = (Card)bf.Deserialize(input);
                MessageBox.Show(cardToLoad.Name, "Twoja karta: ");
                
            }
        }

        private void HexButton_Click(object sender, EventArgs e)
        {
            using (StreamReader reader = new StreamReader(@"C:\Users\cp24\source\repos\Wszystko\ZestawKartSerializacja\bin\Debug\karta3.dat"))
            using (StreamWriter writer = new StreamWriter(@"C:\Users\cp24\source\repos\Wszystko\ZestawKartSerializacja\bin\Debug\karta1.dat", false))
            {
                int position = 0;
                while (!reader.EndOfStream)
                {
                    char[] buffer = new char[16];
                    int charactersRead = reader.ReadBlock(buffer, 0, 16);
                    writer.Write("{0}: ", String.Format("{0:x4}", position));
                    position += charactersRead;
                    for (int i = 0; i < 16; i++)
                    {
                        if (i<charactersRead)
                        {
                            string hex = String.Format("{0:x2}", (byte)buffer[i]);
                            writer.Write(hex + " ");

                        }
                        else
                        {
                            writer.Write(" ");

                        }
                        if (i == 7)
                        {
                            writer.Write("-- ");
                        }
                        if (buffer[i] < 32 || buffer[i] > 250)
                        {
                            buffer[i] = '.';
                        }
                    }
                    string bufferContents = new string(buffer);
                    writer.WriteLine("  " + bufferContents.Substring(0, charactersRead));
                }
            }
        }
    }
}
