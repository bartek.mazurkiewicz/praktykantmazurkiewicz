﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace R3S171_Guy
{
    public partial class Form1 : Form
    {
        Guy joe;
        Guy bob;
        int bank = 1000;
       

        public void UpdateForm()
        {
            joesCash.Text = joe.Name + " ma " + joe.Cash + " zł.";
            bobsCash.Text = bob.Name + " ma " + bob.Cash + " zł.";
            bankCash.Text = "Bank ma " + bank + " zł.";
        }
        public Form1()
        {
            InitializeComponent();
            joe = new Guy();
            joe.Cash = 3;
            joe.Name = "Joe";

            bob = new Guy();
            bob.Cash = 9000;
            bob.Name = "Bob";
            UpdateForm();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (bank >=10)
            {
                bank -= joe.ReceiveCash(10);
                UpdateForm();
            }
            else
            {
                MessageBox.Show("Bank nie posiada takiej ilosci pieniedzy");
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }

        private void Joegivetobob_Click(object sender, EventArgs e)
        {
            joe.Cash -= bob.ReceiveCash(10);
            UpdateForm();
        }

        private void Bobgivestojoe_Click(object sender, EventArgs e)
        {
            bob.Cash -= joe.ReceiveCash(5);
            UpdateForm();
        }
    }
}
