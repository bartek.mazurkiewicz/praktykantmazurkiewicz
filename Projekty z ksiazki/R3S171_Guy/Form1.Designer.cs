﻿namespace R3S171_Guy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.joesCash = new System.Windows.Forms.Label();
            this.bobsCash = new System.Windows.Forms.Label();
            this.bankCash = new System.Windows.Forms.Label();
            this.joegivetobob = new System.Windows.Forms.Button();
            this.bobgivestojoe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(44, 229);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "daj joe";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(225, 228);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(91, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "zabierz bobowi";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // joesCash
            // 
            this.joesCash.AutoSize = true;
            this.joesCash.Location = new System.Drawing.Point(44, 31);
            this.joesCash.Name = "joesCash";
            this.joesCash.Size = new System.Drawing.Size(0, 13);
            this.joesCash.TabIndex = 2;
            // 
            // bobsCash
            // 
            this.bobsCash.AutoSize = true;
            this.bobsCash.Location = new System.Drawing.Point(44, 64);
            this.bobsCash.Name = "bobsCash";
            this.bobsCash.Size = new System.Drawing.Size(0, 13);
            this.bobsCash.TabIndex = 3;
            // 
            // bankCash
            // 
            this.bankCash.AutoSize = true;
            this.bankCash.Location = new System.Drawing.Point(44, 95);
            this.bankCash.Name = "bankCash";
            this.bankCash.Size = new System.Drawing.Size(0, 13);
            this.bankCash.TabIndex = 4;
            // 
            // joegivetobob
            // 
            this.joegivetobob.Location = new System.Drawing.Point(44, 296);
            this.joegivetobob.Name = "joegivetobob";
            this.joegivetobob.Size = new System.Drawing.Size(108, 23);
            this.joegivetobob.TabIndex = 5;
            this.joegivetobob.Text = "joe oddaje bobowi";
            this.joegivetobob.UseVisualStyleBackColor = true;
            this.joegivetobob.Click += new System.EventHandler(this.Joegivetobob_Click);
            // 
            // bobgivestojoe
            // 
            this.bobgivestojoe.Location = new System.Drawing.Point(225, 296);
            this.bobgivestojoe.Name = "bobgivestojoe";
            this.bobgivestojoe.Size = new System.Drawing.Size(75, 23);
            this.bobgivestojoe.TabIndex = 6;
            this.bobgivestojoe.Text = "bob daje joe";
            this.bobgivestojoe.UseVisualStyleBackColor = true;
            this.bobgivestojoe.Click += new System.EventHandler(this.Bobgivestojoe_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 360);
            this.Controls.Add(this.bobgivestojoe);
            this.Controls.Add(this.joegivetobob);
            this.Controls.Add(this.bankCash);
            this.Controls.Add(this.bobsCash);
            this.Controls.Add(this.joesCash);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label joesCash;
        private System.Windows.Forms.Label bobsCash;
        private System.Windows.Forms.Label bankCash;
        private System.Windows.Forms.Button joegivetobob;
        private System.Windows.Forms.Button bobgivestojoe;
    }
}

