﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptaki
{
    class Duck : Bird
    {
        public int Size;
        public KindOfDuck Kind;
        public override string ToString()
        {
            return Size + "-cm kaczka typu " + Kind; 
        }


    }
}
