﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptaki
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Duck> ducks = new List<Duck>()
            {
                new Duck { Kind = KindOfDuck.Mallard, Size = 16 },
                new Duck { Kind = KindOfDuck.Mallard, Size = 18 },
                new Duck { Kind = KindOfDuck.Muscovy, Size = 14 },
                new Duck { Kind = KindOfDuck.Mallard, Size = 12 },
                new Duck { Kind = KindOfDuck.Decoy, Size = 27 },
                new Duck { Kind = KindOfDuck.Muscovy, Size = 19 },
                new Duck { Kind = KindOfDuck.Decoy, Size = 17 },
                new Duck { Kind = KindOfDuck.Decoy, Size = 24 },
                new Duck { Kind = KindOfDuck.Mallard, Size = 23 },
                

            };
            IEnumerable<Bird> upcastDucks = ducks;

            List<Bird> birds = new List<Bird>();
            birds.Add(new Bird() { Name = "Feathers" });
            birds.AddRange(upcastDucks);
            birds.Add(new Penguin() { Name = "George" });

            foreach (Bird bird in birds)
            {
                Console.WriteLine(bird);
            }
            
            Console.ReadKey();
        }
    }
}
