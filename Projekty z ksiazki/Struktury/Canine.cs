﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Struktury
{
    class Canine
    {
        public string Name;
        public string Breed;
        public Canine(string name, string breed)
        {
            this.Name = name;
            this.Breed = breed;
        }
        public void Speak()
        {
            MessageBox.Show("Wabię się " + Name + ". Moja rasa to " + Breed + ".");
        }
    }
}
