﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Struktury
{
    public partial class Form1 : Form
    {
        Canine spot;
        Canine bob;
        Dog jake;
        Dog betty;
        public Form1()
        {
            InitializeComponent();
            spot = new Canine("Burek", "Mops");
            bob = spot;
            jake = new Dog("Tofik", "Pudel");
            betty = jake;
            bob.Name = "Szarik";
            bob.Breed = "Beagle";
            betty.Name = "Becia";
            betty.Breed = "Pitbul";
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            jake.Speak();
            spot.Speak();
        }
    }
}
