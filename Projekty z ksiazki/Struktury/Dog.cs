﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Struktury
{
    public struct Dog
    {
        public string Name;
        public string Breed;
        public Dog(string name, string breed)
        {
            this.Name = name;
            this.Breed = breed;
        }
        public void Speak()
        {
            MessageBox.Show("Wabię się " + Name + ". Moja rasa to " + Breed + ".");
        }
    }
}
