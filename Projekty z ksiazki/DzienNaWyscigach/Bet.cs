﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DzienNaWyscigach
{
    class Bet
    {
        public int Amount;
        public int Dog;
        public Guy bettor;

        public string GetDescription()
        {
            if (Amount > 0)
            {
                return bettor.Name + " postawil " + Amount + " na psa numer " + Dog + ".";
            }
            else
            {
                return bettor.Name + " nie zawarl zakladu.";
            }
        }

        public int PayOut(int Winner)
        {
            if (Winner == Dog)
            {
                return Amount;
            }
            else
            {
                return -Amount;
            }
        }
    }
}
