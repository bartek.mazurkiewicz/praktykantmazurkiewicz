﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DzienNaWyscigach
{
    class Guy
    {
        public string Name;
        public Bet MyBet;
        public int Cash;


        public RadioButton MyRadioButton;
        public Label MyLabel;

        public void UpdateLabels()
        {
            MyRadioButton.Text = Name + " ma " + Cash + " zł";
            MyLabel.Text = MyBet.GetDescription();
        }
        public void ClearBet()
        {
            PlaceBet(0, 0);
;       }
        public bool PlaceBet(int BetAmount, int DogToWin)
        {
            if (BetAmount <= Cash)
            {
                MyBet = new Bet() { Amount = BetAmount, Dog = DogToWin, bettor = this };
                UpdateLabels();
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Collect(int winner)
        {
            Cash += MyBet.PayOut(winner);
            UpdateLabels();
        }
    }
}
