﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DzienNaWyscigach
{
    public partial class Form1 : Form
    {
        Greyhound[] greyhound = new Greyhound[4];
        Guy[] guy = new Guy[3];
        Random MyRandomizer = new Random();
        public Form1()
        {
            InitializeComponent();
            greyhound[0] = new Greyhound()
            {
                MyPictureBox = dog1,
                StartingPosition = racetrackPictureBox.Left,
                Racetracklenght = racetrackPictureBox.Width - dog1.Width,
                MyRandom = MyRandomizer

            };
            greyhound[1] = new Greyhound()
            {
                MyPictureBox = dog2,
                StartingPosition = racetrackPictureBox.Left,
                Racetracklenght = racetrackPictureBox.Width - dog2.Width,
                MyRandom = MyRandomizer

            };
            greyhound[2] = new Greyhound()
            {
                MyPictureBox = dog3,
                StartingPosition = racetrackPictureBox.Left,
                Racetracklenght = racetrackPictureBox.Width - dog3.Width,
                MyRandom = MyRandomizer

            };
            greyhound[3] = new Greyhound()
            {
                MyPictureBox = dog4,
                StartingPosition = racetrackPictureBox.Left,
                Racetracklenght = racetrackPictureBox.Width - dog4.Width,
                MyRandom = MyRandomizer

            };
            guy[0] = new Guy() { Name = "Janek", Cash = 8000, MyRadioButton = joeRadioButton, MyLabel = joeBetLabel, MyBet = null };
            guy[1] = new Guy() { Name = "Bartek", Cash = 15000, MyRadioButton = bobRadioButton, MyLabel = bobBetLabel, MyBet = null };
            guy[2] = new Guy() { Name = "Arek", Cash = 6000, MyRadioButton = alRadioButton, MyLabel = alBetLabel, MyBet = null };

            minimumBetLabel.Text = "Minimalny zakład: " + cashBet.Minimum + " zł";
            refreshGuyState();
        }
        public void refreshGuyState()
        {
            for (int i = 0; i < guy.Length; i++)
            {
                guy[i].ClearBet();
            }
        }

        private void JoeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            name.Text = guy[0].Name;
        }

        private void BobRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            name.Text = guy[1].Name;
        }

        private void AlRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            name.Text = guy[2].Name;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (joeRadioButton.Checked)
                guy[0].PlaceBet((int)cashBet.Value, (int)dogBet.Value);
            if (bobRadioButton.Checked)
                guy[1].PlaceBet((int)cashBet.Value, (int)dogBet.Value);
            if (alRadioButton.Checked)
                guy[2].PlaceBet((int)cashBet.Value, (int)dogBet.Value);

        }
            
        private void Button1_Click(object sender, EventArgs e)
        {
            for (int l = 0; l < greyhound.Length; l++)
            {
                greyhound[l].TakeStartingPosition();
            }
            bettingGroup.Enabled = false;
            timer1.Start();   
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            int superpies = 0;
            for (int i = 0; i < greyhound.Length; i++)
            {
                if (greyhound[i].Run())
                {
                    timer1.Stop();
                    superpies = i + 1;
                    MessageBox.Show("Pies numer " + superpies + " wygrał wyścig!");
                    for (int b = 0; b < guy.Length; b++)
                    {
                        guy[b].Collect(superpies);
                    }
                    refreshGuyState();
                    bettingGroup.Enabled = true;
                    break;

                }
            }
        }

       
    }
}
