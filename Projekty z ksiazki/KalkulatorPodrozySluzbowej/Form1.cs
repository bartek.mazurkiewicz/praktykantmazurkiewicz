﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KalkulatorPodrozySluzbowej
{
    public partial class Form1 : Form
    {
        int startingMileage;
        int endingMileage;
        double milesTraveled;
        double reimburseRate = 0.39;
        double amount0wned;
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            startingMileage = (int)numericUpDown1.Value;
            endingMileage = (int)numericUpDown2.Value;
            if(startingMileage>=endingMileage)
            {
                MessageBox.Show("Poczatkowy stan musi byc mniejszy niz koncowy");
                label4.Text = "Błąd!";

            }
            else
            {
                milesTraveled = endingMileage - startingMileage;
                amount0wned = milesTraveled * reimburseRate;
                label4.Text = amount0wned.ToString() + " zł";
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(milesTraveled + " km", "Przebyta odleglosc");
        }
    }
}
