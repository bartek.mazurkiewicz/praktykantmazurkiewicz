﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods
{
    class OrdinaryHuman
    {
        private int age;
        int weight;
        
        public OrdinaryHuman(int age, int weight)
        {
            this.age = age;
            this.weight = weight;
        }
        public void GoToWork(int howMuchTime)
        {
            Console.WriteLine("Ide do pracy na " + howMuchTime + " godzin.");
        }
        public void PayTaxes(string institution, int money)
        {
            Console.WriteLine("Idę zapłacić podatki instytucji " + institution + " o wartości " + money + " złotych");
        }
    }
}
