﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            OrdinaryHuman ordinaryHuman = new OrdinaryHuman(25, 180);
            Console.WriteLine(ordinaryHuman.BreakWalls(150));
            ordinaryHuman.GoToWork(8);
            ordinaryHuman.PayTaxes("ZUS", 1500);
            Console.ReadKey();
        }
    }
}
