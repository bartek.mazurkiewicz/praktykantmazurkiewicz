﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elephant
{
 
    public partial class Form1 : Form
    {
        Elephant lloyd = new Elephant()
        { EarSize = 40, Name = "Lloyd" };
        Elephant lucinda = new Elephant()
        { EarSize = 33, Name = "Lucinda" };
        Elephant holder = new Elephant();

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            lloyd.WhoAmI();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            lucinda.WhoAmI();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            holder = lloyd;
            lloyd = lucinda;
            lucinda = holder;
            MessageBox.Show("Obiekty zamienione");
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            lucinda.SpeakTo(lloyd, "Witaj");
            lloyd.TellMe("Cześć", lucinda);
           // lloyd = lucinda;
           // lloyd.EarSize = 4321;f 
           // lloyd.WhoAmI();
        }
    }
}
