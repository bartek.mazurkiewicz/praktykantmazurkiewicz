﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baseball
{
    class Fan
    {
        public ObservableCollection<string> FanSays = new ObservableCollection<string>();
        private int pitchNumber = 0;

        public Fan(Ball ball)
        {
            ball.BallInPlay += new EventHandler(Ball_BallInPlay);

        }
        void Ball_BallInPlay(object sender, EventArgs e)
        {
            pitchNumber++;
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if (ballEventArgs.Distance > 400 && ballEventArgs.Trajectory > 30)
                {
                    FanSays.Add("Rzut #" + pitchNumber + ": Idę po piłeczkę!");
                }
                else
                {
                    FanSays.Add("Rzut #" + pitchNumber + ": Juhu! O tak!");
                }
            }
        }
    }
}
