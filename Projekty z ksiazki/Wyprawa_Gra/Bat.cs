﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyprawa_Gra
{
    class Bat : Enemy
    {
        public Bat(Game game, Point location) : base(game, location, 20)
        {
            
        }

        public override void Move(Random random)
        {
            
            int chance = random.Next(2);
            if (HitPoints > 0)
            {
                if (chance == 1)
                {
                    location = Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
                }
                else
                {
                    location = Move((Direction)random.Next(0, 5), game.Boundaries);
                }
                if (NearPlayer())
                {
                    game.HitPlayer(15, random);
                }
            }
        }
    }
}
