﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyprawa_Gra
{
    class RedPotion : Weapon, IPotion
    {
        public RedPotion(Game game, Point location) : base(game, location)
        {
            Used = false;
        }

        public bool Used { get; private set; }

        public override string Name
        {
            get
            {
                return "Czerwona mikstura";
            }
        }

        public override void Attack(Direction direction, Random random)
        {
            game.IncreasePlayerHealth(35, random);
            Used = true;
        }
    }
}
