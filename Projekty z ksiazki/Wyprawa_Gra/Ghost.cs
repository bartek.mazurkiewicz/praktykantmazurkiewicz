﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyprawa_Gra
{
    class Ghost : Enemy
    {
        
        public Ghost(Game game, Point location) : base(game, location, 95)
        {

        }
        public override void Move(Random random)
        {
            if (HitPoints > 0)
            {
                if (random.Next(0,9) !=1)
                {
                    location = Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
                }
                if(NearPlayer())
                {
                    game.HitPlayer(1, random);
                }
            }
        }
    }
}
