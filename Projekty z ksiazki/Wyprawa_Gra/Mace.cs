﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyprawa_Gra
{
    class Mace : Weapon
    {
        public Mace(Game game, Point location) : base(game, location)
        {
        }

        public override string Name
        {
            get
            {
                return "Buława";
            }
        }

        public override void Attack(Direction direction, Random random)
        {
            if (direction == Direction.Right)
            {
                if (DamageEnemy(direction, 5, 12, random) == false)
                {
                    if (DamageEnemy(Direction.Down, 5, 12, random) == false)
                    {
                        if (DamageEnemy(Direction.Left, 5, 12, random) == false)
                        {
                            DamageEnemy(Direction.Up, 5, 12, random);
                        }
                    }
                }
            }
            if (direction == Direction.Left)
            {
                if (DamageEnemy(direction, 5, 12, random) == false)
                {
                    if (DamageEnemy(Direction.Up, 5, 12, random) == false)
                    {
                        if (DamageEnemy(Direction.Right, 5, 12, random) == false)
                        {
                            DamageEnemy(Direction.Down, 5, 12, random);
                        }
                    }
                }
            }
            if (direction == Direction.Down)
            {
                if (DamageEnemy(direction, 5, 12, random) == false)
                {
                    if (DamageEnemy(Direction.Left, 5, 12, random) == false)
                    {
                        if (DamageEnemy(Direction.Up, 5, 12, random) == false)
                        {
                            DamageEnemy(Direction.Right, 5, 12, random);
                        }
                    }
                }
            }
            if (direction == Direction.Up)
            {
                if (DamageEnemy(direction, 5, 12, random) == false)
                {
                    if (DamageEnemy(Direction.Right, 5, 12, random) == false)
                    {
                        if (DamageEnemy(Direction.Down, 5, 12, random) == false)
                        {
                            DamageEnemy(Direction.Left, 5, 12, random);
                        }
                    }
                }
            }
        }
    }
}
