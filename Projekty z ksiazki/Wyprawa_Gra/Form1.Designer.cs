﻿namespace Wyprawa_Gra
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.player = new System.Windows.Forms.PictureBox();
            this.potion_blue = new System.Windows.Forms.PictureBox();
            this.bow = new System.Windows.Forms.PictureBox();
            this.mace = new System.Windows.Forms.PictureBox();
            this.potion_red = new System.Windows.Forms.PictureBox();
            this.bat = new System.Windows.Forms.PictureBox();
            this.ghost = new System.Windows.Forms.PictureBox();
            this.ghoul = new System.Windows.Forms.PictureBox();
            this.sword = new System.Windows.Forms.PictureBox();
            this.swordEq = new System.Windows.Forms.PictureBox();
            this.bowEq = new System.Windows.Forms.PictureBox();
            this.maceEq = new System.Windows.Forms.PictureBox();
            this.potion_blueEq = new System.Windows.Forms.PictureBox();
            this.potion_redEq = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.playerHitPoints = new System.Windows.Forms.Label();
            this.batHitPoints = new System.Windows.Forms.Label();
            this.ghostHitPoints = new System.Windows.Forms.Label();
            this.ghoulHitPoints = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghoul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.swordEq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowEq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maceEq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_blueEq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_redEq)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // player
            // 
            this.player.BackColor = System.Drawing.Color.Transparent;
            this.player.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("player.BackgroundImage")));
            this.player.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.player.Location = new System.Drawing.Point(113, 81);
            this.player.Margin = new System.Windows.Forms.Padding(4);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(50, 50);
            this.player.TabIndex = 0;
            this.player.TabStop = false;
            // 
            // potion_blue
            // 
            this.potion_blue.BackColor = System.Drawing.Color.Transparent;
            this.potion_blue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("potion_blue.BackgroundImage")));
            this.potion_blue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.potion_blue.Location = new System.Drawing.Point(519, 81);
            this.potion_blue.Margin = new System.Windows.Forms.Padding(4);
            this.potion_blue.Name = "potion_blue";
            this.potion_blue.Size = new System.Drawing.Size(50, 50);
            this.potion_blue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.potion_blue.TabIndex = 1;
            this.potion_blue.TabStop = false;
            // 
            // bow
            // 
            this.bow.BackColor = System.Drawing.Color.Transparent;
            this.bow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bow.BackgroundImage")));
            this.bow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bow.Location = new System.Drawing.Point(403, 81);
            this.bow.Margin = new System.Windows.Forms.Padding(4);
            this.bow.Name = "bow";
            this.bow.Size = new System.Drawing.Size(50, 50);
            this.bow.TabIndex = 2;
            this.bow.TabStop = false;
            // 
            // mace
            // 
            this.mace.BackColor = System.Drawing.Color.Transparent;
            this.mace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mace.BackgroundImage")));
            this.mace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mace.Location = new System.Drawing.Point(461, 81);
            this.mace.Margin = new System.Windows.Forms.Padding(4);
            this.mace.Name = "mace";
            this.mace.Size = new System.Drawing.Size(50, 50);
            this.mace.TabIndex = 3;
            this.mace.TabStop = false;
            // 
            // potion_red
            // 
            this.potion_red.BackColor = System.Drawing.Color.Transparent;
            this.potion_red.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("potion_red.BackgroundImage")));
            this.potion_red.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.potion_red.Location = new System.Drawing.Point(577, 81);
            this.potion_red.Margin = new System.Windows.Forms.Padding(4);
            this.potion_red.Name = "potion_red";
            this.potion_red.Size = new System.Drawing.Size(50, 50);
            this.potion_red.TabIndex = 4;
            this.potion_red.TabStop = false;
            // 
            // bat
            // 
            this.bat.BackColor = System.Drawing.Color.Transparent;
            this.bat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bat.BackgroundImage")));
            this.bat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bat.Location = new System.Drawing.Point(171, 81);
            this.bat.Margin = new System.Windows.Forms.Padding(4);
            this.bat.Name = "bat";
            this.bat.Size = new System.Drawing.Size(50, 50);
            this.bat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bat.TabIndex = 5;
            this.bat.TabStop = false;
            // 
            // ghost
            // 
            this.ghost.BackColor = System.Drawing.Color.Transparent;
            this.ghost.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ghost.BackgroundImage")));
            this.ghost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ghost.Location = new System.Drawing.Point(229, 81);
            this.ghost.Margin = new System.Windows.Forms.Padding(4);
            this.ghost.Name = "ghost";
            this.ghost.Size = new System.Drawing.Size(50, 50);
            this.ghost.TabIndex = 6;
            this.ghost.TabStop = false;
            // 
            // ghoul
            // 
            this.ghoul.BackColor = System.Drawing.Color.Transparent;
            this.ghoul.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ghoul.BackgroundImage")));
            this.ghoul.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ghoul.Location = new System.Drawing.Point(287, 81);
            this.ghoul.Margin = new System.Windows.Forms.Padding(4);
            this.ghoul.Name = "ghoul";
            this.ghoul.Size = new System.Drawing.Size(50, 50);
            this.ghoul.TabIndex = 7;
            this.ghoul.TabStop = false;
            // 
            // sword
            // 
            this.sword.BackColor = System.Drawing.Color.Transparent;
            this.sword.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sword.BackgroundImage")));
            this.sword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sword.Location = new System.Drawing.Point(345, 81);
            this.sword.Margin = new System.Windows.Forms.Padding(4);
            this.sword.Name = "sword";
            this.sword.Size = new System.Drawing.Size(50, 50);
            this.sword.TabIndex = 8;
            this.sword.TabStop = false;
            // 
            // swordEq
            // 
            this.swordEq.BackColor = System.Drawing.Color.Transparent;
            this.swordEq.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("swordEq.BackgroundImage")));
            this.swordEq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.swordEq.Location = new System.Drawing.Point(116, 416);
            this.swordEq.Margin = new System.Windows.Forms.Padding(4);
            this.swordEq.Name = "swordEq";
            this.swordEq.Size = new System.Drawing.Size(75, 75);
            this.swordEq.TabIndex = 9;
            this.swordEq.TabStop = false;
            this.swordEq.Click += new System.EventHandler(this.SwordEq_Click);
            // 
            // bowEq
            // 
            this.bowEq.BackColor = System.Drawing.Color.Transparent;
            this.bowEq.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bowEq.BackgroundImage")));
            this.bowEq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bowEq.Location = new System.Drawing.Point(199, 416);
            this.bowEq.Margin = new System.Windows.Forms.Padding(4);
            this.bowEq.Name = "bowEq";
            this.bowEq.Size = new System.Drawing.Size(75, 75);
            this.bowEq.TabIndex = 10;
            this.bowEq.TabStop = false;
            this.bowEq.Click += new System.EventHandler(this.BowEq_Click);
            // 
            // maceEq
            // 
            this.maceEq.BackColor = System.Drawing.Color.Transparent;
            this.maceEq.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("maceEq.BackgroundImage")));
            this.maceEq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.maceEq.Location = new System.Drawing.Point(282, 416);
            this.maceEq.Margin = new System.Windows.Forms.Padding(4);
            this.maceEq.Name = "maceEq";
            this.maceEq.Size = new System.Drawing.Size(75, 75);
            this.maceEq.TabIndex = 11;
            this.maceEq.TabStop = false;
            this.maceEq.Click += new System.EventHandler(this.MaceEq_Click);
            // 
            // potion_blueEq
            // 
            this.potion_blueEq.BackColor = System.Drawing.Color.Transparent;
            this.potion_blueEq.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("potion_blueEq.BackgroundImage")));
            this.potion_blueEq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.potion_blueEq.Location = new System.Drawing.Point(365, 416);
            this.potion_blueEq.Margin = new System.Windows.Forms.Padding(4);
            this.potion_blueEq.Name = "potion_blueEq";
            this.potion_blueEq.Size = new System.Drawing.Size(75, 75);
            this.potion_blueEq.TabIndex = 12;
            this.potion_blueEq.TabStop = false;
            this.potion_blueEq.Click += new System.EventHandler(this.Potion_blueEq_Click);
            // 
            // potion_redEq
            // 
            this.potion_redEq.BackColor = System.Drawing.Color.Transparent;
            this.potion_redEq.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("potion_redEq.BackgroundImage")));
            this.potion_redEq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.potion_redEq.Location = new System.Drawing.Point(452, 416);
            this.potion_redEq.Margin = new System.Windows.Forms.Padding(4);
            this.potion_redEq.Name = "potion_redEq";
            this.potion_redEq.Size = new System.Drawing.Size(75, 75);
            this.potion_redEq.TabIndex = 13;
            this.potion_redEq.TabStop = false;
            this.potion_redEq.Click += new System.EventHandler(this.Potion_redEq_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.8284F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.1716F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.playerHitPoints, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.batHitPoints, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ghostHitPoints, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.ghoulHitPoints, 1, 3);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(605, 270);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.65854F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.34146F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(193, 86);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(4, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Gracz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(4, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Nietoperz";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Location = new System.Drawing.Point(4, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Duch";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label4.Location = new System.Drawing.Point(4, 69);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Upiór";
            // 
            // playerHitPoints
            // 
            this.playerHitPoints.AutoSize = true;
            this.playerHitPoints.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.playerHitPoints.Location = new System.Drawing.Point(82, 5);
            this.playerHitPoints.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.playerHitPoints.Name = "playerHitPoints";
            this.playerHitPoints.Size = new System.Drawing.Size(107, 17);
            this.playerHitPoints.TabIndex = 19;
            // 
            // batHitPoints
            // 
            this.batHitPoints.AutoSize = true;
            this.batHitPoints.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.batHitPoints.Location = new System.Drawing.Point(82, 24);
            this.batHitPoints.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.batHitPoints.Name = "batHitPoints";
            this.batHitPoints.Size = new System.Drawing.Size(107, 17);
            this.batHitPoints.TabIndex = 20;
            // 
            // ghostHitPoints
            // 
            this.ghostHitPoints.AutoSize = true;
            this.ghostHitPoints.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ghostHitPoints.Location = new System.Drawing.Point(82, 47);
            this.ghostHitPoints.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ghostHitPoints.Name = "ghostHitPoints";
            this.ghostHitPoints.Size = new System.Drawing.Size(107, 17);
            this.ghostHitPoints.TabIndex = 21;
            // 
            // ghoulHitPoints
            // 
            this.ghoulHitPoints.AutoSize = true;
            this.ghoulHitPoints.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ghoulHitPoints.Location = new System.Drawing.Point(82, 69);
            this.ghoulHitPoints.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ghoulHitPoints.Name = "ghoulHitPoints";
            this.ghoulHitPoints.Size = new System.Drawing.Size(107, 17);
            this.ghoulHitPoints.TabIndex = 22;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel2.Controls.Add(this.button5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.button7, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.button8, 2, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(673, 423);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(109, 68);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(40, 4);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(28, 26);
            this.button5.TabIndex = 0;
            this.button5.Text = "↑";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(4, 38);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(28, 26);
            this.button6.TabIndex = 1;
            this.button6.Text = "←";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(40, 38);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(28, 26);
            this.button7.TabIndex = 2;
            this.button7.Text = "↓";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(76, 38);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(29, 26);
            this.button8.TabIndex = 3;
            this.button8.Text = "→";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.button2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.button3, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.button4, 2, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(550, 423);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(105, 68);
            this.tableLayoutPanel3.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(40, 4);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 26);
            this.button1.TabIndex = 0;
            this.button1.Text = "↑";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(4, 38);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 26);
            this.button2.TabIndex = 1;
            this.button2.Text = "←";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(40, 38);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(28, 26);
            this.button3.TabIndex = 2;
            this.button3.Text = "↓";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(76, 38);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(25, 26);
            this.button4.TabIndex = 3;
            this.button4.Text = "→";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(587, 406);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Ruch";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(713, 406);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Atak";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(872, 528);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.potion_redEq);
            this.Controls.Add(this.potion_blueEq);
            this.Controls.Add(this.maceEq);
            this.Controls.Add(this.bowEq);
            this.Controls.Add(this.swordEq);
            this.Controls.Add(this.player);
            this.Controls.Add(this.ghoul);
            this.Controls.Add(this.ghost);
            this.Controls.Add(this.bat);
            this.Controls.Add(this.potion_red);
            this.Controls.Add(this.potion_blue);
            this.Controls.Add(this.sword);
            this.Controls.Add(this.bow);
            this.Controls.Add(this.mace);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Wyprawa";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghoul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.swordEq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowEq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maceEq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_blueEq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.potion_redEq)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.PictureBox potion_blue;
        private System.Windows.Forms.PictureBox bow;
        private System.Windows.Forms.PictureBox mace;
        private System.Windows.Forms.PictureBox potion_red;
        private System.Windows.Forms.PictureBox bat;
        private System.Windows.Forms.PictureBox ghost;
        private System.Windows.Forms.PictureBox ghoul;
        private System.Windows.Forms.PictureBox sword;
        private System.Windows.Forms.PictureBox bowEq;
        private System.Windows.Forms.PictureBox maceEq;
        private System.Windows.Forms.PictureBox potion_blueEq;
        private System.Windows.Forms.PictureBox potion_redEq;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label playerHitPoints;
        private System.Windows.Forms.Label batHitPoints;
        private System.Windows.Forms.Label ghostHitPoints;
        private System.Windows.Forms.Label ghoulHitPoints;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox swordEq;
    }
}

