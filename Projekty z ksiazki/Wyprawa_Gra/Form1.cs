﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Wyprawa_Gra
{
    public partial class Form1 : Form
    {
        private Game game;
        private Random random = new Random();
        
        
        public Form1()
        {
            InitializeComponent();
            
        }
        public void UpdateCharacters()
        {
            player.Location = game.PlayerLocation;
            playerHitPoints.Text = game.PlayerHitPoints.ToString();

            bool showBat = false;
            bool showGhost = false;
            bool showGhoul = false;
            bat.Visible = false;
            ghost.Visible = false;
            ghoul.Visible = false;
            int enemiesShown = 0;
         
            foreach (Enemy enemy in game.Enemies)
            {
                if (enemy is Bat)
                {
                    bat.Location = enemy.Location;
                    batHitPoints.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showBat = true;
                        bat.Visible = true;
                        enemiesShown++;
                    }
                    else
                    {
                        bat.Visible = false;
                        batHitPoints.Text = "0";
                    }
                }
                if (enemy is Ghost)
                {
                    ghost.Location = enemy.Location;
                    ghostHitPoints.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showGhost = true;
                        ghost.Visible = true;
                        enemiesShown++;
                    }
                    else
                    {

                        ghost.Visible = false;
                        ghostHitPoints.Text = "0";
                    }

                }
                if (enemy is Ghoul)
                {
                    ghoul.Location = enemy.Location;
                    ghoulHitPoints.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showGhoul = true;
                        ghoul.Visible = true;
                        enemiesShown++;
                    }
                    else
                    {
                        ghoul.Visible = false;
                        ghoulHitPoints.Text = "0";
                    }
                }
            }
            sword.Visible = false;
            bow.Visible = false;
            potion_blue.Visible = false;
            potion_red.Visible = false;
            mace.Visible = false;
            swordEq.Visible = false;
            bowEq.Visible = false;
            potion_redEq.Visible = false;
            potion_blueEq.Visible = false;
            maceEq.Visible = false;
            Control weaponControl = null;
            switch(game.WeaponInRoom.Name)
            {
                case "Miecz":
                    weaponControl = sword;
                  
                    break;
                case "Łuk":
                    weaponControl = bow;

                    break;
                case "Czerwona mikstura":
                    weaponControl = potion_red;

                    
                    break;
                case "Niebieska mikstura":
                    weaponControl = potion_blue;
                   
                    break;
                case "Buława":
                    weaponControl = mace;
                    
                    break;
            }
            if (game.CheckPlayerInventory("Miecz"))
                swordEq.Visible = true;
            if (game.CheckPlayerInventory("Łuk"))
                bowEq.Visible = true;
            if (game.CheckPlayerInventory("Czerwona mikstura"))
            {

                potion_redEq.Visible = true;
            }
            if (game.CheckPlayerInventory("Niebieska mikstura"))
            {
                potion_blueEq.Visible = true;
               
            }
            if (game.CheckPlayerInventory("Buława"))
                maceEq.Visible = true;
            weaponControl.Location = game.WeaponInRoom.Location;
            if (game.WeaponInRoom.PickedUp)
            {
                weaponControl.Visible = false;
            }
            else
            {
                weaponControl.Visible = true;
            }
            if (game.PlayerHitPoints <= 0)
            {
                MessageBox.Show("Zostałeś zabity...", "Władca lochów mówi...");
                DialogResult playAgain = MessageBox.Show("Chcesz zagrać ponowanie?", "Zagraj ponownie...", MessageBoxButtons.YesNo);
                if (playAgain == DialogResult.Yes)
                    Application.Restart();
                else
                    Application.Exit();
                
            }
            if (enemiesShown <1)
            {
                MessageBox.Show("Pokonałeś przeciwników!", "Władca lochów mówi...");
                //Victory();
                game.NewLevel(random);
                UpdateCharacters();
                
            }
            


        }
        /*private void Victory()
        {
            if (game.Level == 7)
            {
                MessageBox.Show("Udało ci się przeżyć...", "Władca lochów mówi...");
                MessageBox.Show("ZWYCIĘSTWO", "Gracz zwyciężył.");
                DialogResult doYouWannaPlay = MessageBox.Show("Chcesz zagrać ponowanie?", "Zagraj ponownie...", MessageBoxButtons.YesNo);
                if (doYouWannaPlay == DialogResult.No)
                    Application.Exit();
                else
                {
                    Application.Restart();

                }
                Application.Exit();
                Application.Exit();           }
           
        }*/

        private void SwordEq_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Miecz"))
            {
                game.Equip("Miecz");
                swordEq.BorderStyle = BorderStyle.FixedSingle;
                bowEq.BorderStyle = BorderStyle.None;
                maceEq.BorderStyle = BorderStyle.None;
                potion_redEq.BorderStyle = BorderStyle.None;
                potion_blueEq.BorderStyle = BorderStyle.None;
                button6.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
                button5.Text = "↑";
            }
        }

        private void BowEq_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Łuk"))
            {
                game.Equip("Łuk");
                bowEq.BorderStyle = BorderStyle.FixedSingle;
                swordEq.BorderStyle = BorderStyle.None;
                maceEq.BorderStyle = BorderStyle.None;
                potion_blueEq.BorderStyle = BorderStyle.None;
                potion_redEq.BorderStyle = BorderStyle.None;
                button6.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
                button5.Text = "↑";
            }
        }

        private void MaceEq_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Buława"))
            {
                game.Equip("Buława");
                maceEq.BorderStyle = BorderStyle.FixedSingle;
                bowEq.BorderStyle = BorderStyle.None;
                swordEq.BorderStyle = BorderStyle.None;
                potion_redEq.BorderStyle = BorderStyle.None;
                potion_blue.BorderStyle = BorderStyle.None;
                button6.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
                button5.Text = "↑";
            }
        }

        private void Potion_blueEq_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Niebieska mikstura"))
            {
                game.Equip("Niebieska mikstura");
                potion_blueEq.BorderStyle = BorderStyle.FixedSingle;
                bowEq.BorderStyle = BorderStyle.None;
                maceEq.BorderStyle = BorderStyle.None;
                potion_redEq.BorderStyle = BorderStyle.None;
                swordEq.BorderStyle = BorderStyle.None;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button5.Text = "Pij";
            }
        }

        private void Potion_redEq_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Czerwona mikstura"))
            {
                game.Equip("Czerwona mikstura");
                potion_redEq.BorderStyle = BorderStyle.FixedSingle;
                bowEq.BorderStyle = BorderStyle.None;
                maceEq.BorderStyle = BorderStyle.None;
                swordEq.BorderStyle = BorderStyle.None;
                potion_blueEq.BorderStyle = BorderStyle.None;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button5.Text = "Pij";
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Up, random);
            UpdateCharacters();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Left, random);
            UpdateCharacters();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Down, random);
            UpdateCharacters();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Right, random);
            UpdateCharacters();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            
            game.Attack(Direction.Up, random);
            UpdateCharacters();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Left, random);
            UpdateCharacters();
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Down, random);
            UpdateCharacters();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Right, random);
            UpdateCharacters();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            game = new Game(new Rectangle(78, 57, 609, 203));
            game.NewLevel(random);
            UpdateCharacters();
        }
    }
}
