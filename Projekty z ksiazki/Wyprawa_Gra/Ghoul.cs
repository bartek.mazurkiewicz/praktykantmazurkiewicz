﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyprawa_Gra
{
    class Ghoul : Enemy
    {
        public Ghoul(Game game, Point location) : base(game, location, 75)
        {
        }

        public override void Move(Random random)
        {
            if (HitPoints>0)
            {
                int chance = random.Next(2);
                if (chance == 1)
                {
                    location = Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
                }
                if (NearPlayer())
                {
                    game.HitPlayer(18, random);
                }
            }
            
        }
    }
}
