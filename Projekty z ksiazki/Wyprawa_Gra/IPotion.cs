﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyprawa_Gra
{
    interface IPotion
    {
        bool Used { get; }
    }
}
