﻿namespace Wyprawa_Gra
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }
}