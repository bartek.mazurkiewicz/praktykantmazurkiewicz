﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods2
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "Klony sieją spustoszenie w fabryce. Help!";
            message.IsDistressCall();
            if (message.IsDistressCall() == true)
            {
                Console.WriteLine("Proszą o pomoc!");
            }
            else
            {
                Console.WriteLine("Nie proszą o pomoc.");
            }
            Console.ReadKey();
        }
    }
}
