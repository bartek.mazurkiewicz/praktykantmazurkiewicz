﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using BasketballRoster.Model;

namespace BasketballRoster.ViewModel
{
    class LeagueViewModel
    {
        public RosterViewModel BriansTeam { get; set; }
        public RosterViewModel JimmysTeam { get; set; }
        public LeagueViewModel()
        {
            Roster jimmysRoster = new Roster("Niesamowici", GetAmazinPlayers());
            JimmysTeam = new RosterViewModel(jimmysRoster);
            Roster briansRoster = new Roster("Bombowce", GetBomberPlayers());
            BriansTeam = new RosterViewModel(briansRoster);
        }
        private IEnumerable<Player> GetAmazinPlayers()
        {
            List<Player> amazinPlayers = new List<Player>()
            {
                new Player("Wiesław",42, true),
                new Player("Heniek",11, true),
                new Player("Burek",4, true),
                new Player("Mieszko", 18, true),
                new Player("Bolo Potężna Łapa", 7, true),
                new Player("Helga", 23, false),
                new Player("Tomcio Paluch",21, false),
            };
            return amazinPlayers;
        }
        private IEnumerable<Player> GetBomberPlayers()
        {
            List<Player> bomberPlayers = new List<Player>()
            {
                new Player("Brajan Syn Seby", 7, true),
                new Player("Lucyfer", 23, true),
                new Player("Łuki Arrow",6, true),
                new Player("Mini majk", 0, true),
                new Player("Jaco", 42, true),
                new Player("Krzysiu Promil",32, false),
                new Player("Szybki Jan",8, false),
            };
            return bomberPlayers;
        }
    }
}
