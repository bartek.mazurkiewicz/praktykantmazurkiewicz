﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace @out
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Random random = new Random();
        public int ReturnThreeValues(out double half, out int twice)
        {
            int value = random.Next(1000);
            half = ((double)value) / 2;
            twice = value * 2;
            Button button = button1;
            button.Text = "go";
            return value;
        }
        public void ModifyAnIntAndButton(ref int value, ref Button button)
        {
            int i = value;
            i *= 5;
            value = i - 3;
            button = button1;
            button.Text = "go go";
        }
        public void CheckTemperature(double Temperature, double tooHigh = 35.5, double tooLow = 5.5)
        {
            if (Temperature < tooHigh && Temperature > tooLow)
            {
                textBox1.Text = "Jest super";
            }
            else
            {
                textBox1.Text = "Beznadziejna pogoda";
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int a;
            double b;
            int c;
            a = ReturnThreeValues(out b, out c);
            textBox1.Text = "Value = " + a + "\r\n" + "Half = " + b + "\r\n" + "Twice = " + c;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int q = 100;
            Button b = button1;
            ModifyAnIntAndButton(ref q, ref b);
            textBox1.Text = "q = " + q + "\r\n" + "b.text = " + b.Text;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            CheckTemperature(20);
            CheckTemperature(20, 20.5, 19);
            CheckTemperature(25, tooLow: 25);

        }
    }
}
