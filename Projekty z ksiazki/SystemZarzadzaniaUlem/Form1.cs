﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystemZarzadzaniaUlem
{
    public partial class Form1 : Form
    {
        Queen queen;
        public Form1()
        {
            InitializeComponent();
            workerBeeJob.SelectedIndex = 0;
            Worker[] workers = new Worker[4];
            workers[0] = new Worker(new string[] { "Patrol leśny", "Wojna z osami" }, 268);
            workers[1] = new Worker(new string[] { "Zamach na szerszenie", "Porwij króla trzmieli"}, 125);
            workers[2] = new Worker(new string[] { "Zbieranie nektaru", "Pielęgnacja jaj" }, 111);
            workers[3] = new Worker(new string[] { "Patrol leśny", "Wojna z osami", "Zamach na szerszenie", "Porwij króla trzmieli", "Zbieranie Nektaru", "Pielęgnacja jaj" }, 189);
            queen = new Queen(workers, 450);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (queen.AssignWork(workerBeeJob.Text, (int)shifts.Value) == false)
            {
                MessageBox.Show("Nie ma dostepnych robotnic do wykonania zadania '" + workerBeeJob.Text + "'", "Królowa pszczół mówi...");

            }
            else
                MessageBox.Show("Zadanie '" + workerBeeJob.Text + "' będzie ukonczone za " + shifts.Value + " zmiany", "Królowa pszczół mówi");
        }

        private void NextShift_Click(object sender, EventArgs e)
        {
            report.Text = queen.WorkTheNextShift();
        }
    }
}
