﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullable
{
    class RobustGuy
    {
        public DateTime? Birthday { get; private set; }
        public int? Height { get; private set; }

        public RobustGuy(string birthday, string height)
        {
            DateTime tempDate;
            if (DateTime.TryParse(birthday, out tempDate))
            {
                Birthday = tempDate;
            }
            else
            {
                Birthday = null;
            }
            int tempInt;
            if (int.TryParse(height, out tempInt))
            {
                Height = tempInt;
            }
            else
            {
                Height = null;
            }


        }
        public override string ToString()
        {
            string description;
            if (Birthday.HasValue)
            {
                description = "Urodziłem się " + Birthday.Value.ToLongDateString() + " roku";
            }
            else
            {
                description = "Nie wiem kiedy się urodziłem";
            }
            if (Height.HasValue)
            {
                description += ", i mam " + Height + " cm wzrostu."; 
            }
            else
            {
                description += ", i nie wiem ile mam wzrostu.";  
            }
            return description;
        }

    }
}
