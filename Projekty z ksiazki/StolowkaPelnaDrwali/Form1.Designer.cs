﻿namespace StolowkaPelnaDrwali
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.addLumberjack = new System.Windows.Forms.Button();
            this.line = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.addFlapjacks = new System.Windows.Forms.Button();
            this.banan = new System.Windows.Forms.RadioButton();
            this.hot = new System.Windows.Forms.RadioButton();
            this.tasty = new System.Windows.Forms.RadioButton();
            this.zajebisty = new System.Windows.Forms.RadioButton();
            this.nuttela = new System.Windows.Forms.RadioButton();
            this.crispy = new System.Windows.Forms.RadioButton();
            this.howMany = new System.Windows.Forms.NumericUpDown();
            this.nextInLine = new System.Windows.Forms.TextBox();
            this.nextLumberjack = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.howMany)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Imię drwala";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(79, 10);
            this.name.MaxLength = 20;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(157, 20);
            this.name.TabIndex = 1;
            this.name.Text = "Waldek Łysy Drwal";
            // 
            // addLumberjack
            // 
            this.addLumberjack.Location = new System.Drawing.Point(16, 39);
            this.addLumberjack.Name = "addLumberjack";
            this.addLumberjack.Size = new System.Drawing.Size(220, 23);
            this.addLumberjack.TabIndex = 2;
            this.addLumberjack.Text = "Dodaj drwala";
            this.addLumberjack.UseVisualStyleBackColor = true;
            this.addLumberjack.Click += new System.EventHandler(this.AddLumberjack_Click);
            // 
            // line
            // 
            this.line.FormattingEnabled = true;
            this.line.Location = new System.Drawing.Point(16, 95);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(120, 303);
            this.line.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kolejka do śniadania";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nextLumberjack);
            this.groupBox1.Controls.Add(this.nextInLine);
            this.groupBox1.Controls.Add(this.addFlapjacks);
            this.groupBox1.Controls.Add(this.banan);
            this.groupBox1.Controls.Add(this.hot);
            this.groupBox1.Controls.Add(this.tasty);
            this.groupBox1.Controls.Add(this.zajebisty);
            this.groupBox1.Controls.Add(this.nuttela);
            this.groupBox1.Controls.Add(this.crispy);
            this.groupBox1.Controls.Add(this.howMany);
            this.groupBox1.Location = new System.Drawing.Point(172, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(127, 303);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nakarm drwala";
            // 
            // addFlapjacks
            // 
            this.addFlapjacks.Location = new System.Drawing.Point(7, 189);
            this.addFlapjacks.Name = "addFlapjacks";
            this.addFlapjacks.Size = new System.Drawing.Size(114, 23);
            this.addFlapjacks.TabIndex = 7;
            this.addFlapjacks.Text = "Dodaj naleśniki";
            this.addFlapjacks.UseVisualStyleBackColor = true;
            this.addFlapjacks.Click += new System.EventHandler(this.AddFlapjacks_Click);
            // 
            // banan
            // 
            this.banan.AutoSize = true;
            this.banan.Location = new System.Drawing.Point(6, 165);
            this.banan.Name = "banan";
            this.banan.Size = new System.Drawing.Size(88, 17);
            this.banan.TabIndex = 6;
            this.banan.Text = "Bananowego";
            this.banan.UseVisualStyleBackColor = true;
            // 
            // hot
            // 
            this.hot.AutoSize = true;
            this.hot.Location = new System.Drawing.Point(6, 142);
            this.hot.Name = "hot";
            this.hot.Size = new System.Drawing.Size(72, 17);
            this.hot.TabIndex = 5;
            this.hot.Text = "Gorącego";
            this.hot.UseVisualStyleBackColor = true;
            // 
            // tasty
            // 
            this.tasty.AutoSize = true;
            this.tasty.Location = new System.Drawing.Point(6, 118);
            this.tasty.Name = "tasty";
            this.tasty.Size = new System.Drawing.Size(81, 17);
            this.tasty.TabIndex = 4;
            this.tasty.Text = "Smacznego";
            this.tasty.UseVisualStyleBackColor = true;
            // 
            // zajebisty
            // 
            this.zajebisty.AutoSize = true;
            this.zajebisty.Checked = true;
            this.zajebisty.Location = new System.Drawing.Point(6, 94);
            this.zajebisty.Name = "zajebisty";
            this.zajebisty.Size = new System.Drawing.Size(80, 17);
            this.zajebisty.TabIndex = 3;
            this.zajebisty.TabStop = true;
            this.zajebisty.Text = "Zajebistego";
            this.zajebisty.UseVisualStyleBackColor = true;
            // 
            // nuttela
            // 
            this.nuttela.AutoSize = true;
            this.nuttela.Location = new System.Drawing.Point(6, 70);
            this.nuttela.Name = "nuttela";
            this.nuttela.Size = new System.Drawing.Size(84, 17);
            this.nuttela.TabIndex = 2;
            this.nuttela.Text = "Nutellowego";
            this.nuttela.UseVisualStyleBackColor = true;
            // 
            // crispy
            // 
            this.crispy.AutoSize = true;
            this.crispy.Location = new System.Drawing.Point(6, 46);
            this.crispy.Name = "crispy";
            this.crispy.Size = new System.Drawing.Size(79, 17);
            this.crispy.TabIndex = 1;
            this.crispy.Text = "Chrupkiego";
            this.crispy.UseVisualStyleBackColor = true;
            // 
            // howMany
            // 
            this.howMany.Location = new System.Drawing.Point(6, 19);
            this.howMany.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.howMany.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.howMany.Name = "howMany";
            this.howMany.Size = new System.Drawing.Size(58, 20);
            this.howMany.TabIndex = 0;
            this.howMany.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nextInLine
            // 
            this.nextInLine.Location = new System.Drawing.Point(7, 219);
            this.nextInLine.Multiline = true;
            this.nextInLine.Name = "nextInLine";
            this.nextInLine.ReadOnly = true;
            this.nextInLine.Size = new System.Drawing.Size(114, 48);
            this.nextInLine.TabIndex = 8;
            // 
            // nextLumberjack
            // 
            this.nextLumberjack.Location = new System.Drawing.Point(7, 274);
            this.nextLumberjack.Name = "nextLumberjack";
            this.nextLumberjack.Size = new System.Drawing.Size(114, 23);
            this.nextLumberjack.TabIndex = 7;
            this.nextLumberjack.Text = "Następny drwal";
            this.nextLumberjack.UseVisualStyleBackColor = true;
            this.nextLumberjack.Click += new System.EventHandler(this.NextLumberjack_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 408);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.line);
            this.Controls.Add(this.addLumberjack);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.howMany)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Button addLumberjack;
        private System.Windows.Forms.ListBox line;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button addFlapjacks;
        private System.Windows.Forms.RadioButton banan;
        private System.Windows.Forms.RadioButton hot;
        private System.Windows.Forms.RadioButton tasty;
        private System.Windows.Forms.RadioButton zajebisty;
        private System.Windows.Forms.RadioButton nuttela;
        private System.Windows.Forms.RadioButton crispy;
        private System.Windows.Forms.NumericUpDown howMany;
        private System.Windows.Forms.TextBox nextInLine;
        private System.Windows.Forms.Button nextLumberjack;
    }
}

