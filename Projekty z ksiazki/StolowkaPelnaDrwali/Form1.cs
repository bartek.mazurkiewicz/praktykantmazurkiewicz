﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StolowkaPelnaDrwali
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private Queue<Lumberjack> breakfastLine = new Queue<Lumberjack>();
       
       

        private void AddLumberjack_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(name.Text))
            {
                return;
            }
            breakfastLine.Enqueue(new Lumberjack(name.Text));
            name.Text = "";
            RedrawList();
        }
        private void RedrawList()
        {
            int number = 1;
            line.Items.Clear();
            foreach (Lumberjack lumberjack in breakfastLine)
            {
                line.Items.Add(number + "." + lumberjack.Name);
                number++;
            }
            if (breakfastLine.Count == 0)
            {
                groupBox1.Enabled = false;
                nextInLine.Text = "";
            }
            else
            {
                groupBox1.Enabled = true;
                Lumberjack currentLumberjack = breakfastLine.Peek();
                nextInLine.Text = currentLumberjack.Name + " ma " + currentLumberjack.FlapjackCount + " naleśników. MMMMMM!!!";
            }
        }

        private void AddFlapjacks_Click(object sender, EventArgs e)
        {
            Flapjack food;
            if (crispy.Checked)
            {
                food = Flapjack.Chrupkiego; 
            }
            else if (nuttela.Checked)
            {
                food = Flapjack.Nutellowego;
            }
            else if (zajebisty.Checked)
            {
                food = Flapjack.Zajebistego;
            }
            else if (banan.Checked)
            {
                food = Flapjack.Bananowego;
            }
            else if (hot.Checked)
            {
                food = Flapjack.Gorącego;
            }
            else
            {
                food = Flapjack.Smacznego;
            }
            Lumberjack currentLumberJack = breakfastLine.Peek();
            currentLumberJack.TakeFlapjacks(food, (int)howMany.Value);
            RedrawList();
        }

        private void NextLumberjack_Click(object sender, EventArgs e)
        {
            if (breakfastLine.Count == 0)
            {
                return;
            }
            Lumberjack nextLumberjack = breakfastLine.Dequeue();
            nextLumberjack.EatFlapjacks();
            nextInLine.Text = "";
            RedrawList();
        }
    }
}
