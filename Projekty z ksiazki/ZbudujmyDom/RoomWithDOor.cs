﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZbudujmyDom
{
    class RoomWithDoor : RoomWithHidingPlace, IHasExteriorDoor
    {
      
        public RoomWithDoor(string name, string decoration, string doorDescripiton, string hidingPlaceName) : base(name, decoration, hidingPlaceName)
        {
            DoorDescription = doorDescripiton;
        }

        public string DoorDescription { get; private set; }
        public Location DoorLocation { get; set; }
    }
}
