﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZbudujmyDom
{
    class Room : Location
    {
        string decoration;
        public Room(string name, string decoration) : base(name)
        {
            this.decoration = decoration;
        }
        public override string Description
        {
            get
            {
                string NewDescription = "Widzisz tutaj " + decoration;
                NewDescription = base.Description;
                return NewDescription;
            }
        }


    }
}
