﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZbudujmyDom
{
    public partial class Form1 : Form
    {
        int Moves;
        Location currentLocation;
        OutsideWithHidingPlace garden;
        OutsideWithHidingPlace driveway;
        OutsideWithDoor backYard;
        OutsideWithDoor frontYard;
        RoomWithDoor livingRoom;
        RoomWithDoor kitchen;
        Room stairs;
        RoomWithHidingPlace diningRoom;
        RoomWithHidingPlace hallway;
        RoomWithHidingPlace bathroom;
        RoomWithHidingPlace masterBedroom;
        RoomWithHidingPlace secondBedroom;
        Opponent opponent;
        public Form1()
        {
            InitializeComponent();
            CreateObjects();
            opponent = new Opponent(frontYard);
            ResetGame(false);
        }
        private void CreateObjects()
        {
            livingRoom = new RoomWithDoor("Salon", "Dywan z bobra", "Bukowe drzwi", "w szafie ściennej");
            diningRoom = new RoomWithHidingPlace("Jadalnia", "Połamane taborety", "W wysokiej szafie");
            kitchen = new RoomWithDoor("Kuchnia", "Złota zmywarka", "Szklane drzwi", "W złotej zmywarce");
            stairs = new Room("Schody", "Poręcz z brokatem");
            hallway = new RoomWithHidingPlace("Korytarz na górze", "Obrazek z psem", "Udawanie psa w obrazie");
            bathroom = new RoomWithHidingPlace("Łazienka", "Kibel ze złota", "W kibelku");
            masterBedroom = new RoomWithHidingPlace("Duża sypialnia", "Wodne łóżko", "Pod łóżkiem");
            secondBedroom = new RoomWithHidingPlace("Druga sypialnia", "Łóżko drewniane", "Pod łóżkiem");
            frontYard = new OutsideWithDoor("Podwórko przed domem", false, "Bukowe drzwi");
            backYard = new OutsideWithDoor("Podwórka za domem", true, "Szklane drzwi");
            garden = new OutsideWithHidingPlace("Ogród", false, "W szopie");
            driveway = new OutsideWithHidingPlace("Droga dojazdowa", true, "W garażu");

            diningRoom.Exits = new Location[] { livingRoom, kitchen };
            livingRoom.Exits = new Location[] { diningRoom, stairs };
            kitchen.Exits = new Location[] { diningRoom };
            stairs.Exits = new Location[] { livingRoom, hallway };
            hallway.Exits = new Location[] { stairs, bathroom, masterBedroom, secondBedroom };
            bathroom.Exits = new Location[] { hallway };
            masterBedroom.Exits = new Location[] { hallway };
            secondBedroom.Exits = new Location[] { hallway };

            frontYard.Exits = new Location[] { backYard, garden, driveway };
            backYard.Exits = new Location[] { frontYard, garden, driveway };
            garden.Exits = new Location[] { backYard, frontYard };
            driveway.Exits = new Location[] { backYard, frontYard };

            livingRoom.DoorLocation = frontYard;
            frontYard.DoorLocation = livingRoom;

            kitchen.DoorLocation = backYard;
            backYard.DoorLocation = kitchen;

        }
        
        private void MoveToANewLocation(Location newLocation)
        {
            Moves++;
            currentLocation = newLocation;
            RedrawForm();
        }

        private void RedrawForm()
        {
            exits.Items.Clear();
            for (int i = 0; i < currentLocation.Exits.Length; i++)
            {
                exits.Items.Add(currentLocation.Exits[i].Name);
            }
            exits.SelectedIndex = 0;
            description.Text = currentLocation.Description + "\r\n(ruch numer" + Moves + ")";
            if (currentLocation is IHidingPlace)
            {
                IHidingPlace hidingplace = currentLocation as IHidingPlace;
                check.Text = "Sprawdz " + hidingplace.HidingPlaceName;
                check.Visible = true;
            }
            else
                check.Visible = false;
            if (currentLocation is IHasExteriorDoor)
            {
                goThroughTheDoor.Visible = true;
            }
            else
                goThroughTheDoor.Visible = false;
        }
        private void ResetGame(bool displayMessage)
        {
            if (displayMessage)
            {
                MessageBox.Show("Odnalazłeś mnie w " + Moves + " ruchach!");
                IHidingPlace foundLocation = currentLocation as IHidingPlace;
                description.Text = "Znalazłeś przeciwnika w " + Moves + " ruchach! Ukrywał się " + foundLocation.HidingPlaceName + ".";

            }
            Moves = 0;
            hide.Visible = true;
            goHere.Visible = false;
            check.Visible = false;
            goThroughTheDoor.Visible = false;
            exits.Visible = false;
        }

        private void GoHere_Click(object sender, EventArgs e)
        {
            MoveToANewLocation(currentLocation.Exits[exits.SelectedIndex]);
        }

        private void GoThroughTheDoor_Click(object sender, EventArgs e)
        {
            IHasExteriorDoor hasDoor = currentLocation as IHasExteriorDoor;
            MoveToANewLocation(hasDoor.DoorLocation);
        }

        private void Check_Click(object sender, EventArgs e)
        {
            Moves++;
            if (opponent.Check(currentLocation))
            {
                ResetGame(true);
            }
            else
                RedrawForm();
        }

        private void Hide_Click(object sender, EventArgs e)
        {
            hide.Visible = false;
            for (int i = 0; i <= 10; i++)
            {
                opponent.Move();
                description.Text = i + "... ";
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
            }
            description.Text = "Gotowy czy nie - nadchodzę!";
            Application.DoEvents();
            System.Threading.Thread.Sleep(500);

            goHere.Visible = true;
            exits.Visible = true;
            MoveToANewLocation(livingRoom);
        }
    }
}
