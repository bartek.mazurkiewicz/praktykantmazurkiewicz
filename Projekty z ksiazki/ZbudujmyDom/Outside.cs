﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZbudujmyDom
{
    class Outside : Location
    {
        private bool hot;
        private string name;

      

        public Outside(string name, bool hot) : base(name)
        {
            this.hot = hot;
            this.name = name;
        }
        public override string Description
        {
            get
            {
                string NewDescription = base.Description;
                if(hot)
                {
                    NewDescription += " Tutaj jest bardzo gorąco ";
                }
                return NewDescription;

            }
        }
    }
}
