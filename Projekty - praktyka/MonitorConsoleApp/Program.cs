﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace MonitorConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                System.Threading.Thread.Sleep(10000);
                PerformanceCounter pCPU = new PerformanceCounter();
                pCPU.CategoryName = "Processor";
                pCPU.CounterName = "% Processor Time";
                pCPU.InstanceName = "_Total";
                PerformanceCounter pRAM = new PerformanceCounter();
                pRAM.CategoryName = "Memory";
                pRAM.CounterName = "% Committed Bytes In Use";
                TaskManager taskManager = new TaskManager();
                taskManager.ListAllProcesses();
                UsageManager usageManager = new UsageManager();
                usageManager.LoadUsageToDatabase(pCPU, pRAM);
                MessageBox.Show("Dodano!");
            }

            
        }
    }
}
