﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data;

namespace MonitorConsoleApp
{
    class SqliteDataAcess
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private void SetConnection(string dataBase)
        {
            sql_con = new SQLiteConnection("Data Source='"+dataBase+"';Version=3;New=False;Compress=True;");
        }
        public void ExecuteQuery(string txtQuery, string dataBase)
        {
            SetConnection(dataBase);
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
        }
    }
}
