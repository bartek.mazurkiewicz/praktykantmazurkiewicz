﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonitorConsoleApp
{ 
    class UsageManager
    {
        string database = @"C:\Users\barte\source\repos\praktyka\Projekty - praktyka\Monitor\bin\Debug\UsageMonitor.db";
        SqliteDataAcess sqliteDataAcess;
        public float LoadCPUData(PerformanceCounter p)
        {
            p.NextValue();
            float fcpu = p.NextValue();
            return fcpu;
        }
        public float LoadRAMData(PerformanceCounter p)
        {
            float fram = p.NextValue();
            return fram;
        }
        public void LoadUsageToDatabase(PerformanceCounter p1, PerformanceCounter p2 )
        {
            string CPUName = "CPU";
            string RAMName = "RAM";
            float fcpu = LoadCPUData(p1);
            float fram = LoadRAMData(p2);
            sqliteDataAcess = new SqliteDataAcess();
            string txtQueryCPU = "insert into Usage (Name, Usage)values('" + CPUName + "','" + (int)fcpu + "')";
            string txtQueryRAM = "insert into Usage (Name, Usage)values('" + RAMName + "','" + (int)fram + "')";
            sqliteDataAcess.ExecuteQuery(txtQueryCPU, database);
            sqliteDataAcess.ExecuteQuery(txtQueryRAM, database);
        }
        
    }
}
