﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor
{
    public partial class Form1 : Form
    {
        ServiceInstaller serviceInstaller;
        SqliteDataAcess sqliteDataAcces;
        TaskManager taskManager;
        UsageManager usageManager;
        Notepad notepad;
        Keylogger keylogger = new Keylogger();
        public Form1()
        {
            InitializeComponent();
            notepad = new Notepad();
            notepad.SetFileDialog(saveFileDialog1, openFileDialog1);
        }

        private void RefreshProcessesButton_Click(object sender, EventArgs e)
        {
            RefreshProcesses();
        }
        private void KillProcessButton_Click(object sender, EventArgs e)
        {
            taskManager = new TaskManager();
            taskManager.KillAProcess(processDataGridView);
            if (taskManager.killAProcessReturn == 1)
            {
                RefreshProcesses();
            }

        }
        public void RefreshProcesses()
        {
            sqliteDataAcces = new SqliteDataAcess();
            taskManager = new TaskManager();
            taskManager.ListAllProcesses();
            sqliteDataAcces.LoadData("Processes", processDataGridView, "Monitor.db");
            MessageBox.Show("Pomyślnie odświeżono procesy", "Odswieżenie procesów", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void RunProcessButton_Click(object sender, EventArgs e)
        {
            taskManager = new TaskManager();
            taskManager.RunAProcess(processNameTextBox);
            if (taskManager.runAProcessReturn == 1)
            {
                RefreshProcesses();
            }

        }
        private void TimerUsage_Tick(object sender, EventArgs e)
        {
            usageManager = new UsageManager();
            usageManager.LoadUsageData(cpuCounter, ramCounter, cpuProgressBar, ramProgressBar, cpuUsageLabel, ramUsageLabel);
        }

        private void StartUsageButton_Click(object sender, EventArgs e)
        {
            timerUsage.Start();
            stopUsageButton.Enabled = true;
            nullValueUsageButton.Enabled = true;

        }

        private void StopUsageButton_Click(object sender, EventArgs e)
        {
            timerUsage.Stop();
        }
        private void NullValueUsageButton_Click(object sender, EventArgs e)
        {
            stopUsageButton.Enabled = false;
            nullValueUsageButton.Enabled = false;
            timerUsage.Stop();
            System.Threading.Thread.Sleep(250);
            cpuProgressBar.Value = 0;
            cpuUsageLabel.Text = "0%";
            System.Threading.Thread.Sleep(50);
            ramProgressBar.Value = 0;
            ramUsageLabel.Text = "0%";
        }
        private void UsageDatabaseButton_Click(object sender, EventArgs e)
        {
            usageManager = new UsageManager();
            usageManager.LoadCPUUsageToDatabase(cpuCounter);
            sqliteDataAcces = new SqliteDataAcess();
            sqliteDataAcces.LoadData("CpuUsage", usageDataGridView, "Monitor.db");
        }
        private void AddRAMUsageDataButton_Click(object sender, EventArgs e)
        {
            usageManager = new UsageManager();
            usageManager.LoadRAMUsageToDatabase(ramCounter);
            sqliteDataAcces = new SqliteDataAcess();
            sqliteDataAcces.LoadData("MemoryUsage", usageDataGridView, "Monitor.db");
        }
        private void OpenUsageDataButton_Click(object sender, EventArgs e)
        {
            sqliteDataAcces = new SqliteDataAcess();
            sqliteDataAcces.LoadData("CpuUsage", usageDataGridView, "Monitor.db");
            addCPUUsageDataButton.Visible = true;
            clearUsageDataButton.Visible = true;
            exitUsageDataButton.Visible = true;
            openCPUUsageDataButton.Visible = false;
            openRamUsageDataButton.Visible = false;
            addRAMUsageDataButton.Visible = false;
        }
        private void OpenRamUsageDataButton_Click(object sender, EventArgs e)
        {
            sqliteDataAcces = new SqliteDataAcess();
            sqliteDataAcces.LoadData("MemoryUsage", usageDataGridView, "Monitor.db");
            addCPUUsageDataButton.Visible = false;
            clearUsageDataButton.Visible = true;
            exitUsageDataButton.Visible = true;
            openCPUUsageDataButton.Visible = false;
            openRamUsageDataButton.Visible = false;
            addRAMUsageDataButton.Visible = true;
        }
        private void ClearUsageDataButton_Click(object sender, EventArgs e)
        {
            sqliteDataAcces = new SqliteDataAcess();
            if (addCPUUsageDataButton.Visible == true)
            {
                sqliteDataAcces.ExecuteQuery("DELETE from CpuUsage", "Monitor.db");
                sqliteDataAcces.LoadData("CpuUsage", usageDataGridView, "Monitor.db");
            }
            else
            {
                sqliteDataAcces.ExecuteQuery("DELETE from MemoryUsage", "Monitor.db");
                sqliteDataAcces.LoadData("MemoryUsage", usageDataGridView, "Monitor.db");
            }
        }
        private void ExitUsageDataButton_Click(object sender, EventArgs e)
        {
            addCPUUsageDataButton.Visible = false;
            clearUsageDataButton.Visible = false;
            openCPUUsageDataButton.Visible = true;
            exitUsageDataButton.Visible = false;
            openRamUsageDataButton.Visible = true;
            addRAMUsageDataButton.Visible = false;
            usageDataGridView.DataSource = "";
        }
        private void RunMonitorServiceButton_Click(object sender, EventArgs e)
        {
            ServiceFile service = new ServiceFile();
            service.ChangeFile(monitorServiceNumericUpDown);
            string filePath = @"C:\Users\barte\source\repos\praktyka\Projekty - usługi\MonitorService\bin\Release\MonitorService.exe";
            Assembly assembly = Assembly.LoadFrom(filePath);
            serviceInstaller = new ServiceInstaller();
            serviceInstaller.InstallService("MonitorService", assembly);
            unistallMonitorServiceButton.Visible = true;
            installMonitorServiceButton.Visible = false;
            monitorServiceNumericUpDown.Visible = false;
        }
        private void UnistallMonitorServiceButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(100);
                string filePath = @"C:\Users\barte\source\repos\praktyka\Projekty - usługi\MonitorService\bin\Release\MonitorService.exe";
                serviceInstaller = new ServiceInstaller();
                serviceInstaller.UnistallService(filePath);
                unistallMonitorServiceButton.Visible = false;
                installMonitorServiceButton.Visible = true;
                monitorServiceNumericUpDown.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }
        private void RunMonitorServiceButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                serviceInstaller = new ServiceInstaller();
                serviceInstaller.StartService("MonitorService", 1000);
                runMonitorServiceButton.Visible = false;
                stopMonitorServiceButton.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void StopMonitorServiceButton_Click(object sender, EventArgs e)
        {
            try
            {
                serviceInstaller = new ServiceInstaller();
                serviceInstaller.StopService("MonitorService", 1000);
                runMonitorServiceButton.Visible = true;
                stopMonitorServiceButton.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                runMonitorServiceButton.Visible = true;
                stopMonitorServiceButton.Visible = false;
            }
        }
        private void RunKeyloggerButton_Click(object sender, EventArgs e)
        {
            stopKeyloggerButton.Visible = true;
            runKeyloggerButton.Visible = false;
            keylogger.b = true;
            Task task = new Task(() =>
            {
                keylogger.Logs();
            });
            task.Start();

        }
        private void StopKeyloggerButton_Click(object sender, EventArgs e)
        {
            runKeyloggerButton.Visible = true;
            stopKeyloggerButton.Visible = false;
            keylogger.b = false;
        }
        private void RunRaportDataBaseButton_Click(object sender, EventArgs e)
        {
            sqliteDataAcces = new SqliteDataAcess();
            sqliteDataAcces.LoadData("Raport", raportGridView, "Monitor.db");
            clearAllTablesButton.Visible = true;
            exitRaportDataButton.Visible = true;
        }
        private void ExitRaportDataButton_Click(object sender, EventArgs e)
        {
            exitRaportDataButton.Visible = false;
            clearAllTablesButton.Visible = false;
            runRaportDataBaseButton.Visible = true;
            raportGridView.DataSource = "";
        }

        private void ClearAllTablesButton_Click(object sender, EventArgs e)
        {
            sqliteDataAcces.ExecuteQuery("DELETE from CpuUsage", "Monitor.db");
            sqliteDataAcces.ExecuteQuery("DELETE from MemoryUsage", "Monitor.db");
            sqliteDataAcces.ExecuteQuery("DELETE from Processes", "Monitor.db");
            sqliteDataAcces.ExecuteQuery("DELETE from Raport", "Monitor.db");
            sqliteDataAcces.LoadData("Raport", raportGridView, "Monitor.db");
            MessageBox.Show("Usunięto rekordy ze wszystkich tabel.", "Usunięto rekordy", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            noteTextBox.Clear();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notepad = new Notepad();
            notepad.OpenNote(openFileDialog1, noteTextBox);
        }
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notepad = new Notepad();
            notepad.SaveNote(saveFileDialog1, noteTextBox);
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BrowseRaportsButton_Click(object sender, EventArgs e)
        {
            Task task = new Task(() =>
            {
                RaportForm raportForm = new RaportForm();
                raportForm.ShowDialog();
            });
            task.Start();
        }
    }
}
