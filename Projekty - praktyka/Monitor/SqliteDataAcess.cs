﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace Monitor
{
    class SqliteDataAcess
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader reader;
        private void SetConnection(string dataBase)
        {
            sql_con = new SQLiteConnection("Data Source='" + dataBase + "';Version=3;New=False;Compress=True;");
        }
        public void ExecuteQuery(string txtQuery, string dataBase)
        {
            SetConnection(dataBase);
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
        }
        public void LoadData(string tableName, DataGridView dataGridView, string dataBase)
        {
            SetConnection(dataBase);
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string commandText = "select * from " + tableName;
            DB = new SQLiteDataAdapter(commandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dataGridView.DataSource = DT;
            sql_con.Close();
        }
        private void LoadRaportToDataGridView(DataGridView dataGridView, DataSet DSR, DataTable DTR, string commandText)
        {
            DSR.Clear();
            DTR.Clear();
            SetConnection("Monitor.db");
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            DB = new SQLiteDataAdapter(commandText, sql_con);
            DSR.Reset();
            DB.Fill(DSR);
            DTR = DSR.Tables[0];
            dataGridView.DataSource = DTR;
            sql_con.Close();
        }
        public void LoadRaport(DataGridView dataGridView, int id, string tableName)
        {
            string commandText = "";
            commandText = "select * from " + tableName + " where Raport_ID = " + id;
            DataSet DSR = new DataSet();
            DataTable DTR = new DataTable();
            LoadRaportToDataGridView(dataGridView, DSR, DTR, commandText);
        }
        public List<string> SelectTime(DateTimePicker dateTimePicker)
        {
            List<string> time = new List<string>();
            SetConnection("Monitor.db");
            string txtQuery = $"select Time from Raport where Date='{dateTimePicker.Text}'";
            sql_cmd = new SQLiteCommand(txtQuery, sql_con);
            sql_con.Open();
            reader = sql_cmd.ExecuteReader();
            while (reader.Read())
            {
                time.Add(Convert.ToString(reader["Time"]));
            }
            sql_con.Close();
            return time;
        }
        public int SelectID(ComboBox comboBox)
        {
            int ID = 0;
            SetConnection("Monitor.db");
            string txtQuery = $"select Raport_ID from Raport where Time='{comboBox.SelectedItem.ToString()}'";
            sql_cmd = new SQLiteCommand(txtQuery, sql_con);
            sql_con.Open();
            reader = sql_cmd.ExecuteReader();
            while (reader.Read())
            {
                ID = Convert.ToInt32(reader["Raport_ID"]);
            }
            sql_con.Close();

            return ID;
        }
        public int GenerateRaport(Types type)
        {
            int ID = 0;
            string date = DateTime.Now.ToString("dd/MM/yyyy");
            string time = DateTime.Now.ToString("HH:mm:ss");
            string txtquery = $"insert into Raport (Date, Time, Type) values('{date}', '{time}' ,{(int)type})";
            ExecuteQuery(txtquery, "Monitor.db");

            SetConnection("Monitor.db");
            string txtQuery = $"select Raport_ID from Raport where Date='{date}'";
            sql_cmd = new SQLiteCommand(txtQuery, sql_con);
            sql_con.Open();
            reader = sql_cmd.ExecuteReader();
            while (reader.Read())
            {
                ID = Convert.ToInt32(reader["Raport_ID"]);
            }
            sql_con.Close();

            return ID;
        }
    }
}
