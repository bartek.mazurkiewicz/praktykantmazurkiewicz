﻿namespace Monitor
{
    partial class RaportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RaportForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ramRaportGridView = new System.Windows.Forms.DataGridView();
            this.processesRaportGridView = new System.Windows.Forms.DataGridView();
            this.cpuRaportGridView = new System.Windows.Forms.DataGridView();
            this.runRaportButton = new System.Windows.Forms.Button();
            this.timeRaportComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.ramRaportGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.processesRaportGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuRaportGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "CPU";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "RAM";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(414, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Processes";
            // 
            // ramRaportGridView
            // 
            this.ramRaportGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.ramRaportGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ramRaportGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ramRaportGridView.Location = new System.Drawing.Point(16, 151);
            this.ramRaportGridView.Name = "ramRaportGridView";
            this.ramRaportGridView.RowHeadersWidth = 51;
            this.ramRaportGridView.RowTemplate.Height = 24;
            this.ramRaportGridView.Size = new System.Drawing.Size(357, 71);
            this.ramRaportGridView.TabIndex = 4;
            // 
            // processesRaportGridView
            // 
            this.processesRaportGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.processesRaportGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.processesRaportGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.processesRaportGridView.Location = new System.Drawing.Point(406, 33);
            this.processesRaportGridView.Name = "processesRaportGridView";
            this.processesRaportGridView.RowHeadersWidth = 51;
            this.processesRaportGridView.RowTemplate.Height = 24;
            this.processesRaportGridView.Size = new System.Drawing.Size(474, 254);
            this.processesRaportGridView.TabIndex = 5;
            // 
            // cpuRaportGridView
            // 
            this.cpuRaportGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.cpuRaportGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cpuRaportGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cpuRaportGridView.Location = new System.Drawing.Point(15, 33);
            this.cpuRaportGridView.Name = "cpuRaportGridView";
            this.cpuRaportGridView.RowHeadersWidth = 51;
            this.cpuRaportGridView.RowTemplate.Height = 24;
            this.cpuRaportGridView.Size = new System.Drawing.Size(358, 71);
            this.cpuRaportGridView.TabIndex = 6;
            // 
            // runRaportButton
            // 
            this.runRaportButton.Location = new System.Drawing.Point(16, 235);
            this.runRaportButton.Name = "runRaportButton";
            this.runRaportButton.Size = new System.Drawing.Size(138, 52);
            this.runRaportButton.TabIndex = 7;
            this.runRaportButton.Text = "Zobacz raport";
            this.runRaportButton.UseVisualStyleBackColor = true;
            this.runRaportButton.Click += new System.EventHandler(this.RunRaportButton_Click);
            // 
            // timeRaportComboBox
            // 
            this.timeRaportComboBox.FormattingEnabled = true;
            this.timeRaportComboBox.Location = new System.Drawing.Point(173, 263);
            this.timeRaportComboBox.Name = "timeRaportComboBox";
            this.timeRaportComboBox.Size = new System.Drawing.Size(200, 24);
            this.timeRaportComboBox.TabIndex = 9;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(173, 235);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.DateTimePicker1_ValueChanged);
            // 
            // RaportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 294);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.timeRaportComboBox);
            this.Controls.Add(this.runRaportButton);
            this.Controls.Add(this.cpuRaportGridView);
            this.Controls.Add(this.processesRaportGridView);
            this.Controls.Add(this.ramRaportGridView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RaportForm";
            this.Text = "Raporty";
            this.Load += new System.EventHandler(this.RaportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ramRaportGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.processesRaportGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuRaportGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView ramRaportGridView;
        private System.Windows.Forms.DataGridView processesRaportGridView;
        private System.Windows.Forms.DataGridView cpuRaportGridView;
        private System.Windows.Forms.Button runRaportButton;
        private System.Windows.Forms.ComboBox timeRaportComboBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}