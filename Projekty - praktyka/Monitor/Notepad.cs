﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor
{
    class Notepad
    {
        string noteName;
        public void SetFileDialog(SaveFileDialog saveFileDialog1, OpenFileDialog openFileDialog1)
        {
            saveFileDialog1.InitialDirectory = @"C:\Users\barte\source\repos\praktyka\Projekty - praktyka\Monitor\Notes";
            saveFileDialog1.Title = "Zapisz plik";
            saveFileDialog1.Filter = "Pliki tekstowe(*.txt) | *.txt";

            openFileDialog1.InitialDirectory = @"C:\Users\barte\source\repos\praktyka\Projekty - praktyka\Monitor\Notes";
            openFileDialog1.Title = "Otwórz plik";
            openFileDialog1.Filter = "Pliki tekstowe(*.txt) | *.txt";
        }
        public void OpenNote(OpenFileDialog openFileDialog1, TextBox noteTextBox)
        {
            using (openFileDialog1)
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                    {
                        noteName = openFileDialog1.FileName;
                        Task<string> text = sr.ReadToEndAsync();
                        noteTextBox.Text = text.Result;
                    }

                }

            }
        }
        public async void SaveNote(SaveFileDialog saveFileDialog1, TextBox noteTextBox)
        {
            if (String.IsNullOrEmpty(noteName))
            {
                using (saveFileDialog1)
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                        {
                            await sw.WriteLineAsync(noteTextBox.Text);
                        }
                    }
                }
            }
            else
            {
                using (StreamWriter sw = new StreamWriter(noteName))
                {
                    await sw.WriteLineAsync(noteTextBox.Text);
                }
            }
        }
    }
}
