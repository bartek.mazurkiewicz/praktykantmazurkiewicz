﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor
{ 
    class UsageManager
    {
        SqliteDataAcess sqliteDataAcess;
        public void LoadUsageData(PerformanceCounter p1, PerformanceCounter p2, ProgressBar progressBar1, ProgressBar progressBar2, Label label1, Label label2)
        {
            float fcpu = LoadCPUData(p1);
            float fram = LoadRAMData(p2);
            progressBar1.Value = (int) fcpu;
            progressBar2.Value = (int) fram;
            label1.Text = string.Format("{0:0.00}%", fcpu);
            label2.Text = string.Format("{0:0.00}%", fram);
            
        }
        public float LoadCPUData(PerformanceCounter p)
        {
            float fcpu = p.NextValue();
            return fcpu;
        }
        public float LoadRAMData(PerformanceCounter p)
        {
            float fram = p.NextValue();
            return fram;
        }
        public void LoadCPUUsageToDatabase(PerformanceCounter p)
        {
            sqliteDataAcess = new SqliteDataAcess();
            int ID = sqliteDataAcess.GenerateRaport(Types.CPU);
            float fcpu = LoadCPUData(p);
            string txtQueryCPU = "insert into CpuUsage (Raport_ID, Usage)values('"+ID+"', '" + (int)fcpu + "')";
            sqliteDataAcess.ExecuteQuery(txtQueryCPU, "Monitor.db");
        }
        public void LoadRAMUsageToDatabase(PerformanceCounter p)
        {
            sqliteDataAcess = new SqliteDataAcess();
            int ID = sqliteDataAcess.GenerateRaport(Types.RAM);
            float fram = LoadRAMData(p);
            string txtQueryRAM = "insert into MemoryUsage (Raport_ID, Usage)values('"+ID+"', '" + (int)fram + "')";
            sqliteDataAcess.ExecuteQuery(txtQueryRAM, "Monitor.db");
        }
        
    }
}
