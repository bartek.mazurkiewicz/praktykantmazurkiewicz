﻿namespace Monitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sqLiteCommandBuilder1 = new System.Data.SQLite.SQLiteCommandBuilder();
            this.cpuCounter = new System.Diagnostics.PerformanceCounter();
            this.ramCounter = new System.Diagnostics.PerformanceCounter();
            this.timerUsage = new System.Windows.Forms.Timer(this.components);
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.stopMonitorServiceButton = new System.Windows.Forms.Button();
            this.runMonitorServiceButton = new System.Windows.Forms.Button();
            this.unistallMonitorServiceButton = new System.Windows.Forms.Button();
            this.stopKeyloggerButton = new System.Windows.Forms.Button();
            this.runKeyloggerButton = new System.Windows.Forms.Button();
            this.installMonitorServiceButton = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.noteTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.addRAMUsageDataButton = new System.Windows.Forms.Button();
            this.exitUsageDataButton = new System.Windows.Forms.Button();
            this.clearUsageDataButton = new System.Windows.Forms.Button();
            this.nullValueUsageButton = new System.Windows.Forms.Button();
            this.openCPUUsageDataButton = new System.Windows.Forms.Button();
            this.addCPUUsageDataButton = new System.Windows.Forms.Button();
            this.stopUsageButton = new System.Windows.Forms.Button();
            this.startUsageButton = new System.Windows.Forms.Button();
            this.usageDataGridView = new System.Windows.Forms.DataGridView();
            this.ramUsageLabel = new System.Windows.Forms.Label();
            this.cpuUsageLabel = new System.Windows.Forms.Label();
            this.ramProgressBar = new System.Windows.Forms.ProgressBar();
            this.cpuProgressBar = new System.Windows.Forms.ProgressBar();
            this.ramLabel = new System.Windows.Forms.Label();
            this.cpuLabel = new System.Windows.Forms.Label();
            this.openRamUsageDataButton = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.runProcessButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.processNameTextBox = new System.Windows.Forms.TextBox();
            this.killProcessButton = new System.Windows.Forms.Button();
            this.refreshProcessesButton = new System.Windows.Forms.Button();
            this.processDataGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.browseRaportsButton = new System.Windows.Forms.Button();
            this.exitRaportDataButton = new System.Windows.Forms.Button();
            this.clearAllTablesButton = new System.Windows.Forms.Button();
            this.runRaportDataBaseButton = new System.Windows.Forms.Button();
            this.raportGridView = new System.Windows.Forms.DataGridView();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.monitorServiceNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.cpuCounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramCounter)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usageDataGridView)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.processDataGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.raportGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorServiceNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // sqLiteCommandBuilder1
            // 
            this.sqLiteCommandBuilder1.DataAdapter = null;
            this.sqLiteCommandBuilder1.QuoteSuffix = "]";
            // 
            // cpuCounter
            // 
            this.cpuCounter.CategoryName = "Processor";
            this.cpuCounter.CounterName = "% Processor Time";
            this.cpuCounter.InstanceName = "_Total";
            // 
            // ramCounter
            // 
            this.ramCounter.CategoryName = "Memory";
            this.ramCounter.CounterName = "% Committed Bytes In Use";
            // 
            // timerUsage
            // 
            this.timerUsage.Interval = 1000;
            this.timerUsage.Tick += new System.EventHandler(this.TimerUsage_Tick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.monitorServiceNumericUpDown);
            this.tabPage4.Controls.Add(this.stopMonitorServiceButton);
            this.tabPage4.Controls.Add(this.runMonitorServiceButton);
            this.tabPage4.Controls.Add(this.unistallMonitorServiceButton);
            this.tabPage4.Controls.Add(this.stopKeyloggerButton);
            this.tabPage4.Controls.Add(this.runKeyloggerButton);
            this.tabPage4.Controls.Add(this.installMonitorServiceButton);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(793, 424);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Usługi";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // stopMonitorServiceButton
            // 
            this.stopMonitorServiceButton.BackColor = System.Drawing.Color.Red;
            this.stopMonitorServiceButton.Location = new System.Drawing.Point(6, 47);
            this.stopMonitorServiceButton.Name = "stopMonitorServiceButton";
            this.stopMonitorServiceButton.Size = new System.Drawing.Size(146, 35);
            this.stopMonitorServiceButton.TabIndex = 5;
            this.stopMonitorServiceButton.Text = "Wyłącz monitor";
            this.stopMonitorServiceButton.UseVisualStyleBackColor = false;
            this.stopMonitorServiceButton.Visible = false;
            this.stopMonitorServiceButton.Click += new System.EventHandler(this.StopMonitorServiceButton_Click);
            // 
            // runMonitorServiceButton
            // 
            this.runMonitorServiceButton.BackColor = System.Drawing.Color.Lime;
            this.runMonitorServiceButton.Location = new System.Drawing.Point(6, 47);
            this.runMonitorServiceButton.Name = "runMonitorServiceButton";
            this.runMonitorServiceButton.Size = new System.Drawing.Size(146, 35);
            this.runMonitorServiceButton.TabIndex = 4;
            this.runMonitorServiceButton.Text = "Włącz monitor";
            this.runMonitorServiceButton.UseVisualStyleBackColor = false;
            this.runMonitorServiceButton.Click += new System.EventHandler(this.RunMonitorServiceButton_Click_1);
            // 
            // unistallMonitorServiceButton
            // 
            this.unistallMonitorServiceButton.BackColor = System.Drawing.Color.Red;
            this.unistallMonitorServiceButton.Location = new System.Drawing.Point(6, 6);
            this.unistallMonitorServiceButton.Name = "unistallMonitorServiceButton";
            this.unistallMonitorServiceButton.Size = new System.Drawing.Size(146, 35);
            this.unistallMonitorServiceButton.TabIndex = 3;
            this.unistallMonitorServiceButton.Text = "Odinstaluj monitor";
            this.unistallMonitorServiceButton.UseVisualStyleBackColor = false;
            this.unistallMonitorServiceButton.Visible = false;
            this.unistallMonitorServiceButton.Click += new System.EventHandler(this.UnistallMonitorServiceButton_Click);
            // 
            // stopKeyloggerButton
            // 
            this.stopKeyloggerButton.BackColor = System.Drawing.Color.Red;
            this.stopKeyloggerButton.Location = new System.Drawing.Point(6, 88);
            this.stopKeyloggerButton.Name = "stopKeyloggerButton";
            this.stopKeyloggerButton.Size = new System.Drawing.Size(146, 35);
            this.stopKeyloggerButton.TabIndex = 2;
            this.stopKeyloggerButton.Text = "Wyłącz keylogger";
            this.stopKeyloggerButton.UseVisualStyleBackColor = false;
            this.stopKeyloggerButton.Visible = false;
            this.stopKeyloggerButton.Click += new System.EventHandler(this.StopKeyloggerButton_Click);
            // 
            // runKeyloggerButton
            // 
            this.runKeyloggerButton.BackColor = System.Drawing.Color.Lime;
            this.runKeyloggerButton.Location = new System.Drawing.Point(6, 88);
            this.runKeyloggerButton.Name = "runKeyloggerButton";
            this.runKeyloggerButton.Size = new System.Drawing.Size(146, 35);
            this.runKeyloggerButton.TabIndex = 1;
            this.runKeyloggerButton.Text = "Włącz keylogger";
            this.runKeyloggerButton.UseVisualStyleBackColor = false;
            this.runKeyloggerButton.Click += new System.EventHandler(this.RunKeyloggerButton_Click);
            // 
            // installMonitorServiceButton
            // 
            this.installMonitorServiceButton.BackColor = System.Drawing.Color.Lime;
            this.installMonitorServiceButton.Location = new System.Drawing.Point(6, 6);
            this.installMonitorServiceButton.Name = "installMonitorServiceButton";
            this.installMonitorServiceButton.Size = new System.Drawing.Size(146, 35);
            this.installMonitorServiceButton.TabIndex = 0;
            this.installMonitorServiceButton.Text = "Zainstaluj monitor";
            this.installMonitorServiceButton.UseVisualStyleBackColor = false;
            this.installMonitorServiceButton.Click += new System.EventHandler(this.RunMonitorServiceButton_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.noteTextBox);
            this.tabPage3.Controls.Add(this.menuStrip1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(793, 424);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Notatki";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // noteTextBox
            // 
            this.noteTextBox.Location = new System.Drawing.Point(0, 34);
            this.noteTextBox.Multiline = true;
            this.noteTextBox.Name = "noteTextBox";
            this.noteTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.noteTextBox.Size = new System.Drawing.Size(793, 353);
            this.noteTextBox.TabIndex = 4;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(787, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "Plik";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.newToolStripMenuItem.Text = "&Nowy";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.NewToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.openToolStripMenuItem.Text = "&Otwórz";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.saveToolStripMenuItem.Text = "&Zapisz";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(194, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.exitToolStripMenuItem.Text = "Zakończ";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.addRAMUsageDataButton);
            this.tabPage2.Controls.Add(this.exitUsageDataButton);
            this.tabPage2.Controls.Add(this.clearUsageDataButton);
            this.tabPage2.Controls.Add(this.nullValueUsageButton);
            this.tabPage2.Controls.Add(this.openCPUUsageDataButton);
            this.tabPage2.Controls.Add(this.addCPUUsageDataButton);
            this.tabPage2.Controls.Add(this.stopUsageButton);
            this.tabPage2.Controls.Add(this.startUsageButton);
            this.tabPage2.Controls.Add(this.usageDataGridView);
            this.tabPage2.Controls.Add(this.ramUsageLabel);
            this.tabPage2.Controls.Add(this.cpuUsageLabel);
            this.tabPage2.Controls.Add(this.ramProgressBar);
            this.tabPage2.Controls.Add(this.cpuProgressBar);
            this.tabPage2.Controls.Add(this.ramLabel);
            this.tabPage2.Controls.Add(this.cpuLabel);
            this.tabPage2.Controls.Add(this.openRamUsageDataButton);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(793, 424);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Monitor zasobów";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // addRAMUsageDataButton
            // 
            this.addRAMUsageDataButton.Location = new System.Drawing.Point(685, 313);
            this.addRAMUsageDataButton.Name = "addRAMUsageDataButton";
            this.addRAMUsageDataButton.Size = new System.Drawing.Size(108, 31);
            this.addRAMUsageDataButton.TabIndex = 15;
            this.addRAMUsageDataButton.Text = "Dodaj RAM";
            this.addRAMUsageDataButton.UseVisualStyleBackColor = true;
            this.addRAMUsageDataButton.Visible = false;
            this.addRAMUsageDataButton.Click += new System.EventHandler(this.AddRAMUsageDataButton_Click);
            // 
            // exitUsageDataButton
            // 
            this.exitUsageDataButton.Location = new System.Drawing.Point(467, 313);
            this.exitUsageDataButton.Name = "exitUsageDataButton";
            this.exitUsageDataButton.Size = new System.Drawing.Size(108, 31);
            this.exitUsageDataButton.TabIndex = 13;
            this.exitUsageDataButton.Text = "Schowaj bazę\r\n";
            this.exitUsageDataButton.UseVisualStyleBackColor = true;
            this.exitUsageDataButton.Visible = false;
            this.exitUsageDataButton.Click += new System.EventHandler(this.ExitUsageDataButton_Click);
            // 
            // clearUsageDataButton
            // 
            this.clearUsageDataButton.Location = new System.Drawing.Point(581, 313);
            this.clearUsageDataButton.Name = "clearUsageDataButton";
            this.clearUsageDataButton.Size = new System.Drawing.Size(98, 31);
            this.clearUsageDataButton.TabIndex = 12;
            this.clearUsageDataButton.Text = "Wyczyść";
            this.clearUsageDataButton.UseVisualStyleBackColor = true;
            this.clearUsageDataButton.Visible = false;
            this.clearUsageDataButton.Click += new System.EventHandler(this.ClearUsageDataButton_Click);
            // 
            // nullValueUsageButton
            // 
            this.nullValueUsageButton.Enabled = false;
            this.nullValueUsageButton.Location = new System.Drawing.Point(316, 96);
            this.nullValueUsageButton.Name = "nullValueUsageButton";
            this.nullValueUsageButton.Size = new System.Drawing.Size(108, 34);
            this.nullValueUsageButton.TabIndex = 11;
            this.nullValueUsageButton.Text = "Wyzeruj";
            this.nullValueUsageButton.UseVisualStyleBackColor = true;
            this.nullValueUsageButton.Click += new System.EventHandler(this.NullValueUsageButton_Click);
            // 
            // openCPUUsageDataButton
            // 
            this.openCPUUsageDataButton.Location = new System.Drawing.Point(467, 313);
            this.openCPUUsageDataButton.Name = "openCPUUsageDataButton";
            this.openCPUUsageDataButton.Size = new System.Drawing.Size(108, 31);
            this.openCPUUsageDataButton.TabIndex = 10;
            this.openCPUUsageDataButton.Text = "Pokaż CPU";
            this.openCPUUsageDataButton.UseVisualStyleBackColor = true;
            this.openCPUUsageDataButton.Click += new System.EventHandler(this.OpenUsageDataButton_Click);
            // 
            // addCPUUsageDataButton
            // 
            this.addCPUUsageDataButton.Location = new System.Drawing.Point(685, 313);
            this.addCPUUsageDataButton.Name = "addCPUUsageDataButton";
            this.addCPUUsageDataButton.Size = new System.Drawing.Size(108, 31);
            this.addCPUUsageDataButton.TabIndex = 9;
            this.addCPUUsageDataButton.Text = "Dodaj CPU";
            this.addCPUUsageDataButton.UseVisualStyleBackColor = true;
            this.addCPUUsageDataButton.Visible = false;
            this.addCPUUsageDataButton.Click += new System.EventHandler(this.UsageDatabaseButton_Click);
            // 
            // stopUsageButton
            // 
            this.stopUsageButton.Enabled = false;
            this.stopUsageButton.Location = new System.Drawing.Point(202, 96);
            this.stopUsageButton.Name = "stopUsageButton";
            this.stopUsageButton.Size = new System.Drawing.Size(108, 34);
            this.stopUsageButton.TabIndex = 8;
            this.stopUsageButton.Text = "Stop";
            this.stopUsageButton.UseVisualStyleBackColor = true;
            this.stopUsageButton.Click += new System.EventHandler(this.StopUsageButton_Click);
            // 
            // startUsageButton
            // 
            this.startUsageButton.Location = new System.Drawing.Point(88, 96);
            this.startUsageButton.Name = "startUsageButton";
            this.startUsageButton.Size = new System.Drawing.Size(108, 34);
            this.startUsageButton.TabIndex = 1;
            this.startUsageButton.Text = "Pokaż zużycie";
            this.startUsageButton.UseVisualStyleBackColor = true;
            this.startUsageButton.Click += new System.EventHandler(this.StartUsageButton_Click);
            // 
            // usageDataGridView
            // 
            this.usageDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.usageDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.usageDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usageDataGridView.Location = new System.Drawing.Point(467, 0);
            this.usageDataGridView.Name = "usageDataGridView";
            this.usageDataGridView.RowHeadersWidth = 51;
            this.usageDataGridView.RowTemplate.Height = 24;
            this.usageDataGridView.Size = new System.Drawing.Size(326, 307);
            this.usageDataGridView.TabIndex = 7;
            // 
            // ramUsageLabel
            // 
            this.ramUsageLabel.AutoSize = true;
            this.ramUsageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ramUsageLabel.Location = new System.Drawing.Point(367, 58);
            this.ramUsageLabel.Name = "ramUsageLabel";
            this.ramUsageLabel.Size = new System.Drawing.Size(56, 32);
            this.ramUsageLabel.TabIndex = 6;
            this.ramUsageLabel.Text = "0%";
            // 
            // cpuUsageLabel
            // 
            this.cpuUsageLabel.AutoSize = true;
            this.cpuUsageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cpuUsageLabel.Location = new System.Drawing.Point(367, 8);
            this.cpuUsageLabel.Name = "cpuUsageLabel";
            this.cpuUsageLabel.Size = new System.Drawing.Size(56, 32);
            this.cpuUsageLabel.TabIndex = 4;
            this.cpuUsageLabel.Text = "0%";
            // 
            // ramProgressBar
            // 
            this.ramProgressBar.Location = new System.Drawing.Point(88, 58);
            this.ramProgressBar.Name = "ramProgressBar";
            this.ramProgressBar.Size = new System.Drawing.Size(273, 32);
            this.ramProgressBar.TabIndex = 3;
            // 
            // cpuProgressBar
            // 
            this.cpuProgressBar.Location = new System.Drawing.Point(88, 8);
            this.cpuProgressBar.Name = "cpuProgressBar";
            this.cpuProgressBar.Size = new System.Drawing.Size(273, 32);
            this.cpuProgressBar.TabIndex = 2;
            // 
            // ramLabel
            // 
            this.ramLabel.AutoSize = true;
            this.ramLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ramLabel.Location = new System.Drawing.Point(8, 58);
            this.ramLabel.Name = "ramLabel";
            this.ramLabel.Size = new System.Drawing.Size(77, 32);
            this.ramLabel.TabIndex = 1;
            this.ramLabel.Text = "RAM";
            // 
            // cpuLabel
            // 
            this.cpuLabel.AutoSize = true;
            this.cpuLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cpuLabel.Location = new System.Drawing.Point(8, 8);
            this.cpuLabel.Name = "cpuLabel";
            this.cpuLabel.Size = new System.Drawing.Size(74, 32);
            this.cpuLabel.TabIndex = 0;
            this.cpuLabel.Text = "CPU";
            // 
            // openRamUsageDataButton
            // 
            this.openRamUsageDataButton.Location = new System.Drawing.Point(581, 313);
            this.openRamUsageDataButton.Name = "openRamUsageDataButton";
            this.openRamUsageDataButton.Size = new System.Drawing.Size(108, 31);
            this.openRamUsageDataButton.TabIndex = 14;
            this.openRamUsageDataButton.Text = "Pokaż RAM";
            this.openRamUsageDataButton.UseVisualStyleBackColor = true;
            this.openRamUsageDataButton.Click += new System.EventHandler(this.OpenRamUsageDataButton_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.runProcessButton);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.processNameTextBox);
            this.tabPage1.Controls.Add(this.killProcessButton);
            this.tabPage1.Controls.Add(this.refreshProcessesButton);
            this.tabPage1.Controls.Add(this.processDataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(793, 424);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Monitor procesów";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // runProcessButton
            // 
            this.runProcessButton.Location = new System.Drawing.Point(690, 34);
            this.runProcessButton.Name = "runProcessButton";
            this.runProcessButton.Size = new System.Drawing.Size(94, 23);
            this.runProcessButton.TabIndex = 6;
            this.runProcessButton.Text = "Uruchom";
            this.runProcessButton.UseVisualStyleBackColor = true;
            this.runProcessButton.Click += new System.EventHandler(this.RunProcessButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(539, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Wpisz nazwę procesu";
            // 
            // processNameTextBox
            // 
            this.processNameTextBox.Location = new System.Drawing.Point(542, 35);
            this.processNameTextBox.Name = "processNameTextBox";
            this.processNameTextBox.Size = new System.Drawing.Size(142, 22);
            this.processNameTextBox.TabIndex = 4;
            // 
            // killProcessButton
            // 
            this.killProcessButton.Location = new System.Drawing.Point(267, 325);
            this.killProcessButton.Name = "killProcessButton";
            this.killProcessButton.Size = new System.Drawing.Size(266, 35);
            this.killProcessButton.TabIndex = 3;
            this.killProcessButton.Text = "Zabij proces";
            this.killProcessButton.UseVisualStyleBackColor = true;
            this.killProcessButton.Click += new System.EventHandler(this.KillProcessButton_Click);
            // 
            // refreshProcessesButton
            // 
            this.refreshProcessesButton.Location = new System.Drawing.Point(0, 325);
            this.refreshProcessesButton.Name = "refreshProcessesButton";
            this.refreshProcessesButton.Size = new System.Drawing.Size(261, 35);
            this.refreshProcessesButton.TabIndex = 1;
            this.refreshProcessesButton.Text = "Odswież procesy";
            this.refreshProcessesButton.UseVisualStyleBackColor = true;
            this.refreshProcessesButton.Click += new System.EventHandler(this.RefreshProcessesButton_Click);
            // 
            // processDataGridView
            // 
            this.processDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.processDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.processDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.processDataGridView.Location = new System.Drawing.Point(3, 15);
            this.processDataGridView.Name = "processDataGridView";
            this.processDataGridView.RowHeadersWidth = 51;
            this.processDataGridView.RowTemplate.Height = 24;
            this.processDataGridView.Size = new System.Drawing.Size(530, 304);
            this.processDataGridView.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(801, 453);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.pictureBox1);
            this.tabPage5.Controls.Add(this.browseRaportsButton);
            this.tabPage5.Controls.Add(this.exitRaportDataButton);
            this.tabPage5.Controls.Add(this.clearAllTablesButton);
            this.tabPage5.Controls.Add(this.runRaportDataBaseButton);
            this.tabPage5.Controls.Add(this.raportGridView);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(793, 424);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Raporty";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(374, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(410, 328);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // browseRaportsButton
            // 
            this.browseRaportsButton.Location = new System.Drawing.Point(374, 341);
            this.browseRaportsButton.Name = "browseRaportsButton";
            this.browseRaportsButton.Size = new System.Drawing.Size(410, 34);
            this.browseRaportsButton.TabIndex = 4;
            this.browseRaportsButton.Text = "Przeglądaj raporty";
            this.browseRaportsButton.UseVisualStyleBackColor = true;
            this.browseRaportsButton.Click += new System.EventHandler(this.BrowseRaportsButton_Click);
            // 
            // exitRaportDataButton
            // 
            this.exitRaportDataButton.Location = new System.Drawing.Point(3, 341);
            this.exitRaportDataButton.Name = "exitRaportDataButton";
            this.exitRaportDataButton.Size = new System.Drawing.Size(108, 34);
            this.exitRaportDataButton.TabIndex = 3;
            this.exitRaportDataButton.Text = "Schowaj bazę";
            this.exitRaportDataButton.UseVisualStyleBackColor = true;
            this.exitRaportDataButton.Visible = false;
            this.exitRaportDataButton.Click += new System.EventHandler(this.ExitRaportDataButton_Click);
            // 
            // clearAllTablesButton
            // 
            this.clearAllTablesButton.Location = new System.Drawing.Point(117, 341);
            this.clearAllTablesButton.Name = "clearAllTablesButton";
            this.clearAllTablesButton.Size = new System.Drawing.Size(237, 34);
            this.clearAllTablesButton.TabIndex = 2;
            this.clearAllTablesButton.Text = "Wyczyść wszystkie tabele";
            this.clearAllTablesButton.UseVisualStyleBackColor = true;
            this.clearAllTablesButton.Visible = false;
            this.clearAllTablesButton.Click += new System.EventHandler(this.ClearAllTablesButton_Click);
            // 
            // runRaportDataBaseButton
            // 
            this.runRaportDataBaseButton.Location = new System.Drawing.Point(3, 341);
            this.runRaportDataBaseButton.Name = "runRaportDataBaseButton";
            this.runRaportDataBaseButton.Size = new System.Drawing.Size(108, 34);
            this.runRaportDataBaseButton.TabIndex = 1;
            this.runRaportDataBaseButton.Text = "Włącz bazę";
            this.runRaportDataBaseButton.UseVisualStyleBackColor = true;
            this.runRaportDataBaseButton.Click += new System.EventHandler(this.RunRaportDataBaseButton_Click);
            // 
            // raportGridView
            // 
            this.raportGridView.BackgroundColor = System.Drawing.Color.White;
            this.raportGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.raportGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.raportGridView.Location = new System.Drawing.Point(3, 3);
            this.raportGridView.Name = "raportGridView";
            this.raportGridView.RowHeadersWidth = 51;
            this.raportGridView.RowTemplate.Height = 24;
            this.raportGridView.Size = new System.Drawing.Size(351, 332);
            this.raportGridView.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // monitorServiceNumericUpDown
            // 
            this.monitorServiceNumericUpDown.Location = new System.Drawing.Point(158, 13);
            this.monitorServiceNumericUpDown.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.monitorServiceNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.monitorServiceNumericUpDown.Name = "monitorServiceNumericUpDown";
            this.monitorServiceNumericUpDown.Size = new System.Drawing.Size(42, 22);
            this.monitorServiceNumericUpDown.TabIndex = 6;
            this.monitorServiceNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 412);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monitor";
            ((System.ComponentModel.ISupportInitialize)(this.cpuCounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramCounter)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usageDataGridView)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.processDataGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.raportGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorServiceNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Data.SQLite.SQLiteCommandBuilder sqLiteCommandBuilder1;
        private System.Diagnostics.PerformanceCounter cpuCounter;
        private System.Diagnostics.PerformanceCounter ramCounter;
        private System.Windows.Forms.Timer timerUsage;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button exitUsageDataButton;
        private System.Windows.Forms.Button clearUsageDataButton;
        private System.Windows.Forms.Button nullValueUsageButton;
        private System.Windows.Forms.Button openCPUUsageDataButton;
        private System.Windows.Forms.Button addCPUUsageDataButton;
        private System.Windows.Forms.Button stopUsageButton;
        private System.Windows.Forms.Button startUsageButton;
        private System.Windows.Forms.DataGridView usageDataGridView;
        private System.Windows.Forms.Label ramUsageLabel;
        private System.Windows.Forms.Label cpuUsageLabel;
        private System.Windows.Forms.ProgressBar ramProgressBar;
        private System.Windows.Forms.ProgressBar cpuProgressBar;
        private System.Windows.Forms.Label ramLabel;
        private System.Windows.Forms.Label cpuLabel;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button runProcessButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox processNameTextBox;
        private System.Windows.Forms.Button killProcessButton;
        private System.Windows.Forms.Button refreshProcessesButton;
        private System.Windows.Forms.DataGridView processDataGridView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button openRamUsageDataButton;
        private System.Windows.Forms.Button addRAMUsageDataButton;
        private System.Windows.Forms.TextBox noteTextBox;
        private System.Windows.Forms.Button installMonitorServiceButton;
        private System.Windows.Forms.Button runKeyloggerButton;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button clearAllTablesButton;
        private System.Windows.Forms.Button runRaportDataBaseButton;
        private System.Windows.Forms.DataGridView raportGridView;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button exitRaportDataButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button browseRaportsButton;
        private System.Windows.Forms.Button stopKeyloggerButton;
        private System.Windows.Forms.Button unistallMonitorServiceButton;
        private System.Windows.Forms.Button runMonitorServiceButton;
        private System.Windows.Forms.Button stopMonitorServiceButton;
        private System.Windows.Forms.NumericUpDown monitorServiceNumericUpDown;
    }
}

