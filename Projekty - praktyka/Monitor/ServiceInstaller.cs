﻿
using System;
using System.Collections;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using System.Windows.Forms;


namespace Monitor
{
    class ServiceInstaller
    {
        public void InstallService(string serviceName, Assembly assembly)
        {
            if (IsServiceInstalled(serviceName))
            {
                MessageBox.Show("Usługa " + serviceName + " jest już zainstalowana.", "Usługa jest zainstalowana", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            using (AssemblyInstaller installer = GetInstaller(assembly))
            {
                IDictionary state = new Hashtable();
                try
                {
                    installer.Install(state);
                    installer.Commit(state);
                    MessageBox.Show("Usługa " + serviceName + " została zainstalowana", "Usługa została zainstalowana", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    try
                    {
                        installer.Rollback(state);
                    }
                    catch { }
                    throw;
                }
            }
        }

        public static bool IsServiceInstalled(string serviceName)
        {
            using (ServiceController controller = new ServiceController(serviceName))
            {
                try
                {
                    ServiceControllerStatus status = controller.Status;
                }
                catch
                {
                    return false;
                }

                return true;
            }
        }

        private static AssemblyInstaller GetInstaller(Assembly assembly)
        {
            AssemblyInstaller installer = new AssemblyInstaller(assembly, null);
            installer.UseNewContext = true;

            return installer;
        }
        public void UnistallService(string logFilePath)
        {
            System.ServiceProcess.ServiceInstaller ServiceInstallerObj = new System.ServiceProcess.ServiceInstaller();
            InstallContext Context = new InstallContext(logFilePath, null);
            ServiceInstallerObj.Context = Context;
            ServiceInstallerObj.ServiceName = "MonitorService";
            ServiceInstallerObj.Uninstall(null);
            MessageBox.Show("Usługa została odinstalowana.", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
            service.Start();
            service.WaitForStatus(ServiceControllerStatus.Running, timeout);
        }
        public void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
            service.Stop();
            service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
        }
    }
}
