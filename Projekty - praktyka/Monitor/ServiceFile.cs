﻿using System.IO;
using System.Windows.Forms;

namespace Monitor
{
    class ServiceFile
    {
        public void ChangeFile(NumericUpDown numeric)
        {
            string path = @"C:\Users\barte\source\repos\praktyka\time.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            else
            {
                using (StreamWriter sw = File.CreateText(path))
                {

                    if (numeric.Value == 1)
                    {
                        sw.Write("60000");
                    }
                    else if (numeric.Value == 2)
                    {
                        sw.Write("120000");
                    }
                    else if (numeric.Value == 3)
                    {
                        sw.Write("180000");
                    }
                    else if (numeric.Value == 4)
                    {
                        sw.Write("240000");
                    }
                    else
                    { 
                        sw.Write("300000");
                    }
                }
            }
        }
    }
}
