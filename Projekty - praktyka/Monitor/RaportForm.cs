﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor
{
    public partial class RaportForm : Form
    {
        SqliteDataAcess sqliteDataAcces;
        public RaportForm()
        {
            InitializeComponent();
        }

        private void RaportForm_Load(object sender, EventArgs e)
        {
            
        }

        private void RunRaportButton_Click(object sender, EventArgs e)
        {
            try
            {
                sqliteDataAcces = new SqliteDataAcess();
                sqliteDataAcces.LoadRaport(cpuRaportGridView, sqliteDataAcces.SelectID(timeRaportComboBox), "CpuUsage");
                sqliteDataAcces.LoadRaport(ramRaportGridView, sqliteDataAcces.SelectID(timeRaportComboBox), "MemoryUsage");
                sqliteDataAcces.LoadRaport(processesRaportGridView, sqliteDataAcces.SelectID(timeRaportComboBox), "Processes");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            //string s = sqliteDataAcces.SelectTime(dateTimePicker1);
            //MessageBox.Show(s);
        }

        private void DateTimePickerRaport_ValueChanged(object sender, EventArgs e)
        {
        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            timeRaportComboBox.Text = "";
            sqliteDataAcces = new SqliteDataAcess();
            List<string> time = sqliteDataAcces.SelectTime(dateTimePicker1);
            timeRaportComboBox.DataSource = time;
        }
    }
}
