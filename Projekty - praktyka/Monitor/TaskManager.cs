﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data;

namespace Monitor
{
    class TaskManager
    {
        public int killAProcessReturn = 0;
        public int runAProcessReturn = 0;
        Process[] allProcesses;
        SqliteDataAcess sqliteDataAcess;
        private string dataBase = "Monitor.db";
        

        public void ListAllProcesses()
        {
            allProcesses = Process.GetProcesses();
            sqliteDataAcess = new SqliteDataAcess();
            int ID = sqliteDataAcess.GenerateRaport(Types.Processes);
            foreach (Process process in allProcesses)
            {
                string txtQuery = "insert into Processes (Raport_ID, Name, PID)values('"+ID+"','" + process.ProcessName + "','" + process.Id + "')";
                sqliteDataAcess.ExecuteQuery(txtQuery, dataBase);
            }
        }
        public void KillAProcess(DataGridView dataGridView)
        {
            try
            {
                Process process = Process.GetProcessById(Int32.Parse(dataGridView.SelectedRows[0].Cells[2].Value.ToString()));
                process.Kill();
                MessageBox.Show("Pomyślnie zabiłeś proces " + dataGridView.SelectedRows[0].Cells[1].Value.ToString() + ".", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
                killAProcessReturn = 1;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Zaznacz cały indeks!", "Błąd indeksu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                killAProcessReturn = 0;
            }
            catch(System.NullReferenceException)
            {
                MessageBox.Show("Zaznacz cały indeks!", "Błąd indeksu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                killAProcessReturn = 0;
            }
            
        }
        public void RunAProcess(TextBox textBox)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = textBox.Text;
                process.Start();
                runAProcessReturn = 1;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                MessageBox.Show("Nie ma takiego programu", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                runAProcessReturn = 0;
            }
            catch (System.InvalidOperationException)
            {
                MessageBox.Show("Wpisz nazwę programu", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                runAProcessReturn = 0;
            }
        }
    }
}
