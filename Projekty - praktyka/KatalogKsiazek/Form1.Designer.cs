﻿namespace KatalogKsiazek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bookCatalogControl = new System.Windows.Forms.TabControl();
            this.addPage = new System.Windows.Forms.TabPage();
            this.addBookButton = new System.Windows.Forms.Button();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.thirdYearRadioButton = new System.Windows.Forms.RadioButton();
            this.secondYearRadioButton = new System.Windows.Forms.RadioButton();
            this.firstYearRadioButton = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.simpleRadioButton = new System.Windows.Forms.RadioButton();
            this.whiteCrownRadioButton = new System.Windows.Forms.RadioButton();
            this.readingRadioButton = new System.Windows.Forms.RadioButton();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.printTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openPage = new System.Windows.Forms.TabPage();
            this.crownViewButton = new System.Windows.Forms.Button();
            this.readingViewButton = new System.Windows.Forms.Button();
            this.simpleViewButton = new System.Windows.Forms.Button();
            this.openGridView = new System.Windows.Forms.DataGridView();
            this.searchPage = new System.Windows.Forms.TabPage();
            this.searchGridView = new System.Windows.Forms.DataGridView();
            this.searchTitleButton = new System.Windows.Forms.Button();
            this.searchTypeComboBox = new System.Windows.Forms.ComboBox();
            this.searchTitleTextBox = new System.Windows.Forms.TextBox();
            this.searchPrintTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.editPage = new System.Windows.Forms.TabPage();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.editIDBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.editGridView = new System.Windows.Forms.DataGridView();
            this.editTypeBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.editThirdRadio = new System.Windows.Forms.RadioButton();
            this.editSecondRadio = new System.Windows.Forms.RadioButton();
            this.editFirstRadio = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.editSimpleRadio = new System.Windows.Forms.RadioButton();
            this.editCrownRadio = new System.Windows.Forms.RadioButton();
            this.editReadingRadio = new System.Windows.Forms.RadioButton();
            this.editDescriptionBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.editTitleBox = new System.Windows.Forms.TextBox();
            this.editPrintBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.historyTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.editHistoryTextBox = new System.Windows.Forms.TextBox();
            this.editCrownButton = new System.Windows.Forms.Button();
            this.editReadingButton = new System.Windows.Forms.Button();
            this.editSimpleButton = new System.Windows.Forms.Button();
            this.searchCrownButton = new System.Windows.Forms.Button();
            this.searchReadingButton = new System.Windows.Forms.Button();
            this.searchSimpleButton = new System.Windows.Forms.Button();
            this.searchTypeButton = new System.Windows.Forms.Button();
            this.searchPrintButton = new System.Windows.Forms.Button();
            this.bookCatalogControl.SuspendLayout();
            this.addPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.openPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.openGridView)).BeginInit();
            this.searchPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchGridView)).BeginInit();
            this.editPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bookCatalogControl
            // 
            this.bookCatalogControl.Controls.Add(this.addPage);
            this.bookCatalogControl.Controls.Add(this.editPage);
            this.bookCatalogControl.Controls.Add(this.openPage);
            this.bookCatalogControl.Controls.Add(this.searchPage);
            this.bookCatalogControl.Location = new System.Drawing.Point(0, 0);
            this.bookCatalogControl.Name = "bookCatalogControl";
            this.bookCatalogControl.SelectedIndex = 0;
            this.bookCatalogControl.Size = new System.Drawing.Size(747, 462);
            this.bookCatalogControl.TabIndex = 3;
            // 
            // addPage
            // 
            this.addPage.Controls.Add(this.pictureBox1);
            this.addPage.Controls.Add(this.label7);
            this.addPage.Controls.Add(this.historyTextBox);
            this.addPage.Controls.Add(this.addBookButton);
            this.addPage.Controls.Add(this.typeComboBox);
            this.addPage.Controls.Add(this.label6);
            this.addPage.Controls.Add(this.groupBox2);
            this.addPage.Controls.Add(this.label5);
            this.addPage.Controls.Add(this.groupBox1);
            this.addPage.Controls.Add(this.descriptionTextBox);
            this.addPage.Controls.Add(this.label4);
            this.addPage.Controls.Add(this.titleTextBox);
            this.addPage.Controls.Add(this.printTextBox);
            this.addPage.Controls.Add(this.label2);
            this.addPage.Controls.Add(this.label1);
            this.addPage.Controls.Add(this.label3);
            this.addPage.Location = new System.Drawing.Point(4, 25);
            this.addPage.Name = "addPage";
            this.addPage.Padding = new System.Windows.Forms.Padding(3);
            this.addPage.Size = new System.Drawing.Size(739, 433);
            this.addPage.TabIndex = 0;
            this.addPage.Text = "Dodaj";
            this.addPage.UseVisualStyleBackColor = true;
            // 
            // addBookButton
            // 
            this.addBookButton.Location = new System.Drawing.Point(11, 128);
            this.addBookButton.Name = "addBookButton";
            this.addBookButton.Size = new System.Drawing.Size(137, 84);
            this.addBookButton.TabIndex = 13;
            this.addBookButton.Text = "Dodaj do bazy";
            this.addBookButton.UseVisualStyleBackColor = true;
            this.addBookButton.Click += new System.EventHandler(this.AddBookButton_Click);
            // 
            // typeComboBox
            // 
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Items.AddRange(new object[] {
            "Komedia",
            "Dramat",
            "Dramat romantyczny",
            "Tragedia",
            "Ballada",
            "Fantasy",
            "Manga",
            "Edukacyjna"});
            this.typeComboBox.Location = new System.Drawing.Point(9, 86);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(159, 24);
            this.typeComboBox.TabIndex = 12;
            this.typeComboBox.Text = "Komedia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 311);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Rok kształcenia";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.thirdYearRadioButton);
            this.groupBox2.Controls.Add(this.secondYearRadioButton);
            this.groupBox2.Controls.Add(this.firstYearRadioButton);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(188, 331);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(157, 97);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // thirdYearRadioButton
            // 
            this.thirdYearRadioButton.AutoSize = true;
            this.thirdYearRadioButton.Location = new System.Drawing.Point(6, 70);
            this.thirdYearRadioButton.Name = "thirdYearRadioButton";
            this.thirdYearRadioButton.Size = new System.Drawing.Size(61, 21);
            this.thirdYearRadioButton.TabIndex = 2;
            this.thirdYearRadioButton.Text = "3 rok";
            this.thirdYearRadioButton.UseVisualStyleBackColor = true;
            // 
            // secondYearRadioButton
            // 
            this.secondYearRadioButton.AutoSize = true;
            this.secondYearRadioButton.Location = new System.Drawing.Point(6, 43);
            this.secondYearRadioButton.Name = "secondYearRadioButton";
            this.secondYearRadioButton.Size = new System.Drawing.Size(61, 21);
            this.secondYearRadioButton.TabIndex = 1;
            this.secondYearRadioButton.Text = "2 rok";
            this.secondYearRadioButton.UseVisualStyleBackColor = true;
            // 
            // firstYearRadioButton
            // 
            this.firstYearRadioButton.AutoSize = true;
            this.firstYearRadioButton.Checked = true;
            this.firstYearRadioButton.Location = new System.Drawing.Point(6, 16);
            this.firstYearRadioButton.Name = "firstYearRadioButton";
            this.firstYearRadioButton.Size = new System.Drawing.Size(61, 21);
            this.firstYearRadioButton.TabIndex = 0;
            this.firstYearRadioButton.TabStop = true;
            this.firstYearRadioButton.Text = "1 rok";
            this.firstYearRadioButton.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(185, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Typ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.simpleRadioButton);
            this.groupBox1.Controls.Add(this.whiteCrownRadioButton);
            this.groupBox1.Controls.Add(this.readingRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(188, 206);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(157, 87);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // simpleRadioButton
            // 
            this.simpleRadioButton.AutoSize = true;
            this.simpleRadioButton.Checked = true;
            this.simpleRadioButton.Location = new System.Drawing.Point(6, 11);
            this.simpleRadioButton.Name = "simpleRadioButton";
            this.simpleRadioButton.Size = new System.Drawing.Size(123, 21);
            this.simpleRadioButton.TabIndex = 2;
            this.simpleRadioButton.TabStop = true;
            this.simpleRadioButton.Text = "Zwykła książka";
            this.simpleRadioButton.UseVisualStyleBackColor = true;
            // 
            // whiteCrownRadioButton
            // 
            this.whiteCrownRadioButton.AutoSize = true;
            this.whiteCrownRadioButton.Location = new System.Drawing.Point(6, 60);
            this.whiteCrownRadioButton.Name = "whiteCrownRadioButton";
            this.whiteCrownRadioButton.Size = new System.Drawing.Size(90, 21);
            this.whiteCrownRadioButton.TabIndex = 1;
            this.whiteCrownRadioButton.Text = "Biały kruk";
            this.whiteCrownRadioButton.UseVisualStyleBackColor = true;
            this.whiteCrownRadioButton.CheckedChanged += new System.EventHandler(this.WhiteCrownRadioButton_CheckedChanged);
            // 
            // readingRadioButton
            // 
            this.readingRadioButton.AutoSize = true;
            this.readingRadioButton.Location = new System.Drawing.Point(6, 35);
            this.readingRadioButton.Name = "readingRadioButton";
            this.readingRadioButton.Size = new System.Drawing.Size(77, 21);
            this.readingRadioButton.TabIndex = 0;
            this.readingRadioButton.Text = "Lektura";
            this.readingRadioButton.UseVisualStyleBackColor = true;
            this.readingRadioButton.CheckedChanged += new System.EventHandler(this.ReadingRadioButton_CheckedChanged);
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Location = new System.Drawing.Point(174, 86);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(201, 97);
            this.descriptionTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Opis";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(8, 23);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(160, 22);
            this.titleTextBox.TabIndex = 4;
            // 
            // printTextBox
            // 
            this.printTextBox.Location = new System.Drawing.Point(174, 23);
            this.printTextBox.Name = "printTextBox";
            this.printTextBox.Size = new System.Drawing.Size(159, 22);
            this.printTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Gatunek";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tytuł";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(171, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Wydawnictwo";
            // 
            // openPage
            // 
            this.openPage.Controls.Add(this.crownViewButton);
            this.openPage.Controls.Add(this.readingViewButton);
            this.openPage.Controls.Add(this.simpleViewButton);
            this.openPage.Controls.Add(this.openGridView);
            this.openPage.Location = new System.Drawing.Point(4, 25);
            this.openPage.Name = "openPage";
            this.openPage.Padding = new System.Windows.Forms.Padding(3);
            this.openPage.Size = new System.Drawing.Size(739, 433);
            this.openPage.TabIndex = 1;
            this.openPage.Text = "Pełny podgląd";
            this.openPage.UseVisualStyleBackColor = true;
            // 
            // crownViewButton
            // 
            this.crownViewButton.Location = new System.Drawing.Point(204, 393);
            this.crownViewButton.Name = "crownViewButton";
            this.crownViewButton.Size = new System.Drawing.Size(91, 34);
            this.crownViewButton.TabIndex = 41;
            this.crownViewButton.Text = "Białe kruki";
            this.crownViewButton.UseVisualStyleBackColor = true;
            this.crownViewButton.Click += new System.EventHandler(this.CrownViewButton_Click);
            // 
            // readingViewButton
            // 
            this.readingViewButton.Location = new System.Drawing.Point(123, 393);
            this.readingViewButton.Name = "readingViewButton";
            this.readingViewButton.Size = new System.Drawing.Size(75, 34);
            this.readingViewButton.TabIndex = 40;
            this.readingViewButton.Text = "Lektury";
            this.readingViewButton.UseVisualStyleBackColor = true;
            this.readingViewButton.Click += new System.EventHandler(this.ReadingViewButton_Click);
            // 
            // simpleViewButton
            // 
            this.simpleViewButton.Location = new System.Drawing.Point(3, 393);
            this.simpleViewButton.Name = "simpleViewButton";
            this.simpleViewButton.Size = new System.Drawing.Size(114, 34);
            this.simpleViewButton.TabIndex = 39;
            this.simpleViewButton.Text = "Zwykłe książki";
            this.simpleViewButton.UseVisualStyleBackColor = true;
            this.simpleViewButton.Click += new System.EventHandler(this.SimpleViewButton_Click);
            // 
            // openGridView
            // 
            this.openGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.openGridView.Location = new System.Drawing.Point(3, 0);
            this.openGridView.Name = "openGridView";
            this.openGridView.RowHeadersWidth = 51;
            this.openGridView.RowTemplate.Height = 24;
            this.openGridView.Size = new System.Drawing.Size(736, 387);
            this.openGridView.TabIndex = 38;
            // 
            // searchPage
            // 
            this.searchPage.Controls.Add(this.searchPrintButton);
            this.searchPage.Controls.Add(this.searchTypeButton);
            this.searchPage.Controls.Add(this.searchCrownButton);
            this.searchPage.Controls.Add(this.searchReadingButton);
            this.searchPage.Controls.Add(this.searchSimpleButton);
            this.searchPage.Controls.Add(this.searchGridView);
            this.searchPage.Controls.Add(this.searchTitleButton);
            this.searchPage.Controls.Add(this.searchTypeComboBox);
            this.searchPage.Controls.Add(this.searchTitleTextBox);
            this.searchPage.Controls.Add(this.searchPrintTextBox);
            this.searchPage.Controls.Add(this.label10);
            this.searchPage.Controls.Add(this.label11);
            this.searchPage.Controls.Add(this.label12);
            this.searchPage.Location = new System.Drawing.Point(4, 25);
            this.searchPage.Name = "searchPage";
            this.searchPage.Size = new System.Drawing.Size(739, 433);
            this.searchPage.TabIndex = 2;
            this.searchPage.Text = "Szukaj";
            this.searchPage.UseVisualStyleBackColor = true;
            // 
            // searchGridView
            // 
            this.searchGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchGridView.Location = new System.Drawing.Point(194, 3);
            this.searchGridView.Name = "searchGridView";
            this.searchGridView.RowHeadersWidth = 51;
            this.searchGridView.RowTemplate.Height = 24;
            this.searchGridView.Size = new System.Drawing.Size(545, 355);
            this.searchGridView.TabIndex = 39;
            // 
            // searchTitleButton
            // 
            this.searchTitleButton.Location = new System.Drawing.Point(5, 51);
            this.searchTitleButton.Name = "searchTitleButton";
            this.searchTitleButton.Size = new System.Drawing.Size(111, 34);
            this.searchTitleButton.TabIndex = 26;
            this.searchTitleButton.Text = "Szukaj w bazie";
            this.searchTitleButton.UseVisualStyleBackColor = true;
            this.searchTitleButton.Click += new System.EventHandler(this.SearchBookButton_Click);
            // 
            // searchTypeComboBox
            // 
            this.searchTypeComboBox.FormattingEnabled = true;
            this.searchTypeComboBox.Items.AddRange(new object[] {
            "Komedia",
            "Dramat",
            "Dramat romantyczny",
            "Tragedia",
            "Ballada",
            "Fantasy",
            "Manga",
            "Edukacyjna"});
            this.searchTypeComboBox.Location = new System.Drawing.Point(6, 119);
            this.searchTypeComboBox.Name = "searchTypeComboBox";
            this.searchTypeComboBox.Size = new System.Drawing.Size(159, 24);
            this.searchTypeComboBox.TabIndex = 25;
            this.searchTypeComboBox.Text = "Komedia";
            // 
            // searchTitleTextBox
            // 
            this.searchTitleTextBox.Location = new System.Drawing.Point(5, 23);
            this.searchTitleTextBox.Name = "searchTitleTextBox";
            this.searchTitleTextBox.Size = new System.Drawing.Size(160, 22);
            this.searchTitleTextBox.TabIndex = 18;
            // 
            // searchPrintTextBox
            // 
            this.searchPrintTextBox.Location = new System.Drawing.Point(8, 205);
            this.searchPrintTextBox.Name = "searchPrintTextBox";
            this.searchPrintTextBox.Size = new System.Drawing.Size(159, 22);
            this.searchPrintTextBox.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 17);
            this.label10.TabIndex = 15;
            this.label10.Text = "Gatunek";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 17);
            this.label11.TabIndex = 14;
            this.label11.Text = "Tytuł";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 185);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 17);
            this.label12.TabIndex = 16;
            this.label12.Text = "Wydawnictwo";
            // 
            // editPage
            // 
            this.editPage.Controls.Add(this.editCrownButton);
            this.editPage.Controls.Add(this.editReadingButton);
            this.editPage.Controls.Add(this.editSimpleButton);
            this.editPage.Controls.Add(this.editHistoryTextBox);
            this.editPage.Controls.Add(this.label17);
            this.editPage.Controls.Add(this.editButton);
            this.editPage.Controls.Add(this.deleteButton);
            this.editPage.Controls.Add(this.editIDBox);
            this.editPage.Controls.Add(this.label16);
            this.editPage.Controls.Add(this.editGridView);
            this.editPage.Controls.Add(this.editTypeBox);
            this.editPage.Controls.Add(this.groupBox3);
            this.editPage.Controls.Add(this.label8);
            this.editPage.Controls.Add(this.groupBox4);
            this.editPage.Controls.Add(this.editDescriptionBox);
            this.editPage.Controls.Add(this.label9);
            this.editPage.Controls.Add(this.editTitleBox);
            this.editPage.Controls.Add(this.editPrintBox);
            this.editPage.Controls.Add(this.label13);
            this.editPage.Controls.Add(this.label14);
            this.editPage.Controls.Add(this.label15);
            this.editPage.Location = new System.Drawing.Point(4, 25);
            this.editPage.Name = "editPage";
            this.editPage.Size = new System.Drawing.Size(739, 433);
            this.editPage.TabIndex = 3;
            this.editPage.Text = "Edytuj i usuń";
            this.editPage.UseVisualStyleBackColor = true;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(95, 144);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 43);
            this.editButton.TabIndex = 31;
            this.editButton.Text = "Edytuj";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(13, 144);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 43);
            this.deleteButton.TabIndex = 30;
            this.deleteButton.Text = "Usuń";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // editIDBox
            // 
            this.editIDBox.Location = new System.Drawing.Point(30, 9);
            this.editIDBox.Name = "editIDBox";
            this.editIDBox.ReadOnly = true;
            this.editIDBox.Size = new System.Drawing.Size(100, 22);
            this.editIDBox.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 17);
            this.label16.TabIndex = 28;
            this.label16.Text = "ID";
            // 
            // editGridView
            // 
            this.editGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.editGridView.Location = new System.Drawing.Point(341, 3);
            this.editGridView.Name = "editGridView";
            this.editGridView.RowHeadersWidth = 51;
            this.editGridView.RowTemplate.Height = 24;
            this.editGridView.Size = new System.Drawing.Size(398, 378);
            this.editGridView.TabIndex = 27;
            this.editGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.EditGridView_CellClick);
            // 
            // editTypeBox
            // 
            this.editTypeBox.FormattingEnabled = true;
            this.editTypeBox.Items.AddRange(new object[] {
            "Komedia",
            "Dramat",
            "Dramat romantyczny",
            "Tragedia",
            "Ballada",
            "Fantasy",
            "Manga",
            "Edukacyjna"});
            this.editTypeBox.Location = new System.Drawing.Point(6, 117);
            this.editTypeBox.Name = "editTypeBox";
            this.editTypeBox.Size = new System.Drawing.Size(159, 24);
            this.editTypeBox.TabIndex = 25;
            this.editTypeBox.Text = "Komedia";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.editThirdRadio);
            this.groupBox3.Controls.Add(this.editSecondRadio);
            this.groupBox3.Controls.Add(this.editFirstRadio);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(173, 190);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(157, 97);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rok kształcenia";
            // 
            // editThirdRadio
            // 
            this.editThirdRadio.AutoSize = true;
            this.editThirdRadio.Location = new System.Drawing.Point(6, 70);
            this.editThirdRadio.Name = "editThirdRadio";
            this.editThirdRadio.Size = new System.Drawing.Size(61, 21);
            this.editThirdRadio.TabIndex = 2;
            this.editThirdRadio.Text = "3 rok";
            this.editThirdRadio.UseVisualStyleBackColor = true;
            // 
            // editSecondRadio
            // 
            this.editSecondRadio.AutoSize = true;
            this.editSecondRadio.Location = new System.Drawing.Point(6, 43);
            this.editSecondRadio.Name = "editSecondRadio";
            this.editSecondRadio.Size = new System.Drawing.Size(61, 21);
            this.editSecondRadio.TabIndex = 1;
            this.editSecondRadio.Text = "2 rok";
            this.editSecondRadio.UseVisualStyleBackColor = true;
            // 
            // editFirstRadio
            // 
            this.editFirstRadio.AutoSize = true;
            this.editFirstRadio.Checked = true;
            this.editFirstRadio.Location = new System.Drawing.Point(6, 16);
            this.editFirstRadio.Name = "editFirstRadio";
            this.editFirstRadio.Size = new System.Drawing.Size(61, 21);
            this.editFirstRadio.TabIndex = 0;
            this.editFirstRadio.TabStop = true;
            this.editFirstRadio.Text = "1 rok";
            this.editFirstRadio.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 17);
            this.label8.TabIndex = 22;
            this.label8.Text = "Typ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.editSimpleRadio);
            this.groupBox4.Controls.Add(this.editCrownRadio);
            this.groupBox4.Controls.Add(this.editReadingRadio);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(13, 193);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(157, 94);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Typ";
            // 
            // editSimpleRadio
            // 
            this.editSimpleRadio.AutoSize = true;
            this.editSimpleRadio.Checked = true;
            this.editSimpleRadio.Location = new System.Drawing.Point(6, 17);
            this.editSimpleRadio.Name = "editSimpleRadio";
            this.editSimpleRadio.Size = new System.Drawing.Size(123, 21);
            this.editSimpleRadio.TabIndex = 32;
            this.editSimpleRadio.TabStop = true;
            this.editSimpleRadio.Text = "Zwykła książka";
            this.editSimpleRadio.UseVisualStyleBackColor = true;
            // 
            // editCrownRadio
            // 
            this.editCrownRadio.AutoSize = true;
            this.editCrownRadio.Location = new System.Drawing.Point(6, 71);
            this.editCrownRadio.Name = "editCrownRadio";
            this.editCrownRadio.Size = new System.Drawing.Size(90, 21);
            this.editCrownRadio.TabIndex = 1;
            this.editCrownRadio.Text = "Biały kruk";
            this.editCrownRadio.UseVisualStyleBackColor = true;
            this.editCrownRadio.CheckedChanged += new System.EventHandler(this.EditCrownRadio_CheckedChanged);
            // 
            // editReadingRadio
            // 
            this.editReadingRadio.AutoSize = true;
            this.editReadingRadio.Location = new System.Drawing.Point(6, 44);
            this.editReadingRadio.Name = "editReadingRadio";
            this.editReadingRadio.Size = new System.Drawing.Size(77, 21);
            this.editReadingRadio.TabIndex = 0;
            this.editReadingRadio.Text = "Lektura";
            this.editReadingRadio.UseVisualStyleBackColor = true;
            this.editReadingRadio.CheckedChanged += new System.EventHandler(this.EditReadingRadio_CheckedChanged);
            // 
            // editDescriptionBox
            // 
            this.editDescriptionBox.Location = new System.Drawing.Point(176, 83);
            this.editDescriptionBox.Multiline = true;
            this.editDescriptionBox.Name = "editDescriptionBox";
            this.editDescriptionBox.Size = new System.Drawing.Size(159, 74);
            this.editDescriptionBox.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(176, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 17);
            this.label9.TabIndex = 19;
            this.label9.Text = "Opis";
            // 
            // editTitleBox
            // 
            this.editTitleBox.Location = new System.Drawing.Point(5, 54);
            this.editTitleBox.Name = "editTitleBox";
            this.editTitleBox.Size = new System.Drawing.Size(160, 22);
            this.editTitleBox.TabIndex = 18;
            // 
            // editPrintBox
            // 
            this.editPrintBox.Location = new System.Drawing.Point(176, 20);
            this.editPrintBox.Name = "editPrintBox";
            this.editPrintBox.Size = new System.Drawing.Size(159, 22);
            this.editPrintBox.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 17);
            this.label13.TabIndex = 15;
            this.label13.Text = "Gatunek";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 17);
            this.label14.TabIndex = 14;
            this.label14.Text = "Tytuł";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(173, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 17);
            this.label15.TabIndex = 16;
            this.label15.Text = "Wydawnictwo";
            // 
            // historyTextBox
            // 
            this.historyTextBox.Enabled = false;
            this.historyTextBox.Location = new System.Drawing.Point(9, 242);
            this.historyTextBox.Multiline = true;
            this.historyTextBox.Name = "historyTextBox";
            this.historyTextBox.Size = new System.Drawing.Size(143, 187);
            this.historyTextBox.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 222);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Historia";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(401, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(328, 398);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 304);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 17);
            this.label17.TabIndex = 32;
            this.label17.Text = "Historia";
            // 
            // editHistoryTextBox
            // 
            this.editHistoryTextBox.Enabled = false;
            this.editHistoryTextBox.Location = new System.Drawing.Point(13, 324);
            this.editHistoryTextBox.Multiline = true;
            this.editHistoryTextBox.Name = "editHistoryTextBox";
            this.editHistoryTextBox.Size = new System.Drawing.Size(317, 97);
            this.editHistoryTextBox.TabIndex = 33;
            // 
            // editCrownButton
            // 
            this.editCrownButton.Location = new System.Drawing.Point(542, 387);
            this.editCrownButton.Name = "editCrownButton";
            this.editCrownButton.Size = new System.Drawing.Size(91, 34);
            this.editCrownButton.TabIndex = 44;
            this.editCrownButton.Text = "Białe kruki";
            this.editCrownButton.UseVisualStyleBackColor = true;
            this.editCrownButton.Click += new System.EventHandler(this.EditCrownButton_Click);
            // 
            // editReadingButton
            // 
            this.editReadingButton.Location = new System.Drawing.Point(461, 387);
            this.editReadingButton.Name = "editReadingButton";
            this.editReadingButton.Size = new System.Drawing.Size(75, 34);
            this.editReadingButton.TabIndex = 43;
            this.editReadingButton.Text = "Lektury";
            this.editReadingButton.UseVisualStyleBackColor = true;
            this.editReadingButton.Click += new System.EventHandler(this.EditReadingButton_Click);
            // 
            // editSimpleButton
            // 
            this.editSimpleButton.Location = new System.Drawing.Point(341, 387);
            this.editSimpleButton.Name = "editSimpleButton";
            this.editSimpleButton.Size = new System.Drawing.Size(114, 34);
            this.editSimpleButton.TabIndex = 42;
            this.editSimpleButton.Text = "Zwykłe książki";
            this.editSimpleButton.UseVisualStyleBackColor = true;
            this.editSimpleButton.Click += new System.EventHandler(this.EditSimpleButton_Click);
            // 
            // searchCrownButton
            // 
            this.searchCrownButton.Location = new System.Drawing.Point(395, 372);
            this.searchCrownButton.Name = "searchCrownButton";
            this.searchCrownButton.Size = new System.Drawing.Size(91, 34);
            this.searchCrownButton.TabIndex = 44;
            this.searchCrownButton.Text = "Białe kruki";
            this.searchCrownButton.UseVisualStyleBackColor = true;
            this.searchCrownButton.Click += new System.EventHandler(this.SearchCrownButton_Click);
            // 
            // searchReadingButton
            // 
            this.searchReadingButton.Location = new System.Drawing.Point(314, 372);
            this.searchReadingButton.Name = "searchReadingButton";
            this.searchReadingButton.Size = new System.Drawing.Size(75, 34);
            this.searchReadingButton.TabIndex = 43;
            this.searchReadingButton.Text = "Lektury";
            this.searchReadingButton.UseVisualStyleBackColor = true;
            this.searchReadingButton.Click += new System.EventHandler(this.SearchReadingButton_Click);
            // 
            // searchSimpleButton
            // 
            this.searchSimpleButton.Location = new System.Drawing.Point(194, 372);
            this.searchSimpleButton.Name = "searchSimpleButton";
            this.searchSimpleButton.Size = new System.Drawing.Size(114, 34);
            this.searchSimpleButton.TabIndex = 42;
            this.searchSimpleButton.Text = "Zwykłe książki";
            this.searchSimpleButton.UseVisualStyleBackColor = true;
            this.searchSimpleButton.Click += new System.EventHandler(this.SearchSimpleButton_Click);
            // 
            // searchTypeButton
            // 
            this.searchTypeButton.Location = new System.Drawing.Point(8, 149);
            this.searchTypeButton.Name = "searchTypeButton";
            this.searchTypeButton.Size = new System.Drawing.Size(111, 34);
            this.searchTypeButton.TabIndex = 45;
            this.searchTypeButton.Text = "Szukaj w bazie";
            this.searchTypeButton.UseVisualStyleBackColor = true;
            this.searchTypeButton.Click += new System.EventHandler(this.SearchTypeButton_Click);
            // 
            // searchPrintButton
            // 
            this.searchPrintButton.Location = new System.Drawing.Point(8, 233);
            this.searchPrintButton.Name = "searchPrintButton";
            this.searchPrintButton.Size = new System.Drawing.Size(111, 34);
            this.searchPrintButton.TabIndex = 46;
            this.searchPrintButton.Text = "Szukaj w bazie";
            this.searchPrintButton.UseVisualStyleBackColor = true;
            this.searchPrintButton.Click += new System.EventHandler(this.SearchPrintButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 458);
            this.Controls.Add(this.bookCatalogControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Katalog Książek";
            this.bookCatalogControl.ResumeLayout(false);
            this.addPage.ResumeLayout(false);
            this.addPage.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.openPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.openGridView)).EndInit();
            this.searchPage.ResumeLayout(false);
            this.searchPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchGridView)).EndInit();
            this.editPage.ResumeLayout(false);
            this.editPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl bookCatalogControl;
        private System.Windows.Forms.TabPage addPage;
        private System.Windows.Forms.TabPage openPage;
        private System.Windows.Forms.TabPage searchPage;
        private System.Windows.Forms.Button searchTitleButton;
        private System.Windows.Forms.ComboBox searchTypeComboBox;
        private System.Windows.Forms.TextBox searchTitleTextBox;
        private System.Windows.Forms.TextBox searchPrintTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView openGridView;
        private System.Windows.Forms.DataGridView searchGridView;
        private System.Windows.Forms.Button addBookButton;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton thirdYearRadioButton;
        private System.Windows.Forms.RadioButton secondYearRadioButton;
        private System.Windows.Forms.RadioButton firstYearRadioButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton whiteCrownRadioButton;
        private System.Windows.Forms.RadioButton readingRadioButton;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.TextBox printTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage editPage;
        private System.Windows.Forms.DataGridView editGridView;
        private System.Windows.Forms.ComboBox editTypeBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton editThirdRadio;
        private System.Windows.Forms.RadioButton editSecondRadio;
        private System.Windows.Forms.RadioButton editFirstRadio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton editCrownRadio;
        private System.Windows.Forms.RadioButton editReadingRadio;
        private System.Windows.Forms.TextBox editDescriptionBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox editTitleBox;
        private System.Windows.Forms.TextBox editPrintBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox editIDBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.RadioButton simpleRadioButton;
        private System.Windows.Forms.Button crownViewButton;
        private System.Windows.Forms.Button readingViewButton;
        private System.Windows.Forms.Button simpleViewButton;
        private System.Windows.Forms.RadioButton editSimpleRadio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox historyTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox editHistoryTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button editCrownButton;
        private System.Windows.Forms.Button editReadingButton;
        private System.Windows.Forms.Button editSimpleButton;
        private System.Windows.Forms.Button searchCrownButton;
        private System.Windows.Forms.Button searchReadingButton;
        private System.Windows.Forms.Button searchSimpleButton;
        private System.Windows.Forms.Button searchPrintButton;
        private System.Windows.Forms.Button searchTypeButton;
    }
}

