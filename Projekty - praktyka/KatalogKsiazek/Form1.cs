﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatalogKsiazek
{
    public partial class Form1 : Form
    {
        
        
        public Form1()
        {
            InitializeComponent();
            
        }
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();

        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=KatalogKsiazek.db;Version=3;New=False;Compress=True;");
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.Parameters.Add("title", DbType.String).Value = editTitleBox.Text;
            sql_cmd.Parameters.Add("type", DbType.String).Value = editTypeBox.Text;
            sql_cmd.Parameters.Add("print", DbType.String).Value = editPrintBox.Text;
            sql_cmd.Parameters.Add("description", DbType.String).Value = editDescriptionBox.Text;
            sql_cmd.Parameters.Add("id", DbType.String).Value = editIDBox.Text;
            sql_cmd.Parameters.Add("history", DbType.String).Value = editHistoryTextBox.Text;

            sql_cmd.ExecuteNonQuery();
            sql_con.Close();

        }
        public void LoadData(string commandText)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            
            DB = new SQLiteDataAdapter(commandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            openGridView.DataSource = DT;
            editGridView.DataSource = DT;
            searchGridView.DataSource = DT;
            sql_con.Close();
        }




        private void AddBookButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(titleTextBox.Text) || String.IsNullOrWhiteSpace(printTextBox.Text) || String.IsNullOrWhiteSpace(typeComboBox.Text) || String.IsNullOrWhiteSpace(descriptionTextBox.Text))
            {
                MessageBox.Show("Nie wpisałeś wszystkich informacji o książce!", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Book tempBook = new Book();
                tempBook.Title = titleTextBox.Text;
                tempBook.Type = typeComboBox.Text;
                tempBook.Print = printTextBox.Text;
                tempBook.Description = descriptionTextBox.Text;
                if (readingRadioButton.Checked)
                {
                    tempBook.Kind = "Lektura";
                    if (firstYearRadioButton.Checked)
                    {
                        tempBook.Year = "1 klasa";

                    }
                    else if (secondYearRadioButton.Checked)
                    {
                        tempBook.Year = "2 klasa";
                    }
                    else if (thirdYearRadioButton.Checked)
                    {
                        tempBook.Year = "3 klasa";
                    }
                    else
                    {
                        tempBook.Year = "";
                    }

                }
                else if (whiteCrownRadioButton.Checked)
                {
                    tempBook.Kind = "Biały kruk";
                }
                else
                {
                    tempBook.Kind = "Zwykła książka";
                }

                if (tempBook.Kind == "Lektura")
                {
                    string txtQuery = "insert into Lektury (Title, Type, Print, Description, Kind, Year)values('" + tempBook.Title + "','" + tempBook.Type + "','" + tempBook.Print + "','" + tempBook.Description + "','" + tempBook.Kind + "','" + tempBook.Year + "')";
                    ExecuteQuery(txtQuery);
                    LoadData("select * from Lektury");
                }
                else if (tempBook.Kind == "Biały kruk")
                {
                    string txtQuery = "insert into BiałeKruki (Title, Type, Print, Description, Kind, History)values('" + tempBook.Title + "','" + tempBook.Type + "','" + tempBook.Print + "','" + tempBook.Description + "','" + tempBook.Kind + "','" + historyTextBox.Text + "')";
                    ExecuteQuery(txtQuery);
                    LoadData("select * from BiałeKruki");
                }
                else
                {
                    string txtQuery = "insert into ZwykłeKsiążki (Title, Type, Print, Description, Kind)values('" + tempBook.Title + "','" + tempBook.Type + "','" + tempBook.Print + "','" + tempBook.Description + "','" + tempBook.Kind + "')";
                    ExecuteQuery(txtQuery);
                    LoadData("select * from ZwykłeKsiążki");
                }

                MessageBox.Show("Dodano książkę " + tempBook.Title + ".", "Pomyślnie dodałeś książkę.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                titleTextBox.Text = "";
                printTextBox.Text = "";
                descriptionTextBox.Text = "";
                historyTextBox.Text = "";
            }
            
        }
     
        

        private void SearchBookButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(searchTitleTextBox.Text))
            {
                MessageBox.Show("Nie wpisałeś tytułu do wyszukania!", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    DataView DV = new DataView(DT);

                    DV.RowFilter = string.Format("Title LIKE '%{0}%'", searchTitleTextBox.Text);
                    searchGridView.DataSource = DV;
                }
                catch (System.Data.EvaluateException)
                {

                    MessageBox.Show("Nie można odnaleźć bazy danych!", "Błąd bazy danych!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

       

        private void ReadingRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (readingRadioButton.Checked)
            {
                groupBox2.Enabled = true;
            }
            else
            {
                groupBox2.Enabled = false;
            }
        }
        private void EditReadingRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (editReadingRadio.Checked)
            {
                groupBox3.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = false;
            }
        }

       

        

        private void EditGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Book tempBook = new Book();
            try
            {
                if (editGridView.SelectedRows[0].Cells[5].Value.ToString() == "Lektura")
                {
                    tempBook.Kind = "Lektura";
                    if (editGridView.SelectedRows[0].Cells[6].Value.ToString() == "1 klasa")
                    {
                        editReadingRadio.Checked = true;
                        tempBook.Year = "1 klasa";


                    }
                    else if (editGridView.SelectedRows[0].Cells[6].Value.ToString() == "2 klasa")
                    {
                        editReadingRadio.Checked = true;
                        tempBook.Year = "2 klasa";

                    }
                    else if (editGridView.SelectedRows[0].Cells[6].Value.ToString() == "3 klasa")
                    {
                        editReadingRadio.Checked = true;
                        tempBook.Year = "3 klasa";

                    }
                    else
                    {

                        tempBook.Year = "";

                    }

                }
                else if(editGridView.SelectedRows[0].Cells[5].Value.ToString() == "Biały kruk")
                {
                    tempBook.Kind = "Biały kruk";
                    editCrownRadio.Checked = true;
                }
                else
                {
                    tempBook.Kind = "Zwykła książka";
                    editSimpleRadio.Checked = true;
                }
                if (tempBook.Kind == "Biały kruk")
                {
                    groupBox4.Enabled = false;
                    editCrownRadio.Checked = true;
                    editIDBox.Text = editGridView.SelectedRows[0].Cells[0].Value.ToString();
                    editTitleBox.Text = editGridView.SelectedRows[0].Cells[1].Value.ToString();
                    editTypeBox.Text = editGridView.SelectedRows[0].Cells[2].Value.ToString();
                    editPrintBox.Text = editGridView.SelectedRows[0].Cells[3].Value.ToString();
                    editDescriptionBox.Text = editGridView.SelectedRows[0].Cells[4].Value.ToString();
                    editHistoryTextBox.Text = editGridView.SelectedRows[0].Cells[6].Value.ToString();
                }
                else if (tempBook.Kind == "Zwykła książka")
                {
                    groupBox4.Enabled = false;
                    editSimpleRadio.Checked = true;
                    editIDBox.Text = editGridView.SelectedRows[0].Cells[0].Value.ToString();
                    editTitleBox.Text = editGridView.SelectedRows[0].Cells[1].Value.ToString();
                    editTypeBox.Text = editGridView.SelectedRows[0].Cells[2].Value.ToString();
                    editPrintBox.Text = editGridView.SelectedRows[0].Cells[3].Value.ToString();
                    editDescriptionBox.Text = editGridView.SelectedRows[0].Cells[4].Value.ToString();

                }
                else
                {
                    groupBox4.Enabled = false;
                    editReadingRadio.Checked = true;
                    editIDBox.Text = editGridView.SelectedRows[0].Cells[0].Value.ToString();
                    editTitleBox.Text = editGridView.SelectedRows[0].Cells[1].Value.ToString();
                    editTypeBox.Text = editGridView.SelectedRows[0].Cells[2].Value.ToString();
                    editPrintBox.Text = editGridView.SelectedRows[0].Cells[3].Value.ToString();
                    editDescriptionBox.Text = editGridView.SelectedRows[0].Cells[4].Value.ToString();
                    if (tempBook.Year == "1 klasa")
                    {
                        editReadingRadio.Checked = true;
                        editFirstRadio.Checked = true;
                        editSecondRadio.Checked = false;
                        editThirdRadio.Checked = false;


                    }
                    else if (tempBook.Year == "2 klasa")
                    {
                        editReadingRadio.Checked = true;
                        editFirstRadio.Checked = false;
                        editSecondRadio.Checked = true;
                        editThirdRadio.Checked = false;
                    }
                    else if (tempBook.Year == "3 klasa")
                    {
                        editReadingRadio.Checked = true;
                        editFirstRadio.Checked = false;
                        editSecondRadio.Checked = false;
                        editThirdRadio.Checked = true; ;
                    }
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {

                MessageBox.Show("Zaznacz cały indeks!", "Błąd indeksu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (System.NullReferenceException)
            {

                MessageBox.Show("Zaznacz cały indeks!", "Błąd indeksu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (editGridView.SelectedRows[0].Cells[0].Value != null)
                {
                    DialogResult dialogResult = MessageBox.Show("Czy na pewno chcesz usunąć rekord?", "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dialogResult == DialogResult.Yes)
                    {
                        if (editReadingRadio.Checked)
                        {
                            string txtQuery = "delete from Lektury where ID='" + editIDBox.Text + "' ";
                            ExecuteQuery(txtQuery);
                            LoadData("select * from Lektury");
                        }
                        else if (editCrownRadio.Checked)
                        {
                            string txtQuery = "delete from BiałeKruki where ID='" + editIDBox.Text + "' ";
                            ExecuteQuery(txtQuery);
                            LoadData("select * from BiałeKruki");
                        }
                        else
                        {
                            string txtQuery = "delete from ZwykłeKsiążki where ID='" + editIDBox.Text + "' ";
                            ExecuteQuery(txtQuery);
                            LoadData("select * from ZwykłeKsiążki");
                        }
                        MessageBox.Show("Usunąłeś " + editTitleBox.Text + ".", "Pomyślnie usunięto książkę.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Nie zaznaczyłeś rekordu do usunięcia!", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Zaznacz cały indeks!", "Błąd indeksu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         
            
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (editGridView.SelectedRows[0].Cells[0].Value != null)
                {

                    Book tempBook = new Book();

                    if (editReadingRadio.Checked)
                    {
                        tempBook.Kind = "Lektura";
                        if (editFirstRadio.Checked)
                        {
                            tempBook.Year = "1 klasa";

                        }
                        else if (editSecondRadio.Checked)
                        {
                            tempBook.Year = "2 klasa";
                        }
                        else if (editThirdRadio.Checked)
                        {
                            tempBook.Year = "3 klasa";
                        }
                        else
                        {
                            tempBook.Year = "";
                        }
                        string txtQuery = "update Lektury set Title = :title, Type = :type, Print = :print, Description = :description, Year = '" + tempBook.Year + "' where ID=:id";

                        ExecuteQuery(txtQuery);
                        LoadData("select * from Lektury");

                    }
                    else if (editCrownRadio.Checked)
                    {
                        tempBook.Kind = "Biały kruk";
                        string txtQuery = "update BiałeKruki set Title = :title, Type = :type, Print = :print, Description = :description, History = :history where ID=:id";

                        ExecuteQuery(txtQuery);
                        LoadData("select * from BiałeKruki");
                    }
                    else
                    {
                        tempBook.Kind = "Zwykła książka";
                        string txtQuery = "update ZwykłeKsiążki set Title = :title, Type = :type, Print = :print, Description = :description where ID=:id";
                        ExecuteQuery(txtQuery);
                        LoadData("select * from ZwykłeKsiążki");

                    }
                    MessageBox.Show("Zedytowałeś wybraną książkę.", "Pomyślnie zedytowałeś książkę.", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("Wybierz książkę!", "Nie wybrano książki", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {

                MessageBox.Show("Zaznacz cały indeks!", "Błąd indeksu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void SimpleViewButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from ZwykłeKsiążki");
        }

        private void ReadingViewButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from Lektury");
        }

        private void CrownViewButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from BiałeKruki");
        }

        private void WhiteCrownRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (whiteCrownRadioButton.Checked)
            {
                historyTextBox.Enabled = true;
            }
            else
            {
                historyTextBox.Enabled = false;
            }
        }

        private void EditCrownRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (editCrownRadio.Checked)
            {
                editHistoryTextBox.Enabled = true;
            }
            else
            {
                editHistoryTextBox.Enabled = false;
            }
        }

        private void EditSimpleButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from ZwykłeKsiążki");
        }

        private void EditReadingButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from Lektury");
        }

        private void EditCrownButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from BiałeKruki");
        }

        private void SearchSimpleButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from ZwykłeKsiążki");
        }

        private void SearchReadingButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from Lektury");
        }

        private void SearchCrownButton_Click(object sender, EventArgs e)
        {
            LoadData("select * from BiałeKruki");
        }

        private void SearchTypeButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(searchTypeComboBox.Text))
            {
                MessageBox.Show("Nie wpisałeś gatunku do wyszukania!", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                        DataView DV = new DataView(DT);
                        DV.RowFilter = string.Format("Type LIKE '%{0}%'", searchTypeComboBox.Text);

                        searchGridView.DataSource = DV;
                }
                catch (System.Data.EvaluateException)
                {
                    MessageBox.Show("Nie można odnaleźć bazy danych!", "Błąd bazy danych!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void SearchPrintButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(searchPrintTextBox.Text))
            {
                MessageBox.Show("Nie wpisałeś wydawnictwa do wyszukania!", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    DataView DV = new DataView(DT);


                    DV.RowFilter = string.Format("Print LIKE '%{0}%'", searchPrintTextBox.Text);
                    searchGridView.DataSource = DV;
                }
               
                catch (System.Data.EvaluateException)
                {

                    MessageBox.Show("Nie można odnaleźć bazy danych!", "Błąd bazy danych!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
