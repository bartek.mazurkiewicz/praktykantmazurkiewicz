﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogKsiazek
{
    class Book
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string Print { get; set; }
        public string Description { get; set; }
        public string Kind { get; set; }
        public string Year { get; set; }
        public string History { get; set; }
        
    }
}
